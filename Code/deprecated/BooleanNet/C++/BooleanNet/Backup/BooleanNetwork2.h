/*
 * File:   BooleanNetwork2.h
 * Author: Shannon Fenn (shannon.fenn@gmail.com)
 *
 * Purpose: Represents a boolean network constructed only from 2 input gates (of arbitrary function)
 *
 * Created on 5 December 2013
 */

#ifndef BOOLEANNETWORK2_H
#define BOOLEANNETWORK2_H

#include <string>
#include <random>
#include <iostream>

using namespace std;

class BooleanNetwork2Exception : public std::exception
{
public:
    BooleanNetwork2Exception(std::string message) : str(message) {}
    virtual ~BooleanNetwork2Exception() throw() {}
    virtual const char* what() const throw() {
        return std::string("BooleanNetwork2Exception").append(str).c_str();
    }
private:
    std::string str;
};

struct Gate {
    size_t lsb;
    size_t msb;
    bool f[4];
    friend bool operator==(const Gate& lhs, const Gate& rhs) {return lhs.lsb == rhs.lsb && lhs.msb == rhs.msb && lhs.f == rhs.f;}
    friend bool operator!=(const Gate& lhs, const Gate& rhs) {return ! (lhs == rhs);}
};


class BooleanNetwork2
{
public:
    BooleanNetwork2(const BooleanNetwork2& orig);
    BooleanNetwork2(size_t numInputs, size_t numOutputs, std::vector<Gate> initialGates);
    virtual ~BooleanNetwork2();
    int GetNo() const;
    int GetNi() const;
    std::vector<bool> evaluate(std::vector<bool> input) const;
    BooleanNetwork2 getRandomNeighbourSimple(std::default_random_engine& generator) const;
    BooleanNetwork2 getRandomNeighbourComplex(std::default_random_engine& generator, double reconnectionRate, double gateMutationRate) const;
    //std::vector<std::pair<ConnectionList, BooleanGate> > getGates() const { return gates; }
    vector<vector<bool>> getTruthTable() const;

    friend bool operator==(BooleanNetwork2 lhs, BooleanNetwork2 rhs);
    friend bool operator!=(BooleanNetwork2 lhs, BooleanNetwork2 rhs);
    friend std::ostream& operator<<(std::ostream& out, BooleanNetwork2 net);
private:
    BooleanNetwork2();
    size_t Ni;
    size_t No;

    std::vector<Gate> gates;
};

#endif // BOOLEANNETWORK2_H
