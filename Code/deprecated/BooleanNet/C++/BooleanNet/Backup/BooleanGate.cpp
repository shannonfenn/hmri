/* 
 * File:   BooleanGate.cpp
 * Author: shannon
 * 
 * Created on 28 September 2013, 3:56 PM
 */

#include "BooleanGate.h"
#include <math.h>

BooleanGate::BooleanGate(int numInputs, int functionValue) {
    Ni = numInputs;
    
    setFunction(functionValue);
}

BooleanGate::BooleanGate(int numInputs, std::vector<bool> functionTable) {
    Ni = numInputs;
    
    setFunction(functionTable);
}

BooleanGate::BooleanGate(const BooleanGate& orig) :
    Ni(orig.Ni),
    function(orig.function) {
}

BooleanGate::~BooleanGate() {
}

size_t BooleanGate::getNi() const {
    return Ni;
}

bool BooleanGate::evaluate(std::vector<bool> inputs) const {
    if(inputs.size() != Ni) {
        throw BooleanGateException(" - Invalid input length.");
    }
    
    int index = 0;
    int pow = 1;
    for(auto i = inputs.rbegin(); i != inputs.rend(); i++) {
        /// replace with += b*pow
        
        //index += *i * pow;      //works - test for speed
        if(*i)
            index += pow;
        pow = pow << 1;
    }
        
    return function.at(index);
}

bool BooleanGate::evaluate(int index) const {
    // Slightly slower but throws exception if index is invalid
    return function.at(index); 
}
    
void BooleanGate::setFunction(int functionValue) {
    for (int i = pow(2, Ni) - 1; i >= 0; i--) {
        bool bit = (1 << i & functionValue) != 0;
        function.push_back(bit);
    }
}

void BooleanGate::setFunction(std::vector<bool> functionTable) {
    if(pow(2, Ni) != functionTable.size())
        throw BooleanGateException(" - Invalid truth table length.");
    
    function.assign(functionTable.begin(), functionTable.end());
}

BooleanGate BooleanGate::getRandomNeighbour(std::default_random_engine& generator) const
{
    BooleanGate newGate(*this);
    size_t bitToChange = std::uniform_int_distribution<size_t>(0, Ni-1)(generator);
    newGate.function.at(bitToChange) = ! newGate.function.at(bitToChange);
    return newGate;
}

// Equality operator for two boolean gates
bool operator==(BooleanGate lhs, BooleanGate rhs) {
    // Gates are equal if they perform the same function on the same number of inputs
    return lhs.Ni == rhs.Ni && lhs.function == rhs.function;
}

// Inequality operator for two boolean gates
bool operator!=(BooleanGate lhs, BooleanGate rhs) {
    // Gates are unequal if they are not equal
    return not (lhs == rhs);
}
