/* 
 * File:   BooleanGate.h
 * Author: shannon
 *
 * Purpose: Represents a N input logic gate with a predefined (but changeable) 
 *          transfer function (any of the 2^N possible).
 * 
 * Created on 28 September 2013, 3:56 PM
 */

#ifndef BOOLEANGATE_H
#define	BOOLEANGATE_H

#include <vector>
#include <string>
#include <random>
#include <boost/functional/hash.hpp>

class BooleanGateException : public std::exception
{
public:
    BooleanGateException(std::string message) : str(message)  {}
    virtual ~BooleanGateException() throw() {}
    virtual const char* what() const throw() {
        return std::string("BooleanGateException").append(str).c_str();
    }
private:
    std::string str;
};

class BooleanGate {
public:
    BooleanGate(int numInputs, int functionValue);
    BooleanGate(int numInputs, std::vector<bool> functionTable);
    BooleanGate(const BooleanGate& orig);
    virtual ~BooleanGate();
    
    
    bool evaluate(std::vector<bool> inputs) const;
    bool evaluate(int index) const;
    
    void setFunction(int functionValue);
    void setFunction(std::vector<bool> functionTable);
    
    std::vector<bool> getFunction() const { return function; }
    
    BooleanGate getRandomNeighbour(std::default_random_engine& generator) const;
    
    size_t getNi() const;
    
    friend bool operator==(BooleanGate lhs, BooleanGate rhs);
    friend bool operator!=(BooleanGate lhs, BooleanGate rhs);
private:
    size_t Ni;
    std::vector<bool> function;
};

#endif	/* BOOLEANGATE_H */

