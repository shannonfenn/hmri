#include "BooleanNetwork2.h"
#include <algorithm>
#include <string>

BooleanNetwork2::BooleanNetwork2() {
}

BooleanNetwork2::BooleanNetwork2(size_t numInputs, size_t numOutputs, std::vector<Gate> initialGates) :
    Ni(numInputs),
    No(numOutputs),
    gates(initialGates)
{
    if(numInputs < 1) {
        throw BooleanNetwork2Exception(" - Invalid number of inputs.");
    }
    if(numOutputs < 1 || numOutputs > initialGates.size()) {
        throw BooleanNetwork2Exception(" - Invalid number of outputs - either negative, none or more outputs than gates.");
    }
    if(initialGates.empty()) {
        throw BooleanNetwork2Exception(" - Empty initial gate list.");
    }
}

BooleanNetwork2::BooleanNetwork2(const BooleanNetwork2& orig) :
    Ni(orig.Ni),
    No(orig.No),
    gates(orig.gates)
{
}

BooleanNetwork2::~BooleanNetwork2() {
}

int BooleanNetwork2::GetNo() const {
    return No;
}

int BooleanNetwork2::GetNi() const {
    return Ni;
}

std::vector<bool> BooleanNetwork2::evaluate(std::vector<bool> input) const {
    if(input.size() != Ni) {
        throw BooleanNetwork2Exception(" - Input length must be equal to network input size (Ni)");
    }

    //use the input as the state
    input.resize(Ni + gates.size());

    for(size_t g=0; g < gates.size(); g++) {
        // generate input vector
        //std::vector<bool> inp;
        //const Gate& gate = gates[g];
        //state[Ni + g] = gate.f[state[gate.msb] * 2 + state[gate.lsb]];

        //state[Ni + g] = gates[g].f[state[gates[g].msb] * 2 + state[gates[g].lsb]];
        input[Ni + g] = gates[g].f[input[gates[g].msb] * 2 + input[gates[g].lsb]];

        //int inp = state[gate.msb] * 2 + state[gate.lsb];
        //state[Ni + g] = gate.f[inp];
    }

//    return std::vector<bool>(state.end() - No, state.end());
    return std::vector<bool>(input.end() - No, input.end());
}

//std::vector<bool> BooleanNetwork2::evaluate(std::vector<bool> input) const {
//    if(input.size() != Ni) {
//        throw BooleanNetwork2Exception(" - Input length must be equal to network input size (Ni)");
//    }

//    std::vector<bool> state(input);
//    state.resize(Ni + gates.size(), false);

//    for(int g=0; g < gates.size(); g++) {
//        // generate input vector
//        //std::vector<bool> inp;
//        std::vector<bool> inp(gates[g].first.size(), false);

////        for(int i : gates[g].first) {
////            inp.push_back(state[i]);
////        }
//        for(int i =0; i < gates[g].first.size(); i++) {
//            inp[i] = state[gates[g].first[i]];
//        }

//        state[Ni + g] = gates[g].second.evaluate(inp);
//    }


//    return std::vector<bool>(state.end() - No, state.end());
//}

BooleanNetwork2 BooleanNetwork2::getRandomNeighbourSimple(std::default_random_engine& generator) const {
    BooleanNetwork2 neighbour(*this);

    // Select a random gate to change
    size_t gateToReconnect = std::uniform_int_distribution<size_t>(0, gates.size() - 1)(generator);

    // pick single random input connection to move
    bool msb = std::bernoulli_distribution()(generator);
    // decide how much to shift the gate (gate can only connect to previous gate or input)
    size_t shift = std::uniform_int_distribution<size_t>(1, gateToReconnect + Ni - 1)(generator);
    // Get location of the original connection
    size_t originalConnection = (msb ? gates.at(gateToReconnect).msb : gates.at(gateToReconnect).lsb);
        // Get shifted connection
    size_t newConnection = (originalConnection + shift) % (gateToReconnect + Ni);

    // actually modify the connection
    (msb ? neighbour.gates.at(gateToReconnect).msb : neighbour.gates.at(gateToReconnect).lsb) = newConnection;

    return neighbour;
}

//BooleanNetwork2 BooleanNetwork2::getRandomNeighbourComplex(std::default_random_engine& generator, double reconnectionRate, double gateMutationRate) const {

//    BooleanNetwork2 neighbour(*this);

//    // Select a random set of gates to change
//    std::vector<size_t> gatesToReconnect;
//    std::vector<size_t> gatesToChange;
//    for(size_t i=0; i < gates.size(); i++) {
//        double r = std::uniform_real_distribution<>(0, 1)(generator);
//        double m = std::uniform_real_distribution<>(0, 1)(generator);
//        if(r < reconnectionRate) {
//            gatesToReconnect.push_back(i);
//        }
//        if(m < gateMutationRate) {
//            gatesToChange.push_back(i);
//        }
//    }

//    // reconnect all randomly chosen gates
//    for(auto gate : gatesToReconnect) {
//        // pick single random input connection to move
//        auto numInputs = gates.at(gate).first.size();
//        auto connectionToChange = std::uniform_int_distribution<>(0, numInputs - 1)(generator);

//        // shift to gate somewhere before this one
//        auto shift = std::uniform_int_distribution<>(1, gate + Ni - 1)(generator);

//        auto previousConnection = gates.at(gate).first.at(connectionToChange);

//        // shift to another gate within range
//        auto newConnection = (previousConnection + shift) % (gate + Ni);

//        // actually modify the connection
//        neighbour.gates.at(gate).first.at(connectionToChange) = newConnection;
//    }
//    // change transfer functions for all randomly chosen gates
//    for(auto gate : gatesToChange) {
//        BooleanGate oldGate = neighbour.gates.at(gate).second;
//        neighbour.gates.at(gate).second = oldGate.getRandomNeighbour(generator);
//    }

//    return neighbour;
//}

vector<vector<bool> > BooleanNetwork2::getTruthTable() const {
    vector<bool> inp(Ni);
    size_t numInputs = pow(2, Ni);
    vector<vector<bool>> table(numInputs);

    for(size_t i=0; i<numInputs; i++) {
        //generate next input
        for (size_t b = 0; b < Ni; b++) {
            inp.at(Ni - 1 - b) = (1 << b & i) != 0;
        }

        table.at(i) = evaluate(inp);
    }

    return table;
}


bool operator==(BooleanNetwork2 lhs, BooleanNetwork2 rhs) {
    if(lhs.Ni != rhs.Ni || lhs.No != rhs.No) {
        return false;
    }
    else {
        return lhs.gates == rhs.gates;
   }
}

bool operator!=(BooleanNetwork2 lhs, BooleanNetwork2 rhs) {
    return not (lhs == rhs);
}

std::ostream& operator<<(std::ostream& out, BooleanNetwork2 net) {
    out << "Ng: " << net.gates.size() << "Ni: " << net.Ni << "No: " << net.No << std::endl;

    for(size_t i=0; i< net.gates.size(); i++) {
        out << "(" << i << "):\t";
        out << "msb: " << net.gates[i].msb << "\tlsb: " << net.gates[i].lsb << "\t";
        for(auto b: net.gates[i].f) {
            out << (b ? " 1" : " 0" );
        }
        out << std::endl;
    }

    return out;
}
