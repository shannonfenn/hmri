/*
 * File:   BooleanNetworkGeneral.h
 * Author: Shannon Fenn (shannon.fenn@gmail.com)
 *
 * Created on 30 September 2013, 8:02 PM
 */

#ifndef BOOLEANNETWORKGENERAL_H
#define BOOLEANNETWORKGENERAL_H

#include <vector>
#include <random>
#include <iostream>
#include "BooleanGate.h"

using namespace std;

typedef vector<int> ConnectionList;

class BooleanNetworkGeneral {
public:
    BooleanNetworkGeneral(const BooleanNetworkGeneral& orig);
    BooleanNetworkGeneral(size_t numInputs, size_t numOutputs, vector<pair<ConnectionList, BooleanGate> > initialGates);
    virtual ~BooleanNetworkGeneral();
    int GetNo() const;
    int GetNi() const;
    vector<bool> evaluate(vector<bool> input) const;
    BooleanNetworkGeneral getRandomNeighbourSimple(default_random_engine& generator) const;
    BooleanNetworkGeneral getRandomNeighbourComplex(default_random_engine& generator, double reconnectionRate, double gateMutationRate) const;
    vector<pair<ConnectionList, BooleanGate> > getGates() const { return gates; }

    friend bool operator==(const BooleanNetworkGeneral& lhs, const BooleanNetworkGeneral& rhs);
    friend bool operator!=(const BooleanNetworkGeneral& lhs, const BooleanNetworkGeneral& rhs);
    friend ostream& operator<<(ostream& out, const BooleanNetworkGeneral& net);
private:
    BooleanNetworkGeneral();
    size_t Ni;
    size_t No;
    vector<pair<ConnectionList, BooleanGate> > gates;
};

#endif // BOOLEANNETWORKGENERAL_H

