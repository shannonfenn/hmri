#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "BooleanNet/BooleanNetwork2.h"
#include <random>

std::vector<bool> toBool(int n, int base) {
    std::vector<bool> bits(base, false);
    for (int i = 0; i < base; i++) {
        bits.at(base - 1 - i) = (1 << i & n) != 0;
    }
    return bits;
}

// ADD TEST FOR TRUTH TABLE

TEST_CASE("getRandomNeighbourSimple/Output after change - single gate guaranteed to be different") {
    std::vector<Gate> gates;
    gates.push_back({0, 1, {0, 0, 0, 1}});
    BooleanNetwork2 net1(4, 1, gates);

    std::random_device rd;  // Seed with a real random value, if available
    std::default_random_engine generator(rd());

    for(int i=0; i<100; i++) {
        BooleanNetwork2 net2 = net1.getRandomNeighbourSimple(generator);
        int differences = 0;
        for(int k=0; k<16; k++) {
            if( net1.evaluate(toBool(k, 4)) != net2.evaluate(toBool(k, 4)) ) {
                differences += 1;
            }
        }
        REQUIRE( differences != 0);
    }
}

TEST_CASE("getRandomNeighbourSimple/Output after change - single layer guaranteed to be different") {
    std::vector<Gate> gates;
    gates.push_back({0, 1, {0, 0, 0, 1}});
    gates.push_back({2, 3, {0, 0, 0, 1}});
    BooleanNetwork2 net1(4, 2, gates);

    std::random_device rd;  // Seed with a real random value, if available
    std::default_random_engine generator(rd());

    for(int i=0; i<100; i++) {
        BooleanNetwork2 net2 = net1.getRandomNeighbourSimple(generator);
        int differences = 0;
        for(int k=0; k<16; k++) {
            if( net1.evaluate(toBool(k, 4)) != net2.evaluate(toBool(k, 4)) ) {
                differences += 1;
            }
        }
        REQUIRE( differences != 0);
    }
}

TEST_CASE("getRandomNeighbourSimple/100 steps") {
    std::vector<Gate> gates;
    gates.push_back({0, 1, {0, 0, 0, 1}});
    gates.push_back({2, 3, {0, 0, 0, 1}});
    gates.push_back({4, 5, {0, 0, 0, 1}});

    std::random_device rd;  // Seed with a real random value, if available
    std::default_random_engine generator(rd());

    BooleanNetwork2 net1(4, 2, gates);
    for(int i=0; i<100; i++) {
        BooleanNetwork2 net2 = net1.getRandomNeighbourSimple(generator);
        REQUIRE( net1 != net2 );
        net1 = net2;
    }
}

TEST_CASE("Evaluate/4-input AND") {
    std::vector<Gate> gates;
    gates.push_back({0, 1, {0, 0, 0, 1}});
    gates.push_back({2, 3, {0, 0, 0, 1}});
    gates.push_back({4, 5, {0, 0, 0, 1}});

    BooleanNetwork2 net(4, 1, gates);

    REQUIRE( net.evaluate( toBool(0, 4) ) == std::vector<bool>(1, false) );
    REQUIRE( net.evaluate( toBool(1, 4) ) == std::vector<bool>(1, false) );
    REQUIRE( net.evaluate( toBool(2, 4) ) == std::vector<bool>(1, false) );
    REQUIRE( net.evaluate( toBool(3, 4) ) == std::vector<bool>(1, false) );
    REQUIRE( net.evaluate( toBool(4, 4) ) == std::vector<bool>(1, false) );
    REQUIRE( net.evaluate( toBool(5, 4) ) == std::vector<bool>(1, false) );
    REQUIRE( net.evaluate( toBool(6, 4) ) == std::vector<bool>(1, false) );
    REQUIRE( net.evaluate( toBool(7, 4) ) == std::vector<bool>(1, false) );
    REQUIRE( net.evaluate( toBool(8, 4) ) == std::vector<bool>(1, false) );
    REQUIRE( net.evaluate( toBool(9, 4) ) == std::vector<bool>(1, false) );
    REQUIRE( net.evaluate( toBool(10, 4) ) == std::vector<bool>(1, false) );
    REQUIRE( net.evaluate( toBool(11, 4) ) == std::vector<bool>(1, false) );
    REQUIRE( net.evaluate( toBool(12, 4) ) == std::vector<bool>(1, false) );
    REQUIRE( net.evaluate( toBool(13, 4) ) == std::vector<bool>(1, false) );
    REQUIRE( net.evaluate( toBool(14, 4) ) == std::vector<bool>(1, false) );
    REQUIRE( net.evaluate( toBool(15, 4) ) == std::vector<bool>(1, true) );
}

TEST_CASE("Evaluate/4-input OR") {
    std::vector<Gate> gates;
    gates.push_back({0, 1, {0, 1, 1, 1}});
    gates.push_back({2, 3, {0, 1, 1, 1}});
    gates.push_back({4, 5, {0, 1, 1, 1}});

    BooleanNetwork2 net(4, 1, gates);

    REQUIRE( net.evaluate( toBool(0, 4) ) == std::vector<bool>(1, false) );
    REQUIRE( net.evaluate( toBool(1, 4) ) == std::vector<bool>(1, true) );
    REQUIRE( net.evaluate( toBool(2, 4) ) == std::vector<bool>(1, true) );
    REQUIRE( net.evaluate( toBool(3, 4) ) == std::vector<bool>(1, true) );
    REQUIRE( net.evaluate( toBool(4, 4) ) == std::vector<bool>(1, true) );
    REQUIRE( net.evaluate( toBool(5, 4) ) == std::vector<bool>(1, true) );
    REQUIRE( net.evaluate( toBool(6, 4) ) == std::vector<bool>(1, true) );
    REQUIRE( net.evaluate( toBool(7, 4) ) == std::vector<bool>(1, true) );
    REQUIRE( net.evaluate( toBool(8, 4) ) == std::vector<bool>(1, true) );
    REQUIRE( net.evaluate( toBool(9, 4) ) == std::vector<bool>(1, true) );
    REQUIRE( net.evaluate( toBool(10, 4) ) == std::vector<bool>(1, true) );
    REQUIRE( net.evaluate( toBool(11, 4) ) == std::vector<bool>(1, true) );
    REQUIRE( net.evaluate( toBool(12, 4) ) == std::vector<bool>(1, true) );
    REQUIRE( net.evaluate( toBool(13, 4) ) == std::vector<bool>(1, true) );
    REQUIRE( net.evaluate( toBool(14, 4) ) == std::vector<bool>(1, true) );
    REQUIRE( net.evaluate( toBool(15, 4) ) == std::vector<bool>(1, true) );
}

TEST_CASE("Evaluate/(1 ^ 2) v (3 ^ 4)") {
    std::vector<Gate> gates;
    gates.push_back({0, 1, {0, 0, 0, 1}});
    gates.push_back({2, 3, {0, 0, 0, 1}});
    gates.push_back({4, 5, {0, 1, 1, 1}});

    BooleanNetwork2 net(4, 1, gates);

    REQUIRE( net.evaluate( toBool(0, 4) ) == std::vector<bool>(1, false) );
    REQUIRE( net.evaluate( toBool(1, 4) ) == std::vector<bool>(1, false) );
    REQUIRE( net.evaluate( toBool(2, 4) ) == std::vector<bool>(1, false) );
    REQUIRE( net.evaluate( toBool(3, 4) ) == std::vector<bool>(1, true) );
    REQUIRE( net.evaluate( toBool(4, 4) ) == std::vector<bool>(1, false) );
    REQUIRE( net.evaluate( toBool(5, 4) ) == std::vector<bool>(1, false) );
    REQUIRE( net.evaluate( toBool(6, 4) ) == std::vector<bool>(1, false) );
    REQUIRE( net.evaluate( toBool(7, 4) ) == std::vector<bool>(1, true) );
    REQUIRE( net.evaluate( toBool(8, 4) ) == std::vector<bool>(1, false) );
    REQUIRE( net.evaluate( toBool(9, 4) ) == std::vector<bool>(1, false) );
    REQUIRE( net.evaluate( toBool(10, 4) ) == std::vector<bool>(1, false) );
    REQUIRE( net.evaluate( toBool(11, 4) ) == std::vector<bool>(1, true) );
    REQUIRE( net.evaluate( toBool(12, 4) ) == std::vector<bool>(1, true) );
    REQUIRE( net.evaluate( toBool(13, 4) ) == std::vector<bool>(1, true) );
    REQUIRE( net.evaluate( toBool(14, 4) ) == std::vector<bool>(1, true) );
    REQUIRE( net.evaluate( toBool(15, 4) ) == std::vector<bool>(1, true) );
    for(int i=0; i<16; i++) {
        std::vector<bool> expected(1, i > 10 || i == 3 || i == 7);

        REQUIRE( net.evaluate( toBool(i, 4) ) == expected );
    }
}
