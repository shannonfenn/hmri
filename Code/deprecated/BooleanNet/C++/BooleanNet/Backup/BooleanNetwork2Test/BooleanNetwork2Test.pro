#-------------------------------------------------
#
# Project created by QtCreator 2013-12-06T15:08:12
#
#-------------------------------------------------

QT       += core

QMAKE_CXXFLAGS += -std=c++11

INCLUDEPATH += ../../

TARGET = BooleanNetwork2Test

TEMPLATE = app

SOURCES += \
    ../../BooleanNet/BooleanNetwork2.cpp \
    BooleanNetwork2Test.cpp

HEADERS += \
    ../../BooleanNet/BooleanNetwork2.h
