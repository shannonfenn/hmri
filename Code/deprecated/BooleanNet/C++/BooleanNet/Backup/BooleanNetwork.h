/* 
 * File:   BooleanNetwork.h
 * Author: shannon
 *
 * Created on 30 September 2013, 8:02 PM
 */

#ifndef BOOLEANNETWORK_H
#define	BOOLEANNETWORK_H

#include <string>
#include <vector>
#include <random>
#include <iostream>

using namespace std;

class BooleanNetworkException : public exception
{
public:
    BooleanNetworkException(string message) : str(message) {}
    virtual ~BooleanNetworkException() throw() {}
    virtual const char* what() const throw() {
        return string("BooleanNetworkException").append(str).c_str();
    }
private:
    string str;
};



class BooleanNetwork {
public:
    struct Move
    {
        size_t gate;
        size_t connection;
        bool changeFirst;
    };

public:
    BooleanNetwork(const BooleanNetwork& orig);
    BooleanNetwork(size_t numInputs, size_t numOutputs);
    virtual ~BooleanNetwork();
    virtual size_t GetNo() const {return No;}
    virtual size_t GetNi() const {return Ni;}
    virtual size_t getNg() const = 0;
    virtual void evaluate(vector<char>& stateVector) const = 0;
//    virtual void evaluateFromChangedGate(vector<char>& stateVector) const = 0;
    virtual Move getRandomMove(default_random_engine& generator) const = 0;
    virtual void moveToNeighbour(const Move& move) = 0;
    virtual void revertMove() = 0;
    virtual vector<vector<char>> getTruthTable() const = 0;
protected:
    size_t Ni;
    size_t No;

private:
    BooleanNetwork();
};



#endif	/* BOOLEANNETWORK_H */

