/*
 * File:   GateAnnealer.cpp
 * Author: Shannon
 *
 * Created on 17 March 2014
 */

#include "GateAnnealer.h"
#include <math.h>

#include <sstream>

using namespace std;

GateAnnealer::GateAnnealer()
{
}

GateAnnealer::GateAnnealer(map<string, size_t> posIntParams, map<string, double> realParams)
{
    setParameters(posIntParams, realParams);
}

GateAnnealer::~GateAnnealer() {
}

void GateAnnealer::setParameters(map<string, size_t> posIntParams, map<string, double> realParams) {
    // Unpack arguments
    // If any of the following do not exist the map container with throw std::out_of_range
    numTemps = posIntParams.at("numTemps");
    stepsPerTemp = posIntParams.at("stepsPerTemp");

    finalNg = posIntParams.at("finalNg");

    initTemp = realParams.at("initTemp");
    tempRate = realParams.at("tempRate");
}

//GateAnnealer::Result GateAnnealer::run(size_t numTemps, size_t stepsPerTemp, double initTemp, double tempRate, BooleanNetworkNAND seed,
//                                 ostream& errorLog, ostream& progressLog, Metric guidingMetric) {
Optimiser::Result GateAnnealer::run(BooleanNetworkNAND seed, Metric guidingMetric, bool logErrors) {
    BooleanNetworkNAND state(seed);
    BooleanNetworkNAND bestState(seed);
    //vector<InputType > outputs;
    double error;
    double bestError;
    double bestErrorForTemp;
    double newError;
    bool accepted;
    double temperature = initTemp;
    size_t iteration;
    size_t bestIteration = 0;
    size_t totalIterations = numTemps * stepsPerTemp;
    size_t tempStep = 0;

    // Calculate initial error
    bestError = error = state.error(guidingMetric);
    
    if(logErrors) {
        bestErrorForTemp = error;
        cout << "\"BestErrorPerTemp\": [";
    }
    
    size_t gateAddIterations = totalIterations / (finalNg - state.getNg());
    
    size_t iterationsUntilGateAddition = gateAddIterations;
    
    for(iteration = 0; iteration < totalIterations && error > 0; iteration++, tempStep++)
    {

        if(iterationsUntilGateAddition > 0) {
            iterationsUntilGateAddition--;
        }
        else {
            iterationsUntilGateAddition = gateAddIterations;
            stringstream ss;
            ss << state;
            state.addGates(1);
            if(state.error(guidingMetric) != error) {
                cerr << "Adding gates changes error!" << state.error(guidingMetric) << " " << error << endl;
                cerr << ss.str() << endl;
                cerr << state << endl;
            }
        }

        // Reduce temperature if required
        if(tempStep >= stepsPerTemp) {
            tempStep = 0;
            temperature *= tempRate;
            
            // Pertubation code
            state.pertubExamples(generator);
            error = state.error(guidingMetric);
            
            // Tracking of error
            if(logErrors) {
                cout << bestErrorForTemp << ", " << std::flush;
                bestErrorForTemp = error;
            }
        }

        state.moveToNeighbour(state.getRandomMove(generator));

        newError = state.error(guidingMetric);

        // Keep best state seen
        if(newError < bestError) {
            bestState = state;
            bestError = newError;
            bestIteration = iteration;
        }

        // Determine whether to accept the new state
        accepted = accept(error, newError, temperature);

        if(accepted) {
            error = newError;
        }
        else {
            state.revertAllMoves();
        }
        state.clearHistory();
        
        // Tracking of error
        if(logErrors and error < bestErrorForTemp) {
            bestErrorForTemp = newError;
        }
    }
        
    if(logErrors)
        cout << "]" << std::flush;
    
    return {bestState, bestIteration, iteration};
}

bool GateAnnealer::accept(double oldError, double newError, double temperature) {
    double delta = newError - oldError;


    // PABLO METHOD

//    if(delta > temperature) {
//        return false;
//    }
//    else if (delta > 0) {
//        return true;
//    }
//    else if (delta > -temperature) {
//        return false;
//    }
//    else {
//        return true;
//    }


    // CLASSICAL EXPONENTIAL

    if(delta <= 0) {
        // Accept all movements for which dE is non-negative
        return true;
    }
    else if(temperature > 0){
        // Probability of acceptance based on standard formula P = e^(-dE/T)
        double P = exp( - delta / temperature);
        // Random uniformly distributed decimal in [0, 1)
        double r = uniform_real_distribution<>(0, 1)(generator);
        // Accept with probability P
        return r < P;
    }
    else {
        // Reject if dE > 0 and T = 0
        return false;
    }
}


