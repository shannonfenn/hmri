/*
 * File:   BooleanGate.h
 * Author: Shannon Fenn (shannon.fenn@gmail.com)
 *
 * Purpose: Represents a 2 input logic gate with a predefined (but changeable)
 *          transfer function (any of the 16 possible).
 *
 * Created on 5 December 2013
 */

#ifndef TWOINPUTBOOLEANGATE_H
#define TWOINPUTBOOLEANGATE_H

#include <string>
#include <random>

class TwoInputBooleanGateException : public std::exception
{
public:
    TwoInputBooleanGateException(std::string message) : str(message)  {}
    virtual ~TwoInputBooleanGateException() throw() {}
    virtual const char* what() const throw() {
        return std::string("TwoInputBooleanGateException").append(str).c_str();
    }
private:
    std::string str;
};

class TwoInputBooleanGate
{
public:
    TwoInputBooleanGate(int functionValue);
    TwoInputBooleanGate(bool functionTable[4]);
    TwoInputBooleanGate(const TwoInputBooleanGate& orig);
    virtual ~TwoInputBooleanGate();

    bool evaluate(bool msb, bool lsb) const;
    bool evaluate(int index) const;

    void setFunction(int functionValue);
    void setFunction(bool functionTable[4]);

    //std::vector<bool> getFunction() const { return function; }

    TwoInputBooleanGate getRandomNeighbour(std::default_random_engine& generator) const;

    int getNi() const;

    friend bool operator==(TwoInputBooleanGate lhs, TwoInputBooleanGate rhs);
    friend bool operator!=(TwoInputBooleanGate lhs, TwoInputBooleanGate rhs);
private:
    bool function[4];
};


#endif // TWOINPUTBOOLEANGATE_H
