/* 
 * File:   BooleanNetwork.cpp
 * Author: shannon
 * 
 * Created on 30 September 2013, 8:02 PM
 */

#include "BooleanNetwork.h"
#include <algorithm>
#include <string>

BooleanNetwork::BooleanNetwork() {
}

BooleanNetwork::BooleanNetwork(size_t numInputs, size_t numOutputs) :
    Ni(numInputs),
    No(numOutputs)
{
}

BooleanNetwork::BooleanNetwork(const BooleanNetwork& orig) : Ni(orig.Ni), No(orig.No) {
}

BooleanNetwork::~BooleanNetwork() {
}
