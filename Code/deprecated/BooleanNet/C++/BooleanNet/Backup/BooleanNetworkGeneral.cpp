/*
 * File:   BooleanNetworkGeneral.cpp
 * Author: Shannon Fenn (shannon.fenn@gmail.com)
 *
 * Created on 30 September 2013, 8:02 PM
 */

#include "BooleanNetworkGeneral.h"

#include <algorithm>
#include <string>
#include <stdexcept>

BooleanNetworkGeneral::BooleanNetworkGeneral() {
}

BooleanNetworkGeneral::BooleanNetworkGeneral(size_t numInputs, size_t numOutputs, std::vector<std::pair<ConnectionList, BooleanGate> > initialGates) :
    Ni(numInputs),
    No(numOutputs),
    gates(initialGates)
{
    if(numInputs < 1) {
        throw invalid_argument("BooleanNetworkGeneral - Invalid number of inputs.");
    }
    if(numOutputs < 1 || numOutputs > initialGates.size()) {
        throw invalid_argument("BooleanNetworkGeneral - Invalid number of outputs - either negative, none or more outputs than gates.");
    }
    if(initialGates.empty()) {
        throw invalid_argument("BooleanNetworkGeneral - Empty initial gate list.");
    }
}

BooleanNetworkGeneral::BooleanNetworkGeneral(const BooleanNetworkGeneral& orig) : Ni(orig.Ni), No(orig.No), gates(orig.gates) {
}

BooleanNetworkGeneral::~BooleanNetworkGeneral() {
}

int BooleanNetworkGeneral::GetNo() const {
    return No;
}

int BooleanNetworkGeneral::GetNi() const {
    return Ni;
}

std::vector<bool> BooleanNetworkGeneral::evaluate(std::vector<bool> input) const {
    if(input.size() != Ni) {
        throw invalid_argument("BooleanNetworkGeneral - Input length must be equal to network input size (Ni)");
    }

    std::vector<bool> state(input);
    state.resize(Ni + gates.size(), false);

    for(size_t g=0; g < gates.size(); g++) {
        // generate input vector
        //std::vector<bool> inp;
        int inp = 0;
        int pow = 1;
        for(int i : gates[g].first) {
            if(state[i])
                inp += pow;
            pow = pow << 1;
        }

        state[Ni + g] = gates[g].second.evaluate(inp);
    }


    return std::vector<bool>(state.end() - No, state.end());
}

//std::vector<bool> BooleanNetworkGeneral::evaluate(std::vector<bool> input) const {
//    if(input.size() != Ni) {
//        throw BooleanNetworkException(" - Input length must be equal to network input size (Ni)");
//    }

//    std::vector<bool> state(input);
//    state.resize(Ni + gates.size(), false);

//    for(int g=0; g < gates.size(); g++) {
//        // generate input vector
//        //std::vector<bool> inp;
//        std::vector<bool> inp(gates[g].first.size(), false);

////        for(int i : gates[g].first) {
////            inp.push_back(state[i]);
////        }
//        for(int i =0; i < gates[g].first.size(); i++) {
//            inp[i] = state[gates[g].first[i]];
//        }

//        state[Ni + g] = gates[g].second.evaluate(inp);
//    }


//    return std::vector<bool>(state.end() - No, state.end());
//}

BooleanNetworkGeneral BooleanNetworkGeneral::getRandomNeighbourSimple(std::default_random_engine& generator) const {
    BooleanNetworkGeneral neighbour(*this);

    // Select a random gate to change
    auto gateToReconnect = std::uniform_int_distribution<size_t>(0, gates.size() - 1)(generator);

    // Get the number of inputs for the selected gate
    auto numInputs = gates.at(gateToReconnect).first.size();
    // pick single random input connection to move
    auto connectionToChange = std::uniform_int_distribution<>(0, numInputs - 1)(generator);
    // decide how much to shift the gate (gate can only connect to previous gate or input)
    auto shift = std::uniform_int_distribution<>(1, gateToReconnect + Ni - 1)(generator);
    // Get location of the original connection
    auto originalConnection = gates.at(gateToReconnect).first.at(connectionToChange);
    // Get shifted connection
    auto newConnection = (originalConnection + shift) % (gateToReconnect + Ni);
    // actually modify the connection
    neighbour.gates.at(gateToReconnect).first.at(connectionToChange) = newConnection;

    return neighbour;
}

BooleanNetworkGeneral BooleanNetworkGeneral::getRandomNeighbourComplex(std::default_random_engine& generator, double reconnectionRate, double gateMutationRate) const {

    BooleanNetworkGeneral neighbour(*this);

    // Select a random set of gates to change
    std::vector<size_t> gatesToReconnect;
    std::vector<size_t> gatesToChange;
    for(size_t i=0; i < gates.size(); i++) {
        double r = std::uniform_real_distribution<>(0, 1)(generator);
        double m = std::uniform_real_distribution<>(0, 1)(generator);
        if(r < reconnectionRate) {
            gatesToReconnect.push_back(i);
        }
        if(m < gateMutationRate) {
            gatesToChange.push_back(i);
        }
    }

    // reconnect all randomly chosen gates
    for(auto gate : gatesToReconnect) {
        // pick single random input connection to move
        auto numInputs = gates.at(gate).first.size();
        auto connectionToChange = std::uniform_int_distribution<>(0, numInputs - 1)(generator);

        // shift to gate somewhere before this one
        auto shift = std::uniform_int_distribution<>(1, gate + Ni - 1)(generator);

        auto previousConnection = gates.at(gate).first.at(connectionToChange);

        // shift to another gate within range
        auto newConnection = (previousConnection + shift) % (gate + Ni);

        // actually modify the connection
        neighbour.gates.at(gate).first.at(connectionToChange) = newConnection;
    }
    // change transfer functions for all randomly chosen gates
    for(auto gate : gatesToChange) {
        BooleanGate oldGate = neighbour.gates.at(gate).second;
        neighbour.gates.at(gate).second = oldGate.getRandomNeighbour(generator);
    }

    return neighbour;
}

bool operator==(const BooleanNetworkGeneral& lhs, const BooleanNetworkGeneral& rhs) {
    if(lhs.Ni != rhs.Ni || lhs.No != rhs.No) {
        return false;
    }
    else {
        return lhs.gates == rhs.gates;
   }
}

bool operator!=(const BooleanNetworkGeneral& lhs, const BooleanNetworkGeneral& rhs) {
    return not (lhs == rhs);
}

std::ostream& operator<<(std::ostream& out, const BooleanNetworkGeneral& net) {
    out << "Ng: " << net.gates.size() << "Ni: " << net.Ni << "No: " << net.No << std::endl;

    out << "gate functions:" << std::endl;
    for(auto gate : net.gates) {
        for(auto b: gate.second.getFunction()) {
            out << (b ? " 1" : " 0" );
        }
        out << std::endl;
    }

    out << "Connections:" << std::endl;
    for(auto gate : net.gates) {
        for(auto c: gate.first) {
            out << c << " ";
        }
        out << std::endl;
    }

    return out;
}
