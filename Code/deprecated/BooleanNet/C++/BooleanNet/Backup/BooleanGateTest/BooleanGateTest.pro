#-------------------------------------------------
#
# Project created by QtCreator 2013-12-06T15:12:43
#
#-------------------------------------------------

QT       -= gui
QT       -= core

QMAKE_CXXFLAGS += -std=c++11

INCLUDEPATH += ../../ \
            ../

TARGET = BooleanGateTest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += \
    BooleanGateTest.cpp \
    ../../BooleanNet/BooleanGate.cpp

HEADERS += \
    ../../BooleanNet/BooleanGate.h
