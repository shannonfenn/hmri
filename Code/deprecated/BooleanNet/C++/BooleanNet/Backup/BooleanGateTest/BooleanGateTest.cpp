#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "BooleanNet/BooleanGate.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    return a.exec();
}

void testBooleanGate() {
    BooleanGate XOR(2, 6), OR(2, 7), AND(2, 1), zero(2, 0), ones(2, 15), arbitrary(2, 0);

    std::cout << "Testing BooleanGate" << std::endl;

    std::vector<bool> func;
    func.push_back(false);
    func.push_back(true);
    func.push_back(false);
    func.push_back(true);
    arbitrary.setFunction(func);

    std::cout << "arbitrary" << std::endl;
    for(size_t i=0; i<func.size(); i++) {
        assert(func.at(i) == arbitrary.evaluate(i));

        if(func.at(i) != arbitrary.evaluate(toBool(i))) {

            std::cout << i << "->" << func.at(i) << std::endl;
            for(auto b: toBool(i))
                std::cout << b;
            std::cout << std::endl;
        }
//        assert(func.at(i) == arbitrary.evaluate(toBool(i)));
    }

    std::cout << "XOR" << std::endl;
    test(!XOR.evaluate(0));
    test(!XOR.evaluate(toBool(0)));
    test(XOR.evaluate(1));
    test(XOR.evaluate(toBool(1)));
    test(XOR.evaluate(2));
    test(XOR.evaluate(toBool(2)));
    test(!XOR.evaluate(3));
    test(!XOR.evaluate(toBool(3)));

    std::cout << "AND" << std::endl;
    for(int i=0; i<3; i++) {
        test(!AND.evaluate(i));
        test(!AND.evaluate(toBool(i)));
    }
    test(AND.evaluate(3));
    test(AND.evaluate(toBool(3)));

    std::cout << "OR" << std::endl;
    test(!OR.evaluate(0));
    test(!OR.evaluate(toBool(0)));
    for(int i=1; i<4; i++) {
        test(OR.evaluate(i));
        test(OR.evaluate(toBool(i)));
    }

    std::cout << "0s" << std::endl;
    for(int i=0; i<4; i++) {
        test(!zero.evaluate(i));
        test(!zero.evaluate(toBool(i)));
    }
    std::cout << "1s" << std::endl;
    for(int i=0; i<4; i++) {
        test(ones.evaluate(i));
        test(ones.evaluate(toBool(i)));
    }
}
