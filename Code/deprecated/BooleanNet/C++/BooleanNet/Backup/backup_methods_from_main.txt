//bool parseAnnealArgs(int argc, char* argv[], size_t& Ng, size_t& numTemps, size_t& stepsPerTemp, double& initTemp, double& tempScaler, BitError::Metric& metric, string& errorLog, string& infoLog, string& resultFileName) {
//    string metricName;

//    bool validArgs = true;
//    try {
//        switch(argc) {
//        case 6:
//            Ng = stoi(argv[0]);
//            numTemps = stoi(argv[1]);
//            stepsPerTemp = stoi(argv[2]);
//            initTemp = stod(argv[3]);
//            tempScaler = stod(argv[4]);
//            metricName = argv[5];
//            errorLog = "";
//            infoLog = "";
//            break;
            
//        case 9:
//            resultFileName = argv[8];
//        case 8:
//            Ng = stoi(argv[0]);
//            numTemps = stoi(argv[1]);
//            stepsPerTemp = stoi(argv[2]);
//            initTemp = stod(argv[3]);
//            tempScaler = stod(argv[4]);
//            metricName = argv[5];
//            errorLog = argv[6];
//            infoLog = argv[7];
//            break;
//        default:
//            return false;
//        }

//        if(validArgs) {
//            transform(metricName.begin(), metricName.end(), metricName.begin(), ::tolower);
//            metric = metricFromName(metricName);
//        }
//    }
//    catch(invalid_argument e) {
//        cerr << e.what() << endl;
//        return false;
//    }
//    catch(...) {
//        return false;
//    }

//    return true;
//}

//bool parseTabuArgs(int argc, char* argv[], size_t& Ng, size_t& maxIterations, size_t& coolDownCount, size_t& movesPerIteration, BitError::Metric& metric, string& errorLog, string& infoLog, string& resultFileName) {
//    string metricName;

//    bool validArgs = true;
//    try {
//        switch(argc) {
//        case 5:
//            Ng = stoi(argv[0]);
//            maxIterations = stoi(argv[1]);
//            coolDownCount = stoi(argv[2]);
//            movesPerIteration = stoi(argv[3]);
//            metricName = argv[4];
//            errorLog = "";
//            infoLog = "";
//            break;
            
//        case 8:
//            resultFileName = argv[7];
//        case 7:
//            Ng = stoi(argv[0]);
//            maxIterations = stoi(argv[1]);
//            coolDownCount = stoi(argv[2]);
//            movesPerIteration = stoi(argv[3]);
//            metricName = argv[4];
//            errorLog = argv[5];
//            infoLog = argv[6];
//            break;
//        default:
//            return false;
//        }

//        if(validArgs) {
//            transform(metricName.begin(), metricName.end(), metricName.begin(), ::tolower);
//            metric = metricFromName(metricName);
//        }
//    }
//    catch(invalid_argument e) {
//        cerr << e.what() << endl;
//        return false;
//    }
//    catch(...) {
//        return false;
//    }

//    return true;
//}

///*
// *  runs simulated annealing on the binary addition problem.
// *  in - File to get data from
// *  Ne - number of examples to train over
// *  Ng - number of gates
// *  numTemps - number of different temperatures
// *  tempRate - (0, 1) multiplier between each temperature
// *  metric - error metric to use
// *  errorLog - log file for the calculated error at each iteration (guiding and objective functions)
// *  infoLog - log for information for the run
// */
//void annealFile(istream& in, size_t Ne, size_t Ng, size_t numTemps, size_t stepsPerTemp, double initTemp, double tempRate, BitError::Metric metric, ostream& errorLog, ostream &infoLog, ofstream& resultStream) {

//    Annealer a;
//    size_t Ni, No;
//    map<size_t, vector<char>> goal;
//    vector<vector<char>> trainingInputs;
//    vector<vector<char>> trainingOutputs;
//    vector<vector<char>> fullInputs;
//    vector<vector<char>> fullOutputs;
//    vector<size_t> trainingIndices;
//    random_device rd;  // Seed with a real random value, if available
//    default_random_engine generator(rd());

//    goal = loadDataSet(in, Ni, No);

//    for(const auto& kv : goal) {
//        trainingIndices.push_back(kv.first);
//        fullInputs.push_back( toBinary(kv.first, Ni) );
//        fullOutputs.push_back( kv.second );
//    }

//    infoLog << "Ni: " << Ni << "No: " << No << " Ne: " << Ne << " Ng: " << Ng << " numTemps: " << numTemps << " stepsPerTemp: " << stepsPerTemp << " initTemp: " << initTemp <<
//               " tempRate: " << tempRate << " metric: " << metric << endl;

//    // Get Ne random numbers in [ 0, 2^2*Ni )
//    shuffle(trainingIndices.begin(), trainingIndices.end(), generator);
//    if(Ne < trainingIndices.size()) {
//        trainingIndices.resize(Ne);
//    }

//    infoLog << "Examples represent " << Ne * 100.0 / goal.size() << "% of possible examples" << endl;

//    // Generate example input-output pairs
//    for(auto tr : trainingIndices) {
//        trainingInputs.push_back( toBinary(tr, Ni) );
//        trainingOutputs.push_back( goal[tr] );
//        infoLog << tr << " ";
//    }
//    infoLog << std::endl;

//    //Generate Random Seed
//    BooleanNetworkNAND seed(Ni, No, getRandomConnectionList(Ni, Ng, generator), trainingInputs, trainingOutputs);
    
//    //run annealing algorithm
////    BooleanNetworkNAND finalNetwork = a.run(coolingSchedule, trainingSet, seed, errorLog, cout, metric);
//    ofstream blank;
//    auto result = a.run(numTemps, stepsPerTemp, initTemp, tempRate, seed, errorLog, blank, metric);
//    BooleanNetworkNAND finalNetworkTrg = result.network;
//    BooleanNetworkNAND finalNetworkFull(finalNetworkTrg);
//    finalNetworkFull.setExamples(fullInputs, fullOutputs);
    
//    finalNetworkTrg.writeGraphML(resultStream);
    

//    infoLog << finalNetworkTrg << std::endl;

//    infoLog << "Training Error Matrix:" << endl;
//    for(auto e: finalNetworkTrg.errorMatrix()) {
//        for(auto b: e)
//            infoLog << (b ? 1 : 0);
//        infoLog << endl;
//    }

//    infoLog << "Full Error Matrix:" << endl;
//    for(auto e: finalNetworkFull.errorMatrix()) {
//        for(auto b: e)
//            infoLog << (b ? 1 : 0);
//        infoLog << endl;
//    }

//    infoLog << "Training Guiding Error: " << finalNetworkTrg.error(metric) << std::endl;
//    infoLog << "Training Objective Error: " << finalNetworkTrg.error() << std::endl;

//    infoLog << "Full Guiding Error: " << finalNetworkFull.error(metric) << std::endl;
//    infoLog << "Full Objective Error: " << finalNetworkFull.error() << std::endl;

////    infoLog << "Acceptance Rate: " << a.getAcceptanceRate() << std::endl;
////    infoLog << "Negative Acceptance Rate: " << a.getNegativeAcceptanceRate() << std::endl;
////    infoLog << "Non-Negative Acceptance Rate: " << a.getNonNegativeAcceptanceRate() << std::endl;

//    stringstream ss;
//    ss << metric;
//    printf("%-25s %18f %18f %18f %18f %15lu %15lu      ", ss.str().c_str(), finalNetworkTrg.error(metric), finalNetworkTrg.error(), finalNetworkFull.error(metric), finalNetworkFull.error(), result.iterationBest, result.iterationFinish);
////    cout << metric << "\t" << a.calculateError(trainingSet) << "\t" << a.calculateError(fullExamples) << "\t" << result.second << "\t";
//}


//void tabuFile(istream& in, size_t Ne, size_t Ng, size_t maxIterations, size_t coolDownCount, size_t movesPerIteration, BitError::Metric metric, ostream& errorLog, ostream &infoLog, ofstream& resultStream) {

//    TabuSearcher tabu;
//    size_t Ni, No;
//    map<size_t, vector<char>> goal;
//    vector<vector<char>> trainingInputs;
//    vector<vector<char>> trainingOutputs;
//    vector<vector<char>> fullInputs;
//    vector<vector<char>> fullOutputs;
//    vector<size_t> trainingIndices;
//    random_device rd;  // Seed with a real random value, if available
//    default_random_engine generator(rd());

//    goal = loadDataSet(in, Ni, No);

//    for(const auto& kv : goal) {
//        trainingIndices.push_back(kv.first);
//        fullInputs.push_back( toBinary(kv.first, Ni) );
//        fullOutputs.push_back( kv.second );
//    }

//    infoLog << "Ni: " << Ni << "No: " << No << " Ne: " << Ne << " Ng: " << Ng << " maxIterations: " << maxIterations << " coolDownCount: " << coolDownCount <<
//               " movesPerIteration: " << movesPerIteration << " metric: " << metric << endl;

//    // Get Ne random numbers in [ 0, 2^2*Ni )
//    shuffle(trainingIndices.begin(), trainingIndices.end(), generator);
//    if(Ne < trainingIndices.size()) {
//        trainingIndices.resize(Ne);
//    }

//    infoLog << "Examples represent " << Ne * 100.0 / goal.size() << "% of possible examples" << endl;

//    // Generate example input-output pairs
//    for(auto tr : trainingIndices) {
//        trainingInputs.push_back( toBinary(tr, Ni) );
//        trainingOutputs.push_back( goal[tr] );
//        infoLog << tr << " ";
//    }
//    infoLog << std::endl;

//    //Generate Random Seed
//    BooleanNetworkNAND seed(Ni, No, getRandomConnectionList(Ni, Ng, generator), trainingInputs, trainingOutputs);
    
//    //run annealing algorithm
////    BooleanNetworkNAND finalNetwork = a.run(coolingSchedule, trainingSet, seed, errorLog, cout, metric);
//    ofstream blank;
//    auto result = tabu.run(maxIterations, coolDownCount, movesPerIteration, seed, errorLog, blank, metric);
//    BooleanNetworkNAND finalNetworkTrg = result.network;
//    BooleanNetworkNAND finalNetworkFull(finalNetworkTrg);
//    finalNetworkFull.setExamples(fullInputs, fullOutputs);
    
//    finalNetworkTrg.writeGraphML(resultStream);
    

//    infoLog << finalNetworkTrg << std::endl;

//    infoLog << "Training Guiding Error: " << finalNetworkTrg.error(metric) << std::endl;
//    infoLog << "Training Objective Error: " << finalNetworkTrg.error() << std::endl;

//    infoLog << "Full Guiding Error: " << finalNetworkFull.error(metric) << std::endl;
//    infoLog << "Full Objective Error: " << finalNetworkFull.error() << std::endl;

//    stringstream ss;
//    ss << metric;
//    printf("%-25s %18f %18f %18f %18f %15lu %15lu      ", ss.str().c_str(), finalNetworkTrg.error(metric), finalNetworkTrg.error(), finalNetworkFull.error(metric), finalNetworkFull.error(), result.iterationBest, result.iterationFinish);
////    cout << metric << "\t" << a.calculateError(trainingSet) << "\t" << a.calculateError(fullExamples) << "\t" << result.second << "\t";
//}

///*
// *  runs simulated annealing on the binary addition problem with given samples.
// *  in - File to get data from
// *  samplesFile - File to get samples from
// *  Ne - number of examples to train over
// *  Ng - number of gates
// *  numTemps - number of different temperatures
// *  tempRate - (0, 1) multiplier between each temperature
// *  metric - error metric to use
// *  errorLog - log file for the calculated error at each iteration (guiding and objective functions)
// *  infoLog - log for information for the run
// */
//void annealFileGivenSamples(istream& in, istream& samplesFile, size_t Ng, size_t numTemps, size_t stepsPerTemp, double initTemp, double tempRate, BitError::Metric metric, ostream& errorLog, ostream &infoLog, ofstream& resultStream) {

//    Annealer a;
//    size_t Ni, No;
//    map<size_t, vector<char> > goal;
//    vector<vector<char>> trainingInputs;
//    vector<vector<char>> trainingOutputs;
//    vector<vector<char>> fullInputs;
//    vector<vector<char>> fullOutputs;
//    vector<size_t> trainingIndices;

//    goal = loadDataSet(in, Ni, No);
    
//    for(const auto& kv : goal) {
//        fullInputs.push_back( toBinary(kv.first, Ni) );
//        fullOutputs.push_back( kv.second );
//    }
    
//    size_t e;
//    while(samplesFile.good()) {
//        samplesFile >> e;
//        if(e >= goal.size())
//            throw invalid_argument("Example index larger than goal size.");
//        trainingIndices.push_back(e);
//    }

//    infoLog << "Ni: " << Ni << "No: " << No << " Ne: " << trainingIndices.size() << " Ng: " << Ng << " numTemps: " << numTemps << " stepsPerTemp: " << stepsPerTemp << " initTemp: " << initTemp <<
//               " tempRate: " << tempRate << " metric: " << metric << endl;

//    infoLog << "Examples represent " << trainingIndices.size() * 100.0 / goal.size() << "% of possible examples" << endl;

//    // Generate example input-output pairs
//    for(auto tr : trainingIndices) {
//        trainingInputs.push_back( toBinary(tr, Ni) );
//        trainingOutputs.push_back( goal[tr] );
//        infoLog << tr << " ";
//    }
//    infoLog << std::endl;

//    //Generate Random Seed
//    random_device rd;  // Seed with a real random value, if available
//    default_random_engine generator(rd());
//    BooleanNetworkNAND seed(Ni, No, getRandomConnectionList(Ni, Ng, generator), trainingInputs, trainingOutputs);
    
//    //run annealing algorithm
////    BooleanNetworkNAND finalNetwork = a.run(coolingSchedule, trainingSet, seed, errorLog, cout, metric);
//    ofstream blank;
//    auto result = a.run(numTemps, stepsPerTemp, initTemp, tempRate, seed, errorLog, blank, metric);
//    BooleanNetworkNAND finalNetworkTrg = result.network;
//    BooleanNetworkNAND finalNetworkFull(finalNetworkTrg);
//    finalNetworkFull.setExamples(fullInputs, fullOutputs);
    
//    finalNetworkTrg.writeGraphML(resultStream);
    
//    infoLog << finalNetworkTrg << std::endl;
    
//    infoLog << "Training Guiding Error: " << finalNetworkTrg.error(metric) << std::endl;
//    infoLog << "Training Objective Error: " << finalNetworkTrg.error() << std::endl;

//    infoLog << "Full Guiding Error: " << finalNetworkFull.error(metric) << std::endl;
//    infoLog << "Full Objective Error: " << finalNetworkFull.error() << std::endl;
    
////    infoLog << "Acceptance Rate: " << a.getAcceptanceRate() << std::endl;
////    infoLog << "Negative Acceptance Rate: " << a.getNegativeAcceptanceRate() << std::endl;
////    infoLog << "Non-Negative Acceptance Rate: " << a.getNonNegativeAcceptanceRate() << std::endl;

//    stringstream ss;
//    ss << metric;
//    printf("%-25s %18f %18f %18f %18f %15lu %15lu      ", ss.str().c_str(), finalNetworkTrg.error(metric), finalNetworkTrg.error(), finalNetworkFull.error(metric), finalNetworkFull.error(), result.iterationBest, result.iterationFinish);
////    cout << metric << "\t" << a.calculateError(trainingSet) << "\t" << a.calculateError(fullExamples) << "\t" << result.second << "\t";
//}

//void tabuFileGivenSamples(istream& in, istream& samplesFile, size_t Ng, size_t maxIterations, size_t coolDownCount, size_t movesPerIteration, BitError::Metric metric, ostream& errorLog, ostream &infoLog, ofstream& resultStream) {
    
//    TabuSearcher tabu;
//    size_t Ni, No;
//    map<size_t, vector<char> > goal;
//    vector<vector<char>> trainingInputs;
//    vector<vector<char>> trainingOutputs;
//    vector<vector<char>> fullInputs;
//    vector<vector<char>> fullOutputs;
//    vector<size_t> trainingIndices;

//    goal = loadDataSet(in, Ni, No);
    
//    for(const auto& kv : goal) {
//        fullInputs.push_back( toBinary(kv.first, Ni) );
//        fullOutputs.push_back( kv.second );
//    }
    
//    size_t e;
//    while(samplesFile.good()) {
//        samplesFile >> e;
//        if(e >= goal.size())
//            throw invalid_argument("Example index larger than goal size.");
//        trainingIndices.push_back(e);
//    }

//    infoLog << "Ni: " << Ni << "No: " << No << " Ne: " << trainingIndices.size() << " Ng: " << Ng << " maxIterations: " << maxIterations << " coolDownCount: " << coolDownCount <<
//               " movesPerIteration: " << movesPerIteration << " metric: " << metric << endl;

//    infoLog << "Examples represent " << trainingIndices.size() * 100.0 / goal.size() << "% of possible examples" << endl;

//    // Generate example input-output pairs
//    for(auto tr : trainingIndices) {
//        trainingInputs.push_back( toBinary(tr, Ni) );
//        trainingOutputs.push_back( goal[tr] );
//        infoLog << tr << " ";
//    }
//    infoLog << std::endl;

//    //Generate Random Seed
//    random_device rd;  // Seed with a real random value, if available
//    default_random_engine generator(rd());
//    BooleanNetworkNAND seed(Ni, No, getRandomConnectionList(Ni, Ng, generator), trainingInputs, trainingOutputs);
    
//    //run annealing algorithm
////    BooleanNetworkNAND finalNetwork = a.run(coolingSchedule, trainingSet, seed, errorLog, cout, metric);
//    ofstream blank;
//    auto result = tabu.run(maxIterations, coolDownCount, movesPerIteration, seed, errorLog, blank, metric);
//    BooleanNetworkNAND finalNetworkTrg = result.network;
//    BooleanNetworkNAND finalNetworkFull(finalNetworkTrg);
//    finalNetworkFull.setExamples(fullInputs, fullOutputs);
    
//    finalNetworkTrg.writeGraphML(resultStream);
    
//    infoLog << finalNetworkTrg << std::endl;
    
//    infoLog << "Training Guiding Error: " << finalNetworkTrg.error(metric) << std::endl;
//    infoLog << "Training Objective Error: " << finalNetworkTrg.error() << std::endl;

//    infoLog << "Full Guiding Error: " << finalNetworkFull.error(metric) << std::endl;
//    infoLog << "Full Objective Error: " << finalNetworkFull.error() << std::endl;
    
//    stringstream ss;
//    ss << metric;
//    printf("%-25s %18f %18f %18f %18f %15lu %15lu      ", ss.str().c_str(), finalNetworkTrg.error(metric), finalNetworkTrg.error(), finalNetworkFull.error(metric), finalNetworkFull.error(), result.iterationBest, result.iterationFinish);
////    cout << metric << "\t" << a.calculateError(trainingSet) << "\t" << a.calculateError(fullExamples) << "\t" << result.second << "\t";
//}

///*
// *  runs simulated annealing on the binary addition problem.
// *  in - File to get data from
// *  Ne - number of examples to train over
// *  Ng - number of gates
// *  numSteps - number of different temperatures
// *  tempRate - (0, 1) multiplier between each temperature
// *  metric - error metric to use
// *  errorLog - log file for the calculated error at each iteration (guiding and objective functions)
// *  infoLog - log for information for the run
// */
//void annealFileLeaveOneOutCV(istream& in, size_t Ng, size_t numTemps, size_t stepsPerTemp, double initTemp, double tempRate, BitError::Metric metric, ostream& errorLog, ostream &infoLog) {

//    Annealer a;
//    size_t Ni, No;
//    map<size_t, vector<char> > goal;
//    vector<vector<char>> trainingInputs;
//    vector<vector<char>> trainingOutputs;
//    vector<vector<char>> fullInputs;
//    vector<vector<char>> fullOutputs;
//    random_device rd;  // Seed with a real random value, if available
//    default_random_engine generator(rd());

//    goal = loadDataSet(in, Ni, No);

//    infoLog << "Ni: " << Ni << "No: " << No << " Ng: " << Ng << " numTemps: " << numTemps << " stepsPerTemp: " << stepsPerTemp << " initTemp: " << initTemp << " tempRate: " << tempRate << " metric: " << metric << endl;

//    vector<double> errors;

//    for(auto& kv : goal) {
//        fullInputs.push_back( toBinary(kv.first, Ni) );
//        fullOutputs.push_back( kv.second );
//    }

//    infoLog << "Leave-one-out cross-validation: " << endl;

//    // Run with every example as a test
//    for(size_t t=0; t<goal.size(); t++) {
//        // Generate training set without test case
//        auto testInp = trainingInputs[t];
//        auto testOut = trainingOutputs[t];
//        auto posInp = trainingInputs.erase(trainingInputs.begin() + t);
//        auto posOut = trainingOutputs.erase(trainingOutputs.begin() + t);
        
//        //Generate Random Seed
//        BooleanNetworkNAND seed(Ni, No, getRandomConnectionList(Ni, Ng, generator), trainingInputs, trainingOutputs);
        
//        // Run annealing algorithm
//        ofstream blank;
//        auto result = a.run(numTemps, stepsPerTemp, initTemp, tempRate, seed, errorLog, blank, metric);

//        // Generate a copy of the resulting network but for evaluation on test sample
//        BooleanNetworkNAND testNetwork(result.network);
//        testNetwork.setExamples(vector<vector<char>>(1, testInp), vector<vector<char>>(1, testOut));
        
//        // Record the error on the test case
//        errors.push_back(testNetwork.error());

//        // Add sample back to training set
//        trainingInputs.insert(posInp, testInp);
//        trainingOutputs.insert(posOut, testOut);
//    }

//    double tot = 0;
//    for(auto e : errors) {
//        cout << e << " ";
//        infoLog << e << endl;
//        tot += e;
//    }
//    cout << endl;
//    cout << tot / errors.size() << endl;
//}

//void tabuFileLeaveOneOutCV(istream& in, size_t Ng, size_t maxIterations, size_t coolDownCount, size_t movesPerIteration, BitError::Metric metric, ostream& errorLog, ostream &infoLog) {

//    TabuSearcher tabu;
//    size_t Ni, No;
//    map<size_t, vector<char> > goal;
//    vector<vector<char>> trainingInputs;
//    vector<vector<char>> trainingOutputs;
//    vector<vector<char>> fullInputs;
//    vector<vector<char>> fullOutputs;
//    random_device rd;  // Seed with a real random value, if available
//    default_random_engine generator(rd());

//    goal = loadDataSet(in, Ni, No);

//    infoLog << "Ni: " << Ni << "No: " << No << " Ng: " << Ng << " maxIterations: " << maxIterations << " coolDownCount: " << coolDownCount << " movesPerIteration: " << movesPerIteration << " metric: " << metric << endl;

//    vector<double> errors;

//    for(auto& kv : goal) {
//        fullInputs.push_back( toBinary(kv.first, Ni) );
//        fullOutputs.push_back( kv.second );
//    }

//    infoLog << "Leave-one-out cross-validation: " << endl;

//    // Run with every example as a test
//    for(size_t t=0; t<goal.size(); t++) {
//        // Generate training set without test case
//        auto testInp = trainingInputs[t];
//        auto testOut = trainingOutputs[t];
//        auto posInp = trainingInputs.erase(trainingInputs.begin() + t);
//        auto posOut = trainingOutputs.erase(trainingOutputs.begin() + t);
        
//        //Generate Random Seed
//        BooleanNetworkNAND seed(Ni, No, getRandomConnectionList(Ni, Ng, generator), trainingInputs, trainingOutputs);
        
//        // Run annealing algorithm
//        ofstream blank;
//        auto result = tabu.run(maxIterations, coolDownCount, movesPerIteration, seed, errorLog, blank, metric);

//        // Generate a copy of the resulting network but for evaluation on test sample
//        BooleanNetworkNAND testNetwork(result.network);
//        testNetwork.setExamples(vector<vector<char>>(1, testInp), vector<vector<char>>(1, testOut));
        
//        // Record the error on the test case
//        errors.push_back(testNetwork.error());

//        // Add sample back to training set
//        trainingInputs.insert(posInp, testInp);
//        trainingOutputs.insert(posOut, testOut);
//    }

//    double tot = 0;
//    for(auto e : errors) {
//        cout << e << " ";
//        infoLog << e << endl;
//        tot += e;
//    }
//    cout << endl;
//    cout << tot / errors.size() << endl;
//}

