#ifndef GATEANNEALER_H
#define GATEANNEALER_H

#include <vector>
#include <map>
#include <random>
#include <iostream>
#include <fstream>
#include "Optimiser.h"

class GateAnnealer : public Optimiser
{
public:
    GateAnnealer();
    GateAnnealer(map<string, size_t> posIntParams, map<string, double> realParams);

    virtual ~GateAnnealer();

    void setParameters(map<string, size_t> posIntParams, map<string, double> realParams);

    Result run(BooleanNetworkNAND seed, Metric guidingMetric, bool logErrors=false);

private:
    bool accept(double oldError, double newError, double temperature);

    size_t numTemps,
           stepsPerTemp,
           finalNg;

    double initTemp,
           tempRate;
};

#endif // GATEANNEALER_H
