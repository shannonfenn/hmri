/* 
 * File:   Annealer.h
 * Author: Shannon
 *
 * Created on 4 November 2013, 1:55 PM
 */

#include <vector>
#include <map>
#include <random>
#include <iostream>
#include <fstream>
#include "Optimiser.h"

#ifndef ANNEALER_H
#define	ANNEALER_H

using namespace BitError;

class Annealer : public Optimiser {
public:
    Annealer();
    Annealer(map<string, size_t> posIntParams, map<string, double> realParams);

    virtual ~Annealer();

//    Result run(size_t numTemps, size_t stepsPerTemp, double initTemp,
//               double tempRate,
//               BooleanNetworkNAND seed,
//               ostream& errorLog,
//               ostream& progressLog,
//               Metric guidingMetric);


    void setParameters(map<string, size_t> posIntParams, map<string, double> realParams);

    Result run(BooleanNetworkNAND seed, Metric guidingMetric, bool logErrors=false);

//    double getAcceptanceRate() const { return mean(acceptanceAcc); }
//    double getNegativeAcceptanceRate() const { return mean(negativeAcceptanceAcc); }
//    double getNonNegativeAcceptanceRate() const { return mean(nonNegativeAcceptanceAcc); }

private:
    bool accept(double oldError, double newError, double temperature);

    size_t numTemps,
           stepsPerTemp,
           finalNg;
    double initTemp,
           tempRate;
//    accumulator_set<double, stats<tag::mean> > acceptanceAcc;
//    accumulator_set<double, stats<tag::mean> > negativeAcceptanceAcc;
//    accumulator_set<double, stats<tag::mean> > nonNegativeAcceptanceAcc;
};

#endif	/* ANNEALER_H */

