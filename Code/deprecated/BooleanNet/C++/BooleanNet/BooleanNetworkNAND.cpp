#include "BooleanNetworkNAND.h"
#include <algorithm>
#include <functional>   // std::plus
#include <string>

// FOR GRAPHML
#include "boost/graph/directed_graph.hpp"
#include "boost/graph/graphml.hpp"

BooleanNetworkNAND::BooleanNetworkNAND(size_t numInputs, size_t numOutputs, 
                                       vector<pair<size_t,size_t> > initialGates, 
                                       vector<vector<char> > inputs, vector<vector<char> > expectedOutputs) :
    Ni(numInputs),
    No(numOutputs),
    evaluated(false),
    firstChangedGate(0),
    gates(initialGates)
{
    if(numInputs < 1) {
        throw invalid_argument("BooleanNetworkNAND - Invalid number of inputs.");
    }
    if(numOutputs < 1 || numOutputs > initialGates.size()) {
        throw invalid_argument("BooleanNetworkNAND - Invalid number of outputs - either negative, none or more outputs than gates.");
    }
    if(initialGates.empty()) {
        throw invalid_argument("BooleanNetworkNAND - Empty initial gate list.");
    }
    setExamples(inputs, expectedOutputs);
}

//BooleanNetworkNAND::BooleanNetworkNAND(const BooleanNetworkNAND& orig) :
//    Ni(orig.Ni),
//    No(orig.No),
//    evaluated(orig.evaluated),
//    firstChangedGate(orig.firstChangedGate),
//    inverseMoves(orig.inverseMoves),
//    gates(orig.gates),
//    stateVectors(orig.stateVectors),
//    expectations(orig.expectations),
//    errors(orig.errors)
//{
//}

BooleanNetworkNAND::~BooleanNetworkNAND() {
}

void BooleanNetworkNAND::addGates(size_t N) {
    evaluated = false;
    clearHistory();

    size_t Ng = gates.size();

    // If the first changed gate is in the region to be moved then
    // move the index to the first gate of this region of new gates
    // this ensures new gates are evaluated. Even though currently they
    // are not connected to the output they may eventually be and if they
    // are not evaluated now they may not be in the future. (Not sure about
    // this?)
    if(firstChangedGate >= Ng - No) {
        firstChangedGate = Ng - No;
    }
    
    // resize gate array
    gates.resize(Ng + N);

    // copy output gates to end
    // must be done in reverse order in case N < No - in this case 
    // the output gate region overlaps the new gate region
    for(size_t o=No; o!=0; o--) {
        // Move the gate to it's new position
        gates[Ng - No + N + o - 1] = gates[Ng - No + o - 1];
        
        // If the gate takes any gates to be moved as it's input
        // then shift that index by the number of inserted gates
        if(gates[Ng - No + N + o - 1].first >= Ni + Ng - No) {
            gates[Ng - No + N + o - 1].first += N;
        }
        if(gates[Ng - No + N + o - 1].second >= Ni + Ng - No) {
            gates[Ng - No + N + o - 1].second += N;
        }
    }
    
    // randomize inserted gates
    // Not done now - gates will be equal to the original gates

    // expand the state vectors by the number of included gates
    // no need to shift state values since the states will be re-evaluated
    // from before the new gate region anyway
    for(auto& state : stateVectors) {
        state.resize(state.size() + N);
    }
}

//vector<char> BooleanNetworkNAND::evaluate(vector<char> input) const {
//    if(input.size() != Ni) {
//        throw BooleanNetworkNANDException(" - Input length must be equal to network input size (Ni)");
//    }

//    //use the input as the state
//    input.resize(Ni + gates.size());

//    for(size_t g=0; g < gates.size(); g++) {
//        input[Ni + g] = ! ( input[gates[g].first] && input[gates[g].second] );
//    }

//    return vector<char>(input.end() - No, input.end());
//}

/*
 *  Evaluates the network from the first gate given an initial state vector.
 */
//void BooleanNetworkNAND::evaluate(vector<char>& stateVector) const {
//    if(stateVector.size() != Ni + gates.size()) {
//        throw invalid_argument("BooleanNetworkNAND - State vector length must be equal to network input size (Ni) plus number of gates (Ng)");
//    }

//    for(size_t g=0; g < gates.size(); g++) {
//        stateVector[Ni + g] = ! ( stateVector[gates[g].first] && stateVector[gates[g].second] );
//    }
//}

/*
 *  Evaluates the network from the gate that was changed in the last move given an initial state vector.
 */
//void BooleanNetworkNAND::evaluateFromChangedGate(vector<char>& stateVector) const {
//    if(stateVector.size() != Ni + gates.size()) {
//        throw BooleanNetworkNANDException(" - State vector length must be equal to network input size (Ni) plus number of gates (Ng)");
//    }

//    for(size_t g=changedGate; g < gates.size(); g++) {
//        stateVector[Ni + g] = ! ( stateVector[gates[g].first] && stateVector[gates[g].second] );
//    }
//}


void BooleanNetworkNAND::evaluate() {
    
    if(not evaluated) {

        size_t stateStart = firstChangedGate;
        size_t outputStart = 0;
        
        // If the gate modified is one of the output gates we don't want to be 
        // re-evaluating the error matrix for outputs before that one
        if(stateStart > gates.size() - No) {
            outputStart = stateStart + No - gates.size();
        }
        
//        // For each example
//        for(size_t ex = 0; ex < stateVectors.size(); ex++) {
//            // Re-evaluate state
//            for(size_t g = stateStart; g < gates.size(); g++) {
//                stateVectors[ex][Ni + g] = ! ( stateVectors[ex][gates[g].first] && stateVectors[ex][gates[g].second] );
//            }
            
//            // Re-evaluate error matrix
//            auto stateOutStart = stateVectors[ex].begin() + Ni + gates.size() - No;
//            for (size_t o = outputStart; o < No; o++) {
//                errors[ex][o] = (stateOutStart[o] != expectations[ex][o]);
//            }
//        }
        
        auto state_it = stateVectors.begin();
        auto error_it = errors.begin();
        auto expec_it = expectations.begin();
        for(; state_it != stateVectors.end(); state_it++, error_it++, expec_it++) {
            // Re-evaluate state
//            for(size_t g = stateStart; g < gates.size(); g++) {
            auto g = gates.begin() + stateStart;
            auto s_it = state_it->begin() + Ni + stateStart;
            for(; g!= gates.end(); g++, s_it++) {
//                (*state_it)[Ni + g] = ! ( (*state_it)[gates[g].first] && (*state_it)[gates[g].second] );
                *(s_it) = ! ( (*state_it)[g->first] && (*state_it)[g->second] );
            }
            
            // Re-evaluate error matrix
            auto er_it = error_it->begin() + outputStart;
            auto st_it = state_it->begin() + Ni + gates.size() - No + outputStart;
            auto ex_it = expec_it->begin() + outputStart;
            for (;
                 er_it < error_it->end();
                 er_it++,
                 st_it++,
                 ex_it++) {
                (*er_it) = ( (*st_it) != (*ex_it) );
            }
//            auto stateOutStart = stateVectors[ex].begin() + Ni + gates.size() - No;
//            for (size_t o = outputStart; o < No; o++) {
//                (*error_it)[o] = (stateOutStart[o] != (*expec_it)[o]);
//            }
        }
        
        evaluated = true;
    }
}

double BooleanNetworkNAND::accuracy() {
    evaluate();

    double numCorrect = 0.0;

    for(const auto& row : errors) {
        if( all_of(row.begin(), row.end(), [](size_t i) { return i==0; }) ) {
            numCorrect += 1.0;
        }
    }

    return numCorrect / errors.size();
}

double BooleanNetworkNAND::error(Metric metric) {
    evaluate();
       
    return BitError::metricValue(errors, metric);
}

vector<double> BooleanNetworkNAND::errorPerBit() {
    evaluate();
    vector<double> bitErrs(No, 0.0);
    for(const auto& row : errors) {
        transform(bitErrs.begin(), bitErrs.end(), row.begin(), bitErrs.begin(), plus<double>());
    }
    size_t Ne = errors.size();
    transform(bitErrs.begin(), bitErrs.end(), bitErrs.begin(), [Ne](double d) { return d / Ne; });
    return bitErrs;
}

vector<vector<char> > BooleanNetworkNAND::errorMatrix() {
     evaluate();
                     
     return errors;                      
}

vector<size_t> BooleanNetworkNAND::maxDepthPerBit() const {
    vector<size_t> depths(gates.size() + Ni, 0);
    size_t d1, d2;
   
    for (size_t g = 0; g < gates.size(); g++) {
        d1 = depths[gates[g].first];
        d2 = depths[gates[g].second];
        
        depths[g + Ni] = max(d1, d2) + 1;
    }
    
    return vector<size_t>(depths.end() - No, depths.end());
}

void BooleanNetworkNAND::setExamples(vector<vector<char> > inputs, vector<vector<char> > expectedOutputs) {

    if(inputs.size() != expectedOutputs.size()) {
        throw invalid_argument("BooleanNetworkNAND::setExamples - number of input examples does not match number of output examples.");
    }

    // Initially copy over 
    stateVectors = move(inputs);
    expectations = move(expectedOutputs);
    errors = vector<vector<char>>(expectations.size(), vector<char>(No, 0));
        
    for(auto& state : stateVectors) {
        state.resize(Ni + gates.size());
    }
    
    // state vectors have been reset, so start point must change as well
    evaluated = false;
    clearHistory();
    firstChangedGate = 0;
}

void BooleanNetworkNAND::pertubExamples(default_random_engine &generator) {
    vector<size_t> indices(stateVectors.size());
    vector<vector<char>> stateVectorsCopy(stateVectors.size());
    vector<vector<char>> expectationsCopy(expectations.size());
    vector<vector<char>> errorsCopy(errors.size());
    
    evaluated = false;
    
    for(size_t i=0; i<stateVectors.size(); i++)
        indices[i] = i;
    shuffle(indices.begin(), indices.end(), generator);
    
    for(size_t i=0; i<stateVectors.size(); i++) {
        stateVectorsCopy[i] = stateVectors[indices[i]];
        expectationsCopy[i] = expectations[indices[i]];
        errorsCopy[i] = errors[indices[i]];
    }
    
    stateVectors = stateVectorsCopy;
    expectations = expectationsCopy;
    errors = errorsCopy;
}


BooleanNetworkNAND::Move BooleanNetworkNAND::getRandomMove(default_random_engine& generator) const {
    Move move;
    move.gate = uniform_int_distribution<size_t>(0, gates.size() - 1)(generator);

    // pick single random input connection to move
    move.input = bernoulli_distribution()(generator);
    // decide how much to shift the gate (gate can only connect to previous gate or input)
    size_t shift = uniform_int_distribution<size_t>(1, move.gate + Ni - 1)(generator);
    // Get location of the original connection
    size_t previousConnection = (move.input ? gates.at(move.gate).second : gates.at(move.gate).first);
    // Get shifted connection
    move.connection = (previousConnection + shift) % (move.gate + Ni);

    return move;
}

void BooleanNetworkNAND::moveToNeighbour(const Move& move) {
    if(evaluated)
        firstChangedGate = move.gate;
    else
        firstChangedGate = min(firstChangedGate, move.gate);

    // Record the inverse to this move
    Move inverse;
    inverse.gate = move.gate;
    inverse.connection = (move.input ? gates.at(move.gate).second : gates.at(move.gate).first);
    inverse.input = move.input;
    inverseMoves.push_back(inverse);

    // actually modify the connection
    if(move.input) {
        // true (1) means second connection
        gates.at(move.gate).second = move.connection;
    }
    else {
        // false (0) means first
        gates.at(move.gate).first = move.connection;
    }

    // indicate that the network must be reevaluated
    evaluated = false;
}

void BooleanNetworkNAND::revertMove() {
    if(not inverseMoves.empty()) {
        auto inverse = inverseMoves.back();

        // indicate re-evaluation is needed
        evaluated = false;

        // change first or second connection
        if(inverse.input)
            gates.at(inverse.gate).second = inverse.connection;
        else
            gates.at(inverse.gate).first = inverse.connection;

        inverseMoves.pop_back();
    }
    else {
        throw logic_error("BooleanNetworkNAND - tried to revert with empty move stack.");
    }
}

void BooleanNetworkNAND::revertAllMoves() {
    while(not inverseMoves.empty()) {
        auto inverse = inverseMoves.back();

        // indicate re-evaluation is needed
        evaluated = false;

        // change first or second connection
        if(inverse.input)
            gates.at(inverse.gate).second = inverse.connection;
        else
            gates.at(inverse.gate).first = inverse.connection;

        inverseMoves.pop_back();
    }
}

void BooleanNetworkNAND::clearHistory() {
    inverseMoves.clear();
}

//vector<vector<char> > BooleanNetworkNAND::getTruthTable() const {
//    vector<char> inp(Ni);
//    size_t numInputs = pow(2, Ni);
//    vector<vector<char>> table(numInputs);

//    for(size_t i=0; i<numInputs; i++) {
//        //generate next input
//        for (size_t b = 0; b < Ni; b++) {
//            inp.at(Ni - 1 - b) = (1 << b & i) != 0;
//        }

//        table.at(i) = evaluate(inp);
//    }

//    return table;
//}

vector<vector<char> > BooleanNetworkNAND::getFullStateTableForSamples() {
    evaluate();

    return stateVectors;
}

vector<vector<char> > BooleanNetworkNAND::getTruthTable() const {
    vector<char> state(Ni + gates.size());
    size_t numInputs = pow(2, Ni);
    vector<vector<char>> table;
    table.reserve(numInputs);

    for(size_t i=0; i<numInputs; i++) {
        // Generate next input
        for (size_t b = 0; b < Ni; b++) {
            state.at(Ni - 1 - b) = (1 << b & i) != 0;
        }
        // Evaluate network state vector
        for(size_t g = 0; g < gates.size(); g++) {
            state[Ni + g] = ! ( state[gates[g].first] && state[gates[g].second] );
        }

        // Copy output gate states to table
        table.push_back(vector<char>(state.end() - No, state.end()));
    }

    return table;
}

void BooleanNetworkNAND::writeGML(ostream& out) const {
    out << "graph" << endl;
    out << "[ hierarchic 0" << endl;
    out << "directed 1" << endl;
    for (int i = 0; i < (int)Ni; i++) {
        out << "node\n[ id " << i << endl;
        out << "graphics\n[ x " << i*100 << endl;
        out << "y -100.0\n]" << endl;
        out << "LabelGraphics\n[ text \"Input - " << i << "\" ]" << endl;
        out << "]" << endl;
    }

    for (int g = 0; g < (int)gates.size(); g++) {
        out << "node\n[ id " << g + Ni << endl;
        out << "LabelGraphics\n[ text \"NAND - " << g + Ni <<"\" ]" << endl;
        out << "]" << endl;
    }


    for (int g = 0; g < (int)gates.size(); g++) {
        out << "edge\n[ source " << gates[g].first << endl;
        out << "target " << g + Ni << " ]" << endl;
        out << "edge\n[ source " << gates[g].second << endl;
        out << "target " << g + Ni << " ]" << endl;
    }
    out << "]" << endl;
}

void BooleanNetworkNAND::writeGraphML(ostream &out) const {
    typedef struct Vertex
    {
       int colour;
       string node_type;
       
    }vertex_container;

    typedef boost::directed_graph<vertex_container> Graph;
    typedef boost::graph_traits<Graph>::vertex_descriptor vertex_descriptor;

    Graph graph;

    vector<vertex_descriptor> vertices;

    for (int i = 0; i < (int)Ni; i++) {
        vertices.push_back( graph.add_vertex({1, "input"}) );
    }

    for (int g = 0; g < (int)gates.size(); g++) {
        vertices.push_back( graph.add_vertex({2, "NAND"}) );
        graph.add_edge( vertices.at(gates[g].first), vertices.back());
        graph.add_edge( vertices.at(gates[g].second), vertices.back());
    }
    
    boost::dynamic_properties dp;
    dp.property("node_id", get(boost::vertex_index, graph));
    dp.property("color", get(&vertex_container::colour, graph));
    dp.property("node_type",get(&vertex_container::node_type, graph));
    
    boost::write_graphml(out, graph, dp);
}

//BooleanNetworkNAND BooleanNetworkNAND::operator =(BooleanNetworkNAND other) {
    
//}

bool operator==(BooleanNetworkNAND lhs, BooleanNetworkNAND rhs) {
    return lhs.Ni == rhs.Ni && lhs.No == rhs.No && lhs.gates == rhs.gates;
}

bool operator!=(BooleanNetworkNAND lhs, BooleanNetworkNAND rhs) {
    return !(lhs == rhs);
}

ostream& operator<<(ostream& out, BooleanNetworkNAND net) {
    out << "Ng: " << net.gates.size() << " Ni: " << net.Ni << " No: " << net.No << endl;

    for(size_t i=0; i< net.gates.size(); i++) {
        out << "(" << i+net.Ni << "):\t";
        out << "[" << net.gates[i].first << ", " << net.gates[i].second << "]\t";
        out << endl;
    }

    return out;
}
