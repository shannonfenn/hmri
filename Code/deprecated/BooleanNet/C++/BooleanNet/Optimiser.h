#ifndef OPTIMISER_H
#define OPTIMISER_H

#include <vector>
#include <map>
#include <exception>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include "BitError.hpp"
#include "BooleanNetworkNAND.h"

using namespace boost::accumulators;

class Optimiser {
public:
    struct Result {
        BooleanNetworkNAND network;
        size_t iterationBest;
        size_t iterationFinish;
    };

public:
    Optimiser();
    virtual ~Optimiser();

    virtual void setParameters(map<string, size_t> posIntParams, map<string, double> realParams) = 0;
    virtual Result run(BooleanNetworkNAND seed, Metric guidingMetric, bool logErrors=false) = 0;

//    vector<vector<char> > evaluateOnExamples(const BooleanNetworkNAND& net, const vector< pair<vector<char>, vector<char> > >& examples);

//    double calculateError(const list<pair<vector<char>, vector<char> > >& examples, Metric metric = Metric::SIMPLE);
    
protected:
    std::default_random_engine generator;
};



#endif // OPTIMISER_H
