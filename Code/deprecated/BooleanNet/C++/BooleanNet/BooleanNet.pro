#-------------------------------------------------
#
# Project created by QtCreator 2013-12-05T09:52:53
#
#-------------------------------------------------

#QT  += core
#QT  -= gui

QMAKE_CXXFLAGS += -std=c++11

TARGET = BooleanNet


SOURCES += main.cpp\
    BooleanNetworkNAND.cpp \
    Annealer.cpp \
    Optimiser.cpp \
    TabuSearcher.cpp \
    BitError.cpp \
    geneticalgorithm.cpp \
    orderingevaluator.cpp

HEADERS  += \
    Annealer.h \
    BooleanNetworkNAND.h \
    Optimiser.h \
    TabuSearcher.h \
    BitError.hpp \
    geneticalgorithm.h \
    orderingevaluator.h \
    VectorTools.h
