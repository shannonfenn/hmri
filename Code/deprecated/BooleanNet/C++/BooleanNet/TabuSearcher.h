#ifndef TABUSEARCHER_H
#define TABUSEARCHER_H

#include <vector>
#include "Optimiser.h"
#include "BooleanNetworkNAND.h"

using namespace std;

class TabuSearcher : public Optimiser
{
public:
    TabuSearcher();
    TabuSearcher(map<string, size_t> posIntParams, map<string, double> realParams);
    virtual ~TabuSearcher();

//    Result run(size_t maxIterations,
//               size_t coolDownCount,
//               size_t movesPerIteration,
//               BooleanNetworkNAND seed,
//               ostream& errorLog,
//               ostream& progressLog,
//               Metric guidingMetric = Metric::SIMPLE);

    void setParameters(map<string, size_t> posIntParams, map<string, double> realParams);

    Result run(BooleanNetworkNAND seed, Metric guidingMetric, bool logErrors=false);

private:
    size_t maxIterations,
           coolDownCount,
           movesPerIteration;
};

#endif // TABUSEARCHER_H
