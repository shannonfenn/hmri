/* 
 * File:   main.cpp
 * Author: shannon
 *
 * Created on 28 September 2013, 3:55 PM
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <chrono>
#include <random>
#include <algorithm>
#include <string>
#include <algorithm>

#include "BooleanNetworkNAND.h"

#include "Annealer.h"
#include "TabuSearcher.h"
#include "VectorTools.h"

using namespace std;

/*
 *  printTable - prints a table for the given error metric for disagreement values from 0000 -> 1111
 */
void printTable(BitError::Metric metric) {
    cout << "| ";
    for(int i=0; i<16; i++) {
        vector<vector<char>> err(1, toBinary(i, 4));
        double e = BitError::metricValue(err, metric);
        cout << toBinary(i,4) << " | " << e << " | ";
        if(i==7)
            cout << endl << "| ";
    }
    cout << endl;
}


vector<pair<size_t, size_t> > getRandomConnectionList(size_t Ni, size_t Ng, default_random_engine& generator) {
    vector<pair<size_t, size_t> > initialGates;

    for(size_t g=0; g<Ng; g++) {
        size_t connection1 = std::uniform_int_distribution<>(0, g + Ni - 1)(generator);
        size_t connection2 = std::uniform_int_distribution<>(0, g + Ni - 1)(generator);

        initialGates.push_back( make_pair(connection1, connection2) );
    }

    return initialGates;
}


bool parseArgs(int argc, char* argv[], size_t& Ng,
               map<string, size_t>& posIntArgs, map<string, double>& realArgs,
               BitError::Metric& metric, string& resultFileName, string& infoLog) {

    string metricName;

    bool validArgs = true;
    if(argc < 6)
        return false;

    try {

        Ng = stoi(argv[1]); // This is independant of method
        infoLog = "";

        if(string(argv[0]).compare("anneal") == 0) {
            //annealing
            posIntArgs["numTemps"] = stoi(argv[2]);
            posIntArgs["stepsPerTemp"] = stoi(argv[3]);
            realArgs["initTemp"] = stod(argv[4]);
            realArgs["tempRate"] = stod(argv[5]);
        }
        else if(string(argv[0]).compare("ganneal") == 0) {
            //annealing
            posIntArgs["finalNg"] = stoi(argv[2]);
            posIntArgs["numTemps"] = stoi(argv[3]);
            posIntArgs["stepsPerTemp"] = stoi(argv[4]);
            realArgs["initTemp"] = stod(argv[5]);
            realArgs["tempRate"] = stod(argv[6]);
        }
        else if(string(argv[0]).compare("tabu") == 0) {
            posIntArgs["maxIterations"] = stoi(argv[2]);
            posIntArgs["coolDownCount"] = stoi(argv[3]);
            posIntArgs["movesPerIteration"] = stoi(argv[4]);
        }
        else {
            return false;
        }

        size_t sharedArgPos = posIntArgs.size() + realArgs.size() + 2;

        metricName = argv[sharedArgPos];

        switch(argc - sharedArgPos) {
        case 3:
            infoLog = argv[sharedArgPos + 2];
        case 2:
            resultFileName = argv[sharedArgPos + 1];
            break;
        case 1:
            break;
        default:
            return false;
        }

        if(validArgs) {
            transform(metricName.begin(), metricName.end(), metricName.begin(), ::tolower);
            metric = metricFromName(metricName);
        }
    }
    catch(invalid_argument e) {
        cerr << e.what() << endl;
        return false;
    }
    catch(...) {
        return false;
    }

    return true;
}


map<size_t, vector<char> > loadDataSet(istream& in, size_t& Ni, size_t& No) {
    map<size_t, vector<char> > goal;

    in >> Ni >> No;

    while(in.good()) {
        vector<char> ibits(Ni);
        vector<char> obits(No);
        bool b;

        for(size_t i=0; i<Ni; i++) {
            in >> b;
            ibits.at(i) = b;
        }
        for(size_t i=0; i<No; i++) {
            in >> b;
            obits.at(i) = b;
        }

        if(not in.eof()) {
            size_t input = fromBinary(ibits);
            if(goal.count(input) > 0) {
                if(goal[input] != obits) {
                    cerr << "Same input exists twice with different outputs - in: " << ibits << " o1: " << goal[input] << " o2: " << obits << ". First input overwritten." << endl;
                }
            }
            // assign output
            goal[input] = obits;
        }
    }

    return goal;
}


/*
 *  runs optimiser on the binary addition problem with given samples.
 */
void givenSamples(Optimiser& opt, map<size_t, vector<char> > goal, vector<size_t> trainingIndices,
                  size_t Ni, size_t No, size_t Ng,
                  BitError::Metric metric, ostream &infoLog, ofstream& resultStream)
{
    vector<vector<char>> trainingInputs;
    vector<vector<char>> trainingOutputs;
    vector<vector<char>> fullInputs;
    vector<vector<char>> fullOutputs;

    for(const auto& kv : goal) {
        fullInputs.push_back( toBinary(kv.first, Ni) );
        fullOutputs.push_back( kv.second );
    }

    infoLog << "Examples represent " << trainingIndices.size() * 100.0 / goal.size() << "% of possible examples" << endl;

    cout << "\"Examples\": {";
    for(auto tri = trainingIndices.begin(); tri < trainingIndices.end() - 1; tri++) {
        cout << "\"" << toBinary(*tri, Ni) << "\": \"" << goal[*tri] << "\", ";
    }
    cout << "\"" << toBinary(trainingIndices.back(), Ni) << "\": \"" << goal[trainingIndices.back()] << "\"}";
    
    // Generate example input-output pairs
    for(auto tr : trainingIndices) {
        trainingInputs.push_back( toBinary(tr, Ni) );
        trainingOutputs.push_back( goal[tr] );
        infoLog << tr << " ";
    }
    infoLog << std::endl;

    auto start = std::chrono::steady_clock::now();

    //Generate Random Seed
    random_device rd;  // Seed with a real random value, if available
    default_random_engine generator(rd());
    BooleanNetworkNAND seed(Ni, No, getRandomConnectionList(Ni, Ng, generator), trainingInputs, trainingOutputs);

    //run annealing algorithm
    Optimiser::Result result = opt.run(seed, metric);
    
    BooleanNetworkNAND finalNetworkTrg = result.network;
    BooleanNetworkNAND finalNetworkFull(finalNetworkTrg);
    finalNetworkFull.setExamples(fullInputs, fullOutputs);

//    finalNetworkTrg.writeGraphML(resultStream);
    finalNetworkTrg.writeGML(resultStream);

    auto end = chrono::steady_clock::now();    
    
    infoLog << finalNetworkTrg << std::endl;

    infoLog << "Training Guiding Error: " << finalNetworkTrg.error(metric) << std::endl;
    infoLog << "Training Objective Error: " << finalNetworkTrg.error() << std::endl;

    infoLog << "Full Guiding Error: " << finalNetworkFull.error(metric) << std::endl;
    infoLog << "Full Objective Error: " << finalNetworkFull.error() << std::endl;

    infoLog << "Duration: " << chrono::duration<double>(end - start).count() << "s" << endl;
    
    cout << ", \"results\": {\"metric\" : \"" << metric << "\",";
    cout << "\"Training Error (guiding)\" : " << finalNetworkTrg.error(metric) << ", ";
    cout << "\"Training Error (simple)\" : " << finalNetworkTrg.error()  << ", ";
    cout << "\"Full Error (guiding)\" : " << finalNetworkFull.error(metric) << ", ";
    cout << "\"Full Error (simple)\" : " << finalNetworkFull.error() << ", ";
    cout << "\"Training Accuracy\" : " << finalNetworkTrg.accuracy() << ", ";
    cout << "\"Full Accuracy\" : " << finalNetworkFull.accuracy() << ", ";
    cout << "\"Iteration for best\" : " << result.iterationBest << ", ";
    cout << "\"Total iterations\" : " << result.iterationFinish << ", ";

    cout << "\"Training Error per bit\" : " << finalNetworkTrg.errorPerBit() << ", ";
    cout << "\"Full Error per bit\" : " << finalNetworkFull.errorPerBit() << ", ";
    cout << "\"Max Depth per bit\" : " << finalNetworkFull.maxDepthPerBit() << ", ";    
    cout << "\"time\" : " << chrono::duration<double>(end - start).count() << "}" << endl;
    cout << ", \"Final Network\" : " << finalNetworkTrg.getGates() << "}" << endl;
}


void randomSamples(Optimiser& opt, map<size_t, vector<char> > goal, size_t Ni, size_t No, size_t Ne, size_t Ng,
           BitError::Metric metric, ostream &infoLog, ofstream& resultStream)
{
    vector<size_t> trainingIndices;
    random_device rd;  // Seed with a real random value, if available

    for(const auto& kv : goal)
        trainingIndices.push_back(kv.first);

    shuffle(trainingIndices.begin(), trainingIndices.end(), default_random_engine(rd()));

    if(Ne < trainingIndices.size())
        trainingIndices.resize(Ne);

    givenSamples(opt, goal, trainingIndices, Ni, No, Ng, metric, infoLog, resultStream);
}


void leaveOneOutCV(Optimiser& opt, map<size_t, vector<char> > goal, size_t Ni, size_t No, size_t Ng, BitError::Metric metric, ostream &infoLog)
{
    vector<vector<char>> trainingInputs;
    vector<vector<char>> trainingOutputs;
    vector<vector<char>> fullInputs;
    vector<vector<char>> fullOutputs;
    vector<double> errors;
    random_device rd;  // Seed with a real random value, if available
    default_random_engine generator(rd());

    for(auto& kv : goal) {
        fullInputs.push_back( toBinary(kv.first, Ni) );
        fullOutputs.push_back( kv.second );
    }

    infoLog << "Leave-one-out cross-validation: " << endl;

    auto start = std::chrono::steady_clock::now();

    // Run with every example as a test
    for(size_t t=0; t<goal.size(); t++) {
        // Generate training set without test case
        auto testInp = trainingInputs[t];
        auto testOut = trainingOutputs[t];
        auto posInp = trainingInputs.erase(trainingInputs.begin() + t);
        auto posOut = trainingOutputs.erase(trainingOutputs.begin() + t);

        //Generate Random Seed
        BooleanNetworkNAND seed(Ni, No, getRandomConnectionList(Ni, Ng, generator), trainingInputs, trainingOutputs);

        // Run annealing algorithm
        Optimiser::Result result = opt.run(seed, metric);

        // Generate a copy of the resulting network but for evaluation on test sample
        BooleanNetworkNAND testNetwork(result.network);
        testNetwork.setExamples(vector<vector<char>>(1, testInp), vector<vector<char>>(1, testOut));

        // Record the error on the test case
        errors.push_back(testNetwork.error());

        // Add sample back to training set
        trainingInputs.insert(posInp, testInp);
        trainingOutputs.insert(posOut, testOut);
    }

    auto end = chrono::steady_clock::now();
    infoLog << "Duration: " << chrono::duration<double>(end - start).count() << "s" << endl;
    cout << "time : " << chrono::duration<double>(end - start).count() << endl;

    double tot = 0;
    for(auto e : errors) {
        cout << e << " ";
        infoLog << e << endl;
        tot += e;
    }
    cout << endl;
    cout << tot / errors.size() << endl;
}

/*
 * Parses given arguments to run on data in given file.
 */
void runFromFile(int argc, char* argv[]) {
    size_t Ne=0, Ng, Ni, No;
    map<string, size_t> posIntParams;
    map<string, double> realParams;
    string infoLogName, resultFileName, samplesFileName;
    BitError::Metric metric;
    const string usageStr = "\tUsage: BooleanNet\toption inputfile [Ne|sampleFileName] optimiser Ng optimisationOptions metric [resultFileName [infoLogName]]\n";
    string option;
    string optimiserName;
    Optimiser* opt;
    map<size_t, vector<char> > goal;

    bool validArgs = argc > 6;

    if(validArgs) {

        option = argv[1];
        int shift = 3;

        if(option.compare("cv") == 0) {
            shift = 3;
        }
        else if(option.compare("randomsamples") == 0) {
            Ne = stoi(argv[3]);
            shift = 4;
        }
        else if(option.compare("givensamples") == 0) {
            samplesFileName = argv[3];
            shift = 4;
        }
        else {
            validArgs = false;
        }

        optimiserName = argv[shift];

        validArgs = validArgs && parseArgs(argc - shift, argv + shift, Ng, posIntParams, realParams, metric, resultFileName, infoLogName);

        if(validArgs) {
            if(optimiserName.compare("tabu") == 0) {
                opt = new TabuSearcher(posIntParams, realParams);
            }
            else if(optimiserName.compare("anneal") == 0) {
                posIntParams["finalNg"] = Ng;
                opt = new Annealer(posIntParams, realParams);
            }
            else if(optimiserName.compare("ganneal") == 0) {
                opt = new Annealer(posIntParams, realParams);
            }
            else {
                validArgs = false;
            }
        }
    }

    if(validArgs) {
        ifstream infile(argv[2]);
        ofstream infoLog(infoLogName);
        ofstream resultFile(resultFileName);
        
        if(not infile.is_open()) {
            cerr << "Failed to open " << argv[2] << " for reading. Terminating." << endl;
        }
        else if(not infoLog.is_open() and not infoLogName.empty()) {
            cerr << "Failed to open " << infoLogName << " for writing. Terminating." << endl;
        }
        else if(not resultFile.is_open() and not resultFileName.empty()) {
            cerr << "Failed to open " << resultFileName << " for writing. Terminating." << endl;
        }
        else {
            // Load goal
            goal = loadDataSet(infile, Ni, No);

            // Print running parameters to log
            for(const auto& kv : posIntParams)
                infoLog << " " << kv.first << ": " << kv.second;
            for(const auto& kv : realParams)
                infoLog << " " << kv.first << ": " << kv.second;
            infoLog << "Ni: " << Ni << "No: " << No << " Ng: " << Ng << " metric: " << metric;
            cout << "{\"Paramers\": { \"Optimiser\": \"" << optimiserName << "\", \"Ni\": " << Ni << ", \"No\": " << No << ", \"Ng\": " << Ng << ", \"metric\": \"" << metric << "\"";
            for(const auto& kv : posIntParams)
                cout << ", \"" << kv.first << "\": " << kv.second;
            for(const auto& kv : realParams)
                cout << ", \"" << kv.first << "\": " << kv.second;


            if(option.compare("cv") == 0) {
                infoLog << endl;
                cout << "}, " << endl;
                leaveOneOutCV(*opt, goal, Ni, No,  Ng, metric, infoLog);
            }
            else if(option.compare("randomsamples") == 0) {
                infoLog << " Ne: " << Ne << endl;
                cout << ", \"Ne\": " << Ne << "}, " << endl;
                randomSamples(*opt, goal, Ni, No, Ne, Ng, metric, infoLog, resultFile);
            }
            else if(option.compare("givensamples") == 0) {
                ifstream samplesFile(samplesFileName);
                if(samplesFile.is_open()) {
                    vector<size_t> samples;
                    size_t e;
                    while(samplesFile.good()) {
                        samplesFile >> e;
                        if(e >= goal.size())
                            throw invalid_argument("Example index larger than goal size.");
                        samples.push_back(e);
                    }

                    infoLog << " Ne: " << samples.size() << endl;
                    cout << ", \"Ne\": " << Ne << ", \"samples\": " << samples << "}, " << endl;

                    givenSamples(*opt, goal, samples, Ni, No, Ng, metric, infoLog, resultFile);
                }
                else {
                    cerr << "Failed to open " << samplesFileName << " for reading. Terminating." << endl;
                }
            }
        }
    }
    else {
        cerr << usageStr << endl;
    }
}

///*
// *  runs optimiser on the binary addition problem with given samples.
// */
//void optimiseSampleOrder(Optimiser& opt, map<size_t, vector<char> > goal,
//                         size_t Ni, size_t No, vector<size_t> trainingIndices, size_t Ng,
//                         BitError::Metric metric, ostream &infoLog)
//{
//    vector<vector<char>> fullInputs;
//    vector<vector<char>> fullOutputs;

//    // Generate input and output vectors
//    for(const auto& kv : goal) {
//        fullInputs.push_back( toBinary(kv.first, Ni) );
//        fullOutputs.push_back( kv.second );
//    }

//    // Generate example input-output pairs
//    for(auto tr : trainingIndices) {
//        infoLog << tr << " ";
//    }
//    infoLog << endl;

//    cout << metric << endl;
//    // THE FOLLOWING NEEDS TO BE MADE INTO THE GA - POSSIBLY IN SEPARATE FUNCTION

//}


int main(int argc, char* argv[]) {

    runFromFile(argc, argv);

//    vector<Optimiser::Metric> metrics = {Optimiser::Metric::SIMPLE,
//                                         Optimiser::Metric::WEIGHTED_LINEAR_MSB,
//                                         Optimiser::Metric::WEIGHTED_LINEAR_LSB,
//                                         Optimiser::Metric::HIERARCHICAL_LINEAR_MSB,
//                                         Optimiser::Metric::HIERARCHICAL_LINEAR_LSB,
//                                         Optimiser::Metric::WORST_SAMPLE_LINEAR_MSB,
//                                         Optimiser::Metric::WORST_SAMPLE_LINEAR_LSB};

//    for(auto metric : metrics) {

//        cout << metric << endl;
//        printTable(metric);
//        cout << endl;
//    }

////    pablo example

//    list<pair<vector<char>, vector<char> > > l;
//    l.push_back(make_pair(toBinary(0, 6), toBinary(0, 6)));
//    l.push_back(make_pair(toBinary(0, 6), toBinary(5, 6)));
//    l.push_back(make_pair(toBinary(0, 6), toBinary(8, 6)));
//    l.push_back(make_pair(toBinary(0, 6), toBinary(21, 6)));
//    l.push_back(make_pair(toBinary(0, 6), toBinary(37, 6)));
//    l.push_back(make_pair(toBinary(0, 6), toBinary(42, 6)));
//    l.push_back(make_pair(toBinary(0, 6), toBinary(42, 6)));

//    cout << Optimiser::Metric::SIMPLE << "\t\t" << Optimiser::calculateError(l, Optimiser::Metric::SIMPLE) << endl;
//    cout << Optimiser::Metric::HIERARCHICAL_LINEAR_LSB << " " << Optimiser::calculateError(l, Optimiser::Metric::HIERARCHICAL_LINEAR_LSB) << endl;

    return 0;
}
