/*
 * File:   BooleanNetworkNAND.h
 * Author: Shannon Fenn (shannon.fenn@gmail.com)
 *
 * Purpose: Represents a boolean network constructed only from 2 input NAND gates
 *
 * Created on 16 December 2013
 */

#ifndef BOOLEANNETWORKNAND_H
#define BOOLEANNETWORKNAND_H

#include <string>
#include <random>
#include <iostream>
#include <vector>
#include <deque>
#include "BitError.hpp"
#include "VectorTools.h"

using namespace std;
using namespace BitError;

class BooleanNetworkNAND
{
public:
    struct Move {
        size_t gate;
        size_t connection;
        bool input; // false means first
    };
    
public:
//    BooleanNetworkNAND(const BooleanNetworkNAND& orig);
    BooleanNetworkNAND(size_t numInputs, size_t numOutputs, 
                       vector<pair<size_t, size_t>> initialGates, 
                       vector<vector<char>> inputs, vector<vector<char>> expectedOutputs);
    virtual ~BooleanNetworkNAND();

    size_t getNi() const {return Ni;}
    size_t getNo() const {return No;}
    size_t getNg() const {return gates.size();}

    void addGates(size_t N=1);
    vector<pair<size_t, size_t> > getGates() const { return gates; }

//    vector<char> evaluate(vector<char> input) const;
//    void evaluate(vector<char>& stateVector) const;
//    void evaluateFromChangedGate(vector<char>& stateVector) const;
    
    double accuracy();
    double error(Metric metric = Metric::SIMPLE);
    vector<double> errorPerBit();
    vector<vector<char>> errorMatrix();

    vector<vector<char> > getFullStateTableForSamples();

    vector<size_t> maxDepthPerBit() const;
    
    void setExamples(vector<vector<char>> inputs, vector<vector<char>> expectedOutputs) ;
    void pertubExamples(default_random_engine& generator);
    
    vector<vector<char>> getTruthTable() const;

    Move getRandomMove(default_random_engine& generator) const;
    void moveToNeighbour(const Move& move);
    void revertMove();
    void revertAllMoves();
    void clearHistory();

    void writeGML(ostream &out) const;
    void writeGraphML(ostream& out) const;
    
//    BooleanNetworkNAND operator = (BooleanNetworkNAND other);
    
    friend bool operator == (BooleanNetworkNAND lhs, BooleanNetworkNAND rhs);
    friend bool operator != (BooleanNetworkNAND lhs, BooleanNetworkNAND rhs);
    friend ostream& operator << (ostream& out, BooleanNetworkNAND net);
private:
    BooleanNetworkNAND();
    void evaluate();
        
    size_t Ni;
    size_t No;
    
    bool evaluated;
    size_t firstChangedGate;

    deque<Move> inverseMoves;

    vector<pair<size_t, size_t> > gates;

//    list<pair<vector<char>, vector<char> > > stateExamplePairs;
    vector<vector<char>> stateVectors;
    vector<vector<char>> expectations;
    vector<vector<char>> errors;
};

#endif // BOOLEANNETWORKNAND_H
