#include "TabuSearcher.h"

TabuSearcher::TabuSearcher()
{
}

TabuSearcher::TabuSearcher(map<string, size_t> posIntParams, map<string, double> realParams)
{
    setParameters(posIntParams, realParams);
}

TabuSearcher::~TabuSearcher() {}

//Optimiser::Result TabuSearcher::run(size_t maxIterations, size_t coolDownCount, size_t movesPerIteration, BooleanNetworkNAND seed,
//                                     ostream& errorLog, ostream& progressLog, Metric guidingMetric) {

void TabuSearcher::setParameters(map<string, size_t> posIntParams, map<string, double> realParams) {
    // Unpack arguments
    // If any of the following do not exist the map container with throw std::out_of_range
    maxIterations = posIntParams.at("maxIterations");
    coolDownCount = posIntParams.at("coolDownCount");
    movesPerIteration = posIntParams.at("movesPerIteration");
}

Optimiser::Result TabuSearcher::run(BooleanNetworkNAND seed, Metric guidingMetric, bool logErrors) {
    BooleanNetworkNAND state(seed);
    BooleanNetworkNAND globalBestState(seed);
    size_t iteration = 0;
    size_t bestIteration = 0;
    double iterationError, globalBestError;
    vector<BooleanNetworkNAND::Move> moves(movesPerIteration);

    size_t Ng = state.getNg();
    size_t Ni = state.getNi();

    if(coolDownCount >= Ng*Ni*2 +Ng*(Ng+1) - movesPerIteration) {
        throw invalid_argument("TabuSearcher::run - Cool down count greater than number of moves available - movesPerIteration.");
    }

    // Tabu table is Ng x (Ng + Ni) x 2
    // each value is the iteration after which the move
    // will no longer be tabu
    vector<vector<vector<size_t> > > tabuTable(Ng);
    // Setup tabu table
    for(size_t col=0; col < Ng; col++) {
        // Each column has Ni + current gate number of rows each with two values
        tabuTable[col] = vector<vector<size_t> >(col + Ni, vector<size_t>(2, 0));
    }

    // Calculate initial error
    globalBestError = iterationError = state.error(guidingMetric);

    // print initial error to log
//    errorLog << iterationError << " " << state.error() << endl;

    // Repeat for given number of iterations stopping if examples are memorised.
    while( iteration < maxIterations and globalBestError > 0.0 ) {

//        if(iteration % (maxIterations/100) == 0) {
//            progressLog << iteration*100.0/maxIterations << "% complete" << endl;
//        }

        // Generate random moves
        moves.clear();
        while(moves.size() < movesPerIteration) {
            // Generate the move
            auto move = state.getRandomMove(generator);
            // Check if tabu
//            size_t tabuUntil = tabuTable[move.gate][move.connection][move.input];
            size_t tabuUntil = tabuTable.at(move.gate).at(move.connection).at(move.input);

            if( tabuUntil > iteration ) {
                // Move is tabu, check aspiration criteria
                // Actually move the state
                state.moveToNeighbour(move);
                // Calculate the error
                double error = state.error(guidingMetric);

                if(error < globalBestError) {
                    // Aspiration criteria met, keep this move regardless
                    moves.push_back(move);
                }

                // Revert the move so another neighbour can be checked
                state.revertMove();
            }
            else {
                // Not tabu so keep the move
                moves.push_back(move);
            }
        }

        // Now we have movesPerIteration random non-tabu or aspiring moves, pick the best
        BooleanNetworkNAND::Move bestMove;
        double bestMoveError = numeric_limits<double>::max();

        for (const auto& move : moves) {
            // Apply the move
            state.moveToNeighbour(move);
            // Calculate the error
            double newError = state.error(guidingMetric);
            if(newError < bestMoveError) {
                bestMoveError = newError;
                bestMove = move;
            }

            // Revert the move so the next move can be checked from the original state
            state.revertMove();
        }
        
        // Actually perform the move, and mark it as tabu
        iterationError = bestMoveError;
        state.moveToNeighbour(bestMove);
        tabuTable.at(bestMove.gate).at(bestMove.connection).at(bestMove.input) = iteration + coolDownCount;

        // Update global best
        if (iterationError < globalBestError) {
            bestIteration = iteration;
            globalBestError = iterationError;
            globalBestState = state;
        }

        // print latest error to log
//        errorLog << iterationError << " " << state.error() << endl;
        
        iteration++;
    }

//    errorLog.flush();
    return {globalBestState, bestIteration, iteration};
}
