TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -std=c++11

INCLUDEPATH += ../../ \
             ../

SOURCES += \
    ../../BooleanNet/BooleanNetworkNAND.cpp \
    main.cpp \
    ../../BooleanNet/BitError.cpp

HEADERS += \
    ../../BooleanNet/BooleanNetworkNAND.h \
    BooleanNetworkNANDTest.hpp \
    common.hpp \
    ../../BooleanNet/BitError.hpp \
    BitErrorTest.hpp

