import numpy as np
from timeit import timeit
import json
import yaml
from progress.bar import Bar
from collections import OrderedDict
import argparse
import sys
import os
sys.path.append(os.path.expanduser('~/HMRI/Code/Python/'))
from BoolNet.BitError import all_metrics, metric_name, Metric
from BoolNet.BooleanNetwork import BooleanNetwork
from experiment import pack_bool_matrix


def parse_arguments():
    parser = argparse.ArgumentParser(description='Rough profiling of high cost boolnet ops')
    parser.add_argument('--file',
                        type=argparse.FileType('r'),
                        default='profiling/speed_test_small.json')
    parser.add_argument('--evaluator', default='cy', choices=['py', 'cy', 'gpu'])

    return parser.parse_args()


def error_per_output(evaluator):
    # force reevaluation
    evaluator.network(0).force_reevaluation()
    return evaluator.error_per_output(0)


def metric_value(evaluator, metric):
    # net.force_reevaluation()
    return evaluator.metric_value(0, metric)


def truth_table(evaluator):
    # force reevaluation not needed
    return evaluator.truth_table(0)


def rand_move_revert(net):
    net.move_to_neighbour(net.random_move())
    net.revert_move()


def set_mask(net, sourceable, changeable):
    net.set_mask(sourceable, changeable)


def move_evaluate(net, evaluator, metric):
    net.move_to_neighbour(net.random_move())
    return evaluator.metric_value(0, metric)


if __name__ == '__main__':
    args = parse_arguments()

    test = json.load(args.file)

    if args.evaluator == 'gpu':
        from BoolNet.NetworkEvaluatorGPU import NetworkEvaluatorGPU
        from BoolNet.BitErrorGPU import IMPLEMENTED_METRICS
        evaluator_class = NetworkEvaluatorGPU
        metrics = IMPLEMENTED_METRICS
    elif args.evaluator == 'cy':
        import pyximport
        pyximport.install()
        from BoolNet.NetworkEvaluatorCython import NetworkEvaluatorCython
        evaluator_class = NetworkEvaluatorCython
        metrics = list(all_metrics())
    else:
        from BoolNet.NetworkEvaluator import NetworkEvaluator
        evaluator_class = NetworkEvaluator
        metrics = list(all_metrics())

    inputs = np.array(test['inputs'], dtype=np.byte)
    target = np.array(test['target'], dtype=np.byte)
    gates = np.array(test['gates'], dtype=np.uint32)

    evaluator = evaluator_class(inputs, target)

    net = BooleanNetwork(gates, inputs.shape[1], target.shape[1])
    evaluator.add_network(net)

    Ng = net.Ng
    changeable = list(range(int(Ng/4), int(Ng/2)))
    sourceable = list(range(int(Ng/4)))

    REPEATS = 100

    results = dict()
    results['test file'] = args.file.name

    bar = Bar('Analysing', max=len(metrics) + 3)

    # results['truth table'] = timeit('truth_table(net)',
    #                         setup='from __main__ import truth_table, net',
    #                         number=3) / 3     # this is a slow operation
    # bar.next()

    for metric in metrics:
        results[metric_name(metric)] = timeit(
            'metric_value(evaluator, metric)',
            setup='from __main__ import metric_value, metric, evaluator',
            number=REPEATS) / REPEATS
        bar.next()

    results['move then revert'] = timeit(
        'rand_move_revert(net)',
        setup='from __main__ import rand_move_revert, net',
        number=REPEATS) / REPEATS
    bar.next()

    results['set mask'] = timeit(
        'set_mask(net, sourceable, changeable)',
        setup='from __main__ import set_mask, net, changeable, sourceable',
        number=REPEATS) / REPEATS
    bar.next()

    for metric in [Metric.E1, Metric.E6_LSB]:
        results['move then eval {}'.format(metric_name(metric))] = timeit(
            'move_evaluate(net, evaluator, metric)',
            setup='from __main__ import metric, net, evaluator, move_evaluate',
            number=REPEATS) / REPEATS
        bar.next()
    bar.finish()

    print(yaml.dump(results, default_flow_style=False))

# ######## CODE USED TO GENERATE ABOVE FILE ########## #
# inputs = np.random.randint(0, 2, (1024, 16))
# target = np.random.randint(0, 2, (1024, 16))
# gates = np.zeros((1000, 2), dtype=int)
# for g in range(len(gates)):
#     gates[g] = np.random.randint(0, g+16, 2)
# test = {'inputs': inputs.tolist(),
#         'target': target.tolist(),
#         'gates' : gates.tolist()}
# with open('speed_test_net.json', 'w') as fp:
#     json.dump(test, fp)
