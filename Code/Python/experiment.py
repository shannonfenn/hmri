from copy import deepcopy                   # for copying settings dicts
from collections import MutableMapping      # for updating nested dicts
from good import Schema, Required, In, All, Any, Range, Type, IsDir
from good import message, Allow
from BoolNet.learnBoolNet import LEARNERS, OPTIMISERS
from os.path import join
import numpy as np
import json


metrics = [
    'e1',
    'e2 lsb', 'e2 msb',
    'e3 lsb', 'e3 msb',
    'e4 lsb', 'e4 msb',
    'e5 lsb', 'e5 msb',
    'e6 lsb', 'e6 msb',
    'e7 lsb', 'e7 msb']


@message('2D array expected')
def is_2d(v):
    if len(v.shape) != 2:
        raise ValueError
    return v


@message('1D integer array expected')
def is_1d(v):
    if len(v.shape) != 1:
        raise ValueError
    return v


@message('Integer array expected')
def is_int_arr(v):
    if not np.issubdtype(v.dtype, np.integer):
        raise ValueError
    return v

SA_schema = Schema({
    'name':           'SA',
    'metric':         In(metrics),
    'num_temps':      All(int, Range(min=1)),
    'init_temp':      Range(min=0.0),
    'temp_rate':      Range(min=0.0, max=1.0),
    'steps_per_temp': All(int, Range(min=1))
    }, default_keys=Required)

SA_VN_schema = Schema({
    'name':             'SA-VN',
    'metric':           In(metrics),
    'num_temps':        All(int, Range(min=1)),
    'init_temp':        Range(min=0.0),
    'temp_rate':        Range(min=0.0, max=1.0),
    'steps_per_temp':   All(int, Range(min=1)),
    'init_move_count':  All(int, Range(min=1))
    }, default_keys=Required)

LAHC_schema = Schema({
    'name':             'LAHC',
    'metric':           In(metrics),
    'cost_list_length': All(int, Range(min=1)),
    'max_iterations':   All(int, Range(min=1))
    }, default_keys=Required)

LAHC_VN_schema = Schema({
    'name':             'LAHC-VN',
    'metric':           In(metrics),
    'cost_list_length': All(int, Range(min=1)),
    'max_iterations':   All(int, Range(min=1)),
    'init_move_count':  All(int, Range(min=1))
    }, default_keys=Required)

TS_schema = Schema({
    'name':             'TS',
    'metric':           In(metrics),
    'tabu_period':      All(int, Range(min=1)),
    'max_iterations':   All(int, Range(min=1))
    }, default_keys=Required)

optimiser_name_schema = Schema({
    'name':     In(OPTIMISERS.keys())
    }, extra_keys=Allow)

optimiser_schema = Schema(
    All(Any(SA_schema, SA_VN_schema, LAHC_schema, LAHC_VN_schema, TS_schema),
        optimiser_name_schema))

sampling_schema = Schema({
    'method':   In({'given', 'generated'}),
    'Ns':       All(int, Range(min=1)),
    'Ne':       All(int, Range(min=1))
    }, default_keys=Required)

config_schema = Schema({
    'name':                 str,
    'Ng':                   All(int, Range(min=1)),
    'dataset_dir':          IsDir(),
    'dataset':              All(str, lambda v: v.endswith('.json')),
    'logging':              In(['none', 'warning', 'info', 'debug']),
    'node_funcs':           In(['NAND', 'random']),
    'learner':              In(LEARNERS.keys()),
    'optimiser':            optimiser_schema,
    'sampling':             sampling_schema,
    'configuration_number': All(int, Range(min=0)),
    'training_set_number':  All(int, Range(min=0)),
    'inter_file_base':      str,
    'training_set':         All(Type(np.ndarray), is_2d),
    'test_set':             All(Type(np.ndarray), is_2d),
    'training_indices':     All(Type(np.ndarray), is_1d, is_int_arr)
    }, default_keys=Required)


class NumpyAwareJSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)


def pack_bool_matrix(mat):
    return np.packbits(mat.T, axis=1)


def unpack_bool_matrix(packed_mat, Ne):
    return np.unpackbits(packed_mat, axis=1)[:, :Ne].T


def dump_configurations(configurations, stream):
    first = True
    for conf in configurations:
        stream.write('[' if first else ', ')
        json.dump(conf[0], stream, cls=NumpyAwareJSONEncoder)
    stream.write(']')


def dump_results(results, stream):
    json.dump(results, stream, cls=NumpyAwareJSONEncoder)


def update_nested(d, u):
    ''' this updates a dict with another where the two may contain nested
        dicts themselves (or more generally nested mutable mappings). '''
    for k, v in u.items():
        if isinstance(v, MutableMapping):
            r = update_nested(d.get(k, {}), v)
            d[k] = r
        else:
            d[k] = u[k]
    return d


def partition_examples(examples, training_indices):
    ''' Parititions the given function into training and test sets,
        based on the given training indices.'''
    # Parameters
    N = examples.shape[0]
    Ns, Ne = training_indices.shape
    # Generate the test indices array, each row should contain all
    # indices not in the equivalent row of training_indices
    test_indices = np.zeros(shape=(Ns, N - Ne), dtype=int)
    for s in range(Ns):
        test_indices[s] = np.setdiff1d(np.arange(N), training_indices[s])
    # Using numpy's advanced indexing we can get the sets
    training_sets = examples[training_indices]
    test_sets = examples[test_indices]
    return training_sets, test_sets


def load_datasets(settings):
    # load data set from file
    dataset_dir = settings['dataset_dir']
    with open(join(dataset_dir, 'functions', settings['dataset'])) as ds_file:
        ds_settings = json.load(ds_file)
    sample_settings = settings['sampling']

    # load function
    function = np.array(ds_settings['function'])
    # load samples from file
    if sample_settings['method'] == 'given':
        # prepate filename
        sample_filename = join(dataset_dir, 'samples', 'samples_{}_{}_{}.json'.format(
            ds_settings['Ni'], sample_settings['Ns'], sample_settings['Ne']))
        # open and load file
        with open(sample_filename) as sample_file:
            training_indices = np.array(json.load(sample_file))
    # generate samples
    elif sample_settings['method'] == 'generated':
        Ns = sample_settings['Ns']
        Ne = sample_settings['Ne']
        # generate
        training_indices = np.random.randint(len(function), size=(Ns, Ne))
    else:
        raise ValueError('Invalid sampling method {}'.format(
                         sample_settings['method']))
    # partition the sets based on loaded indices
    training_sets, test_sets = partition_examples(function, training_indices)
    # add training sets to settings so they can be logged
    return training_sets, test_sets, training_indices


def generate_configurations(settings, evaluator_class):
    # CAUTION: Will modify the settings parameter!
    # the configurations approach involves essentially having
    # a new settings dict for each configuration and updating
    # it with values in each dict in the configurations list

    variable_sets = settings['configurations']
    # no need to keep this sub-dict around
    settings.pop('configurations')

    # Build up the task list
    tasks = []
    for conf_no, variables in enumerate(variable_sets):
        # keep each configuration isolated
        config_settings = deepcopy(settings)
        # update the settings dict with the values for this configuration
        update_nested(config_settings, variables)
        config_settings['configuration_number'] = conf_no

        # load the data for this configuration
        training_sets, test_sets, training_indices = load_datasets(config_settings)

        # for each training set
        for i in range(len(training_sets)):
            iteration_settings = deepcopy(config_settings)
            iteration_settings['training_indices'] = training_indices[i]
            iteration_settings['training_set_number'] = i
            iteration_settings['inter_file_base'] += '{}_{}_'.format(conf_no, i)
            iteration_settings['training_set'] = training_sets[i]
            iteration_settings['test_set'] = test_sets[i]

            config_schema(iteration_settings)

            # dump the iteration settings out
            tasks.append((iteration_settings, evaluator_class))
    return tasks
