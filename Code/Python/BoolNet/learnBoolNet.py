from datetime import datetime
from BoolNet.BooleanNetwork import BooleanNetwork
from BoolNet.RandomBooleanNetwork import RandomBooleanNetwork
from BoolNet.BitError import Metric, metric_from_name
from BoolNet.Learners import basic_learn, stratified_learn
import BoolNet.Optimisers as Optimisers
import numpy as np
import functools


OPTIMISERS = {
    'SA': Optimisers.SA(),
    'SA-VN': Optimisers.SA_VN(),
    'LAHC': Optimisers.LAHC(),
    'LAHC-VN': Optimisers.LAHC_VN(),
    'TS': Optimisers.TabuSearch()}


LEARNERS = {
    'basic': basic_learn,
    'stratified': functools.partial(stratified_learn, use_kfs_masking=False),
    'stratified kfs': functools.partial(stratified_learn, use_kfs_masking=True)}


def check_data(training_set, test_set):
    if training_set.shape[0] <= 0 or test_set.shape[0] <= 0:
        raise ValueError('dim of trg inputs/outputs do not match parameters.')
    if training_set.shape[1] != 2 or test_set.shape[1] != 2:
        raise ValueError
    if len(training_set[:, 0][0]) != len(test_set[:, 0][0]):
        raise ValueError
    if len(training_set[:, 1][0]) != len(test_set[:, 1][0]):
        raise ValueError


def learn_bool_net(parameters, evaluator_class):
    optimiser_name = parameters['optimiser']['name']
    learner_name = parameters['learner']
    Ng = parameters['Ng']
    metric = metric_from_name(parameters['optimiser']['metric'])
    training_set = parameters['training_set']
    test_set = parameters['test_set']

    check_data(training_set, test_set)

    Ni = len(training_set[:, 0][0])
    No = len(training_set[:, 1][0])

    # make evaluators for the training and test sets
    training_evaluator = evaluator_class(
        training_set[:, 0].tolist(),   # inputs
        training_set[:, 1].tolist())   # target
    test_evaluator = evaluator_class(
        test_set[:, 0].tolist(),   # inputs
        test_set[:, 1].tolist())   # target

    # generate random feedforward network
    initial_gates = np.empty(shape=(Ng, 2), dtype=np.int32)
    for g in range(Ng):
        initial_gates[g, :] = np.random.randint(g+Ni, size=2)
    # print(initial_gates < np.hstack((np.arange(Ng), np.arange(Ng))))

    # create the seed network
    if parameters['node_funcs'] == 'random':
        # generate a random set of transfer functions
        transfer_functions = np.random.randint(2, size=(Ng, 2, 2))
        initial_network = RandomBooleanNetwork(initial_gates, Ni, No, transfer_functions)
    elif parameters['node_funcs'] == 'NAND':
        initial_network = BooleanNetwork(initial_gates, Ni, No)
    else:
        raise ValueError('Invalid setting for \'transfer functions\': {}'.format(
            parameters['node_funcs']))

    # add the network to the training evaluator list
    training_evaluator.add_network(initial_network)

    learner = LEARNERS[learner_name]
    optimiser = OPTIMISERS[optimiser_name]

    # learn the network
    start_time = datetime.now()
    learner_out = learner(training_evaluator, parameters, optimiser)
    final_network = learner_out[0]
    steps_best = learner_out[1]
    steps_total = learner_out[2]
    feature_sets = learner_out[3] if len(learner_out) > 3 else None

    end_time = datetime.now()

    training_evaluator.remove_all_networks()
    training_evaluator.add_network(final_network)
    test_evaluator.add_network(final_network)

    # lambdas to make following more readable
    training_value = lambda metric: training_evaluator.metric_value(0, metric)
    test_value = lambda metric: test_evaluator.metric_value(0, metric)

    results = {
        'Ni':                       Ni,
        'No':                       No,
        'Ng':                       Ng,
        'learner':                  learner_name,
        'training_set_number':      parameters['training_set_number'],
        'transfer_functions':       parameters['node_funcs'],
        # 'Final Network':            network_trg,
        'iteration_for_best':       steps_best,
        'total_iterations':         steps_total,
        'training_error_guiding':   training_value(metric),
        'training_error_simple':    training_value(Metric.E1),
        'training_accuracy':        training_value(Metric.ACCURACY),
        'test_error_guiding':       test_value(metric),
        'test_error_simple':        test_value(Metric.E1),
        'test_accuracy':            test_value(Metric.ACCURACY),
        'final_network':            final_network.gates,
        'Ne':                       training_set.shape[0],
        'time':                     (end_time - start_time).total_seconds(),
        'evaluator':                evaluator_class.__name__
        }

    if feature_sets:
        for bit, v in enumerate(feature_sets):
            key = 'feature_set_target_{}'.format(bit)
            results[key] = v
    for bit, v in enumerate(training_value(Metric.PER_OUTPUT)):
        key = 'training_error_target_{}'.format(bit)
        results[key] = v
    for bit, v in enumerate(test_value(Metric.PER_OUTPUT)):
        key = 'test_error_target_{}'.format(bit)
        results[key] = v
    for bit, v in enumerate(final_network.max_node_depths()):
        key = 'max_depth_target_{}'.format(bit)
        results[key] = v
    for k, v in parameters['optimiser'].items():
        results['optimiser_' + k] = v

    return results

    # if parameters['optimiser_name'] == 'anneal':
    #     parameters['finalNg'] = parameters['Ng']
    # elif parameters['optimiser_name'] == 'ganneal':

    # elif parameters['optimiser_name'] == 'tabu':
    #     raise ValueError('Tabu search not implemented yet.')
