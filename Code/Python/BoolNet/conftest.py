# content of conftest.py
import pytest
import glob
import yaml
import numpy as np
from BoolNet.BooleanNetwork import BooleanNetwork
from BoolNet.RandomBooleanNetwork import RandomBooleanNetwork


TEST_LOCATION = 'BoolNet/test/'


# ################ Command line options #################### #
def pytest_addoption(parser):
    parser.addoption("--evaluator", action="store", default="cy",
                     help="evaluator: [py | cy | gpu]")


def pytest_runtest_setup(item):
    if ('gpu' in item.keywords and
       item.config.getoption("--evaluator") != "gpu"):
        pytest.skip("need \'gpu\' option to run")
    if ('cython' in item.keywords and
       item.config.getoption("--evaluator") != "cy"):
        pytest.skip("need \'cy\' option to run")
    if ('python' in item.keywords and
       item.config.getoption("--evaluator") != "py"):
        pytest.skip("need \'py\' option to run")


# metric fixture generator
def pytest_generate_tests(metafunc):
    if 'metric' in metafunc.fixturenames:
        if metafunc.config.option.evaluator == 'gpu':
            from BoolNet.BitErrorGPU import IMPLEMENTED_METRICS
            metrics = IMPLEMENTED_METRICS
        else:
            from BoolNet.BitError import all_metrics
            metrics = list(all_metrics())
        metafunc.parametrize("metric", metrics)


# ############ Helpers for fixtures ############# #
def harnesses_with_property(bool_property_name):
    for name in glob.glob(TEST_LOCATION + 'networks/*.yaml'):
        with open(name) as f:
            test = yaml.safe_load(f)
            if test[bool_property_name]:
                yield name


def harness_to_fixture(stream, evaluator_class):
    test = yaml.safe_load(stream)
    Ni = test['Ni']
    No = test['No']
    gates = test['gates']
    samples = test['samples']

    full_target = test['target matrix']['full']
    full_state = test['activation matrix']['full']
    full_error = test['error matrix']['full']
    full_inputs = [example[:Ni] for example in full_state]
    # generate sample versions
    sample_target = [full_target[s] for s in samples]
    sample_inputs = [full_inputs[s] for s in samples]
    # add sample version of expectations to test
    test['target matrix']['sample'] = sample_target
    test['activation matrix']['sample'] = [full_state[s] for s in samples]
    test['error matrix']['sample'] = [full_error[s] for s in samples]
    # add network to test
    if 'transfer functions' in test:
        test['network'] = RandomBooleanNetwork(np.array(
            gates, dtype=np.uint32), Ni, No, test['transfer functions'])
    else:
        test['network'] = BooleanNetwork(np.array(gates, dtype=np.uint32), Ni, No)

    # add evaluators to test
    test['evaluator'] = {
        'sample': evaluator_class(sample_inputs, sample_target),
        'full': evaluator_class(full_inputs, full_target)}
    test['evaluator']['sample'].add_network(test['network'])
    test['evaluator']['full'].add_network(test['network'])

    return test


# #################### Fixtures ############################ #
@pytest.fixture
def evaluator_class(request):
    if request.config.getoption("--evaluator") == 'gpu':
        import BoolNet.NetworkEvaluatorGPU
        return BoolNet.NetworkEvaluatorGPU.NetworkEvaluatorGPU
    elif request.config.getoption("--evaluator") == 'cy':
        import pyximport
        pyximport.install()
        import BoolNet.NetworkEvaluatorCython
        return BoolNet.NetworkEvaluatorCython.NetworkEvaluatorCython
    else:
        import BoolNet.NetworkEvaluator
        return BoolNet.NetworkEvaluator.NetworkEvaluator


# @pytest.fixture(scope='module', autouse=True)
# def compile_BooleanNetwork():
#     # compilation fixture
#     subprocess.check_call(['python3', 'setup.py', 'build_ext', '--inplace'])


@pytest.yield_fixture(params=glob.glob(TEST_LOCATION + '/error matrices/*.yaml'))
def error_matrix_harness(request):
    with open(request.param) as f:
        test = yaml.safe_load(f)
        test['error matrix'] = np.array(test['error matrix'], dtype=np.uint8)
        yield test


@pytest.fixture(params=['sample', 'full'])
def network_type(request):
    return request.param


@pytest.yield_fixture(params=glob.glob(TEST_LOCATION + 'networks/*.yaml'))
def any_test_network(request, evaluator_class):
    with open(request.param) as f:
        yield harness_to_fixture(f, evaluator_class)


@pytest.yield_fixture(params=list(harnesses_with_property(
    'invariant under single move')))
def single_move_invariant(request, evaluator_class):
    with open(request.param) as f:
        yield harness_to_fixture(f, evaluator_class)


@pytest.yield_fixture(params=list(harnesses_with_property(
    'invariant under multiple moves')))
def multiple_move_invariant(request, evaluator_class):
    with open(request.param) as f:
        yield harness_to_fixture(f, evaluator_class)


@pytest.yield_fixture
def single_layer_zero(evaluator_class):
    with open(TEST_LOCATION + 'networks/single_layer_zero.yaml') as f:
        yield harness_to_fixture(f, evaluator_class)


@pytest.yield_fixture
def adder2(evaluator_class):
    with open(TEST_LOCATION + 'networks/adder2.yaml') as f:
        yield harness_to_fixture(f, evaluator_class)
