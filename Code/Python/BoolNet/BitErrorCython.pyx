# cython: profile=True
from BoolNet.BitError import (E1, E2_LSB, E2_MSB, E3_LSB, E3_MSB, E4_LSB, E4_MSB, E5_LSB, E5_MSB,
                              E6_LSB, E6_MSB, E7_LSB, E7_MSB, ACCURACY, PER_OUTPUT)
import numpy as np
cimport numpy as np
import cython


# Array datatypes
COUNT_TYPE = np.uint32
BIT_TYPE = np.uint8
# Corresponding compile-time type to DTYPE_t. For
ctypedef np.uint32_t COUNT_TYPE_t
ctypedef np.uint8_t BIT_TYPE_t


@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
cdef np.ndarray[np.float_t, ndim=1] flood_count_msb(
    np.ndarray[BIT_TYPE_t, ndim=2] matrix, unsigned int rows, unsigned int cols):
    
    cdef unsigned int r
    cdef int c
    cdef np.ndarray[np.float_t, ndim=1] count = np.zeros(rows, dtype=np.float)
    for r in range(rows):
        count[r] = 0
        for c in reversed(range(cols)):
            if matrix[r, <unsigned int>c]:
                count[r] = c + 1
                break
    return count


@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
cdef np.ndarray[np.float_t, ndim=1] flood_count_lsb(
    np.ndarray[BIT_TYPE_t, ndim=2] matrix, unsigned int rows, unsigned int cols):
    cdef unsigned int r
    cdef unsigned int c
    cdef np.ndarray[np.float_t, ndim=1] count = np.zeros(rows, dtype=np.float)
    for r in range(rows):
        count[r] = 0
        for c in range(cols):
            if matrix[r, c]:
                count[r] = cols - c
                break
    return count


@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
cdef double dot_mean(np.ndarray[BIT_TYPE_t, ndim=2] matrix, 
                     np.ndarray[COUNT_TYPE_t, ndim=1] weight_vector, 
                     unsigned int rows, unsigned int cols):
    cdef unsigned int r, c
    cdef double total = 0.0
    for r in range(rows):
        for c in range(cols):
            total += (matrix[r, c] * weight_vector[c])
    return total / rows / cols


@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
cdef double matrix_mean(np.ndarray[BIT_TYPE_t, ndim=2] matrix, 
                        unsigned int rows, unsigned int cols):
    cdef unsigned int r, c
    cdef double total = 0.0
    for r in range(rows):
        for c in range(cols):
            total += matrix[r, c]
    return total / rows / cols


@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
cdef double vector_mean(np.ndarray[np.float_t, ndim=1] vector, 
                        unsigned int N):
    cdef unsigned int i
    cdef double total = 0.0
    for i in range(N):
        total += vector[i]
    return total / N

# Many of the calculations in this method rely on error_matrix
# only being comprised of 1s and 0s
@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def metric_value(np.ndarray[BIT_TYPE_t, ndim=2] error_matrix, metric):
    cdef double error = 0.0
    cdef unsigned int e, o
    cdef double max_err
    # for weighted and hierarchical methods
    cdef unsigned int Ne = error_matrix.shape[0]
    cdef unsigned int No = error_matrix.shape[1]
    cdef np.ndarray[COUNT_TYPE_t, ndim=1] msb_vector = np.arange(1, No+1, dtype=COUNT_TYPE)
    cdef np.ndarray[COUNT_TYPE_t, ndim=1] lsb_vector = np.arange(No, 0, -1, dtype=COUNT_TYPE)

    cdef double weight_correction_factor = 2.0 / (No + 1)
    cdef np.ndarray[double, ndim=1] totals

    # ################# MULTI-VALUED METRICS ################# #

    if metric == PER_OUTPUT:
        totals = np.zeros(No, dtype=np.float)
        for e in range(Ne):
            for o in range(No):
                totals[o] += error_matrix[e, o]
        for o in range(No):
                totals[o] /= Ne
        return totals

    # ################ SINGLE-VALUED  METRICS ################ #

    elif metric == E1:
        return matrix_mean(error_matrix, Ne, No)

    elif metric == E2_MSB:
        return dot_mean(error_matrix, msb_vector, Ne, No) * 2.0 / (No + 1)
    elif metric == E2_LSB:
        return dot_mean(error_matrix, lsb_vector, Ne, No) * 2.0 / (No + 1)

    elif metric == ACCURACY:
        return 1.0 - np.mean(np.any(error_matrix, axis=1))

    elif metric == E3_MSB:
        return vector_mean(flood_count_msb(error_matrix, Ne, No), Ne) / No

    elif metric == E3_LSB:
        return vector_mean(flood_count_lsb(error_matrix, Ne, No), Ne) / No

    elif metric == E4_MSB or metric == E4_LSB:
        if metric == E4_MSB:
            totals = flood_count_msb(error_matrix, Ne, No)
        else:
            totals = flood_count_lsb(error_matrix, Ne, No)
        # make errors monotonic increasing
        error = totals[0]
        max_err = totals[0]
        for e in range(1, Ne):
            max_err = max(max_err, totals[e])
            error += max_err
        return error / Ne / No

    elif metric == E5_MSB or metric == E5_LSB:
        # Find the msb/lsb in error for each sample
        if metric == E5_MSB:
            totals = flood_count_msb(error_matrix, Ne, No)
        else:
            totals = flood_count_lsb(error_matrix, Ne, No)
        # Find the first sample with the largest error
        worst_example = np.argmax(totals)
        largest_error = totals[worst_example]
        error = ((largest_error - 1) * worst_example +
                 largest_error * (Ne - worst_example))
        return error / Ne / No

    elif metric == E6_MSB or metric == E6_LSB:
        # Find the msb in error for each sample
        if metric == E6_MSB:
            totals = flood_count_msb(error_matrix, Ne, No)
        else:
            totals = flood_count_lsb(error_matrix, Ne, No)

        largest_error = np.amax(totals)
        num_wrong_on_worst_bit = np.count_nonzero(totals == largest_error)
        return (largest_error - 1 + num_wrong_on_worst_bit / Ne) / No

    elif metric == E7_MSB:
        return flood_count_msb(error_matrix, Ne, No).max() / <float>No

    elif metric == E7_LSB:
        return flood_count_lsb(error_matrix, Ne, No).max() / <float>No

    else:
        raise ValueError('Invalid metric - {}'.format(metric))
