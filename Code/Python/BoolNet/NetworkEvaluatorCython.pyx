# cython: profile=True
from BoolNet.NetworkEvaluator import NetworkEvaluator
import BoolNet.BitErrorCython as BitErrorCython
import numpy as np
# "cimport" is used to import special compile-time information
# about the numpy module (this is stored in a file numpy.pxd which is
# currently part of the Cython distribution)
cimport numpy as np
import cython

# cdef struct State:
#     BooleanNetwork network
#     np.uint8[:, :] activation
#     np.uint8[:, :] output
#     np.uint8[:, :] error

class NetworkEvaluatorCython(NetworkEvaluator):

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.nonecheck(False)
    @cython.cdivision(True)
    def _evaluate_random(self, state):
        ''' Evaluate the activation and error matrices for the selected network
            getting node TFs from network. '''
        cdef unsigned int Ni, No, Ng, Ne
        Ng = state.network.Ng
        Ne, Ni = self._inputs.shape
        No = self._target.shape[1]
        cdef np.uint8_t[:,:] activation = state.activation
        cdef np.uint8_t[:,:] output = state.output
        cdef np.uint8_t[:,:] error = state.error
        cdef np.uint8_t[:,:] target = self._target
        cdef np.ndarray[np.uint32_t, ndim=2] gates = state.network.gates
        cdef np.ndarray[np.uint8_t, ndim=3] tf_matrix = state.network.transfer_functions

        # evaluate the state matrix
        cdef unsigned int e
        cdef unsigned int g
        cdef unsigned int o
        cdef unsigned int start = state.network.first_unevaluated_gate
        for e in range(Ne):
            for g in range(start, Ng):
                activation[e, Ni + g] = tf_matrix[
                    g, activation[e, gates[g, 0]], activation[e, gates[g, 1]]]
            # evaluate the error matrix
            for o in range(No):
                error[e, o] = (target[e, o] != output[e, o])

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.nonecheck(False)
    @cython.cdivision(True)
    def _evaluate_NAND(self, state):
        ''' Evaluate the activation and error matrices for the selected network
            assuming each node TF is NAND. '''
        cdef unsigned int Ni, No, Ng, Ne
        Ng = state.network.Ng
        Ne, Ni = self._inputs.shape
        No = self._target.shape[1]
        cdef np.uint8_t[:,:] activation = state.activation
        cdef np.uint8_t[:,:] output = state.output
        cdef np.uint8_t[:,:] error = state.error
        cdef np.uint8_t[:,:] target = self._target
        cdef np.ndarray[np.uint32_t, ndim=2] gates = state.network.gates

        # evaluate the state matrix
        cdef unsigned int e
        cdef unsigned int g
        cdef unsigned int o
        cdef unsigned int start = state.network.first_unevaluated_gate
        for e in range(Ne):
            for g in range(start, Ng):
                activation[e, Ni + g] = not (
                    activation[e, gates[g, 0]] and activation[e, gates[g, 1]])
            # evaluate the error matrix
            for o in range(No):
                error[e, o] = (target[e, o] != output[e, o])

        state.network._evaluated = True

    def metric_value(self, unsigned int index, metric):
        self.evaluate(index)
        return BitErrorCython.metric_value(self._states[index].error, metric)
