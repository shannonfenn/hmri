from copy import copy
from collections import namedtuple
import numpy as np
import pytest

import pyximport
pyximport.install()


Move = namedtuple('Move', ['gate', 'terminal', 'source'])


# ############# General helpers ################# #
def to_binary(value, num_bits):
    return [int(i) for i in '{:0{w}b}'.format(value, w=num_bits)][::-1]


def all_possible_inputs(num_bits):
    return np.array([to_binary(i, num_bits) for i in range(2**num_bits)],
                    dtype=np.byte)


class TestExceptions:
    @pytest.fixture
    def gates(self):
        return np.array([(0, 1)], dtype=np.uint32)

    @pytest.fixture
    def inputs(self):
        return all_possible_inputs(4)

    @pytest.fixture
    def target(self):
        return np.zeros((2**4, 1), dtype=np.byte)

    ''' Exception Testing '''
    def test_construction_exceptions(self, gates, inputs, target,
                                     evaluator_class):
        ''' This test may not check all cases but is not
            particularly important as it is just error checks.'''
        # num input examples != num target examples
        with pytest.raises(ValueError):
            evaluator_class(all_possible_inputs(1), target)
        with pytest.raises(ValueError):
            evaluator_class(inputs, np.zeros((2, 1), dtype=np.byte))


class TestFunctionality:

    def test_input_matrix(self, any_test_network, network_type):
        evaluator = any_test_network['evaluator'][network_type]
        actual = evaluator.input_matrix
        Ni = any_test_network['Ni']
        activation = np.array(any_test_network['activation matrix'][network_type], dtype=np.uint8)
        expected = activation[:, :Ni]
        assert np.array_equal(actual, expected)

    def test_target_matrix(self, any_test_network, network_type):
        evaluator = any_test_network['evaluator'][network_type]
        actual = evaluator.target_matrix
        expected = np.array(any_test_network['target matrix'][network_type], dtype=np.uint8)
        assert np.array_equal(actual, expected)

    def test_output_matrix(self, any_test_network, network_type):
        evaluator = any_test_network['evaluator'][network_type]
        actual = evaluator.output_matrix(0)
        No = any_test_network['No']
        activation = np.array(any_test_network['activation matrix'][network_type], dtype=np.uint8)
        expected = activation[:, -No:]
        assert np.array_equal(actual, expected)

    def test_truth_table(self, any_test_network, network_type):
        evaluator = any_test_network['evaluator'][network_type]
        actual = evaluator.truth_table(0)
        activation = np.array(any_test_network['activation matrix']['full'], dtype=np.uint8)
        expected = activation[:, -any_test_network['No']:]
        assert np.array_equal(actual, expected)

    def test_activation_matrix(self, any_test_network, network_type):
        evaluator = any_test_network['evaluator'][network_type]
        actual = evaluator.activation_matrix(0)
        expected = np.array(
            any_test_network['activation matrix'][network_type],
            dtype=np.byte)
        assert np.array_equal(actual, expected)

    def test_single_move_output_different(
            self, single_move_invariant, network_type):
        evaluator = single_move_invariant['evaluator'][network_type]
        net = evaluator.network(0)
        for k in range(250):
            old_error = copy(evaluator.error_matrix(0))
            net.move_to_neighbour(net.random_move())
            assert not np.array_equal(evaluator.error_matrix(0),
                                      old_error)
            net.revert_move()

    def test_multiple_move_output_different(
            self, multiple_move_invariant, network_type):
        evaluator = multiple_move_invariant['evaluator'][network_type]
        net = evaluator.network(0)
        for k in range(250):
            old_error = copy(evaluator.error_matrix(0))
            net.move_to_neighbour(net.random_move())
            assert not np.array_equal(evaluator.error_matrix(0),
                                      old_error)

    def test_move_with_initial_evaluation(
            self, single_layer_zero, network_type):
        evaluator = single_layer_zero['evaluator'][network_type]
        net = evaluator.network(0)

        assert np.array_equal(evaluator.error_matrix(0),
                              single_layer_zero['error matrix'][network_type])

        move = Move(gate=1, source=4, terminal=True)

        net.move_to_neighbour(move)
        actual = evaluator.error_matrix(0)
        expected = np.array([
            [1, 1], [1, 1], [1, 1], [0, 1],
            [1, 0], [1, 0], [1, 0], [0, 1],
            [1, 1], [1, 1], [1, 1], [0, 1],
            [1, 0], [1, 0], [1, 0], [0, 1]], dtype=np.byte)
        assert np.array_equal(actual, expected)

    def test_multiple_moves_error_matrix(
            self, single_layer_zero, network_type):
        evaluator = single_layer_zero['evaluator'][network_type]
        net = evaluator.network(0)

        moves = [Move(gate=0, source=1, terminal=False),
                 Move(gate=1, source=0, terminal=True),
                 Move(gate=0, source=0, terminal=True),
                 Move(gate=1, source=3, terminal=True),
                 Move(gate=1, source=4, terminal=True),
                 Move(gate=1, source=4, terminal=False)]

        expected = np.array([
            # expected after move 1
            [[1, 1], [1, 1], [0, 1], [0, 1], [1, 1], [1, 1], [0, 1], [0, 1],
             [1, 1], [1, 1], [0, 1], [0, 1], [1, 0], [1, 0], [0, 0], [0, 0]],
            # expected after move 2
            [[1, 1], [1, 1], [0, 1], [0, 1], [1, 1], [1, 0], [0, 1], [0, 0],
             [1, 1], [1, 1], [0, 1], [0, 1], [1, 1], [1, 0], [0, 1], [0, 0]],
            # expected after move 3
            [[1, 1], [1, 1], [1, 1], [0, 1], [1, 1], [1, 0], [1, 1], [0, 0],
             [1, 1], [1, 1], [1, 1], [0, 1], [1, 1], [1, 0], [1, 1], [0, 0]],
            # expected after move 4
            [[1, 1], [1, 1], [1, 1], [0, 1], [1, 1], [1, 1], [1, 1], [0, 1],
             [1, 1], [1, 1], [1, 1], [0, 1], [1, 0], [1, 0], [1, 0], [0, 0]],
            # expected after move 5
            [[1, 1],  [1, 1], [1, 1], [0, 1], [1, 0], [1, 0], [1, 0], [0, 1],
             [1, 1], [1, 1], [1, 1], [0, 1], [1, 0], [1, 0], [1, 0], [0, 1]],
            # expected after move 6
            [[1, 0], [1, 0], [1, 0], [0, 1], [1, 0], [1, 0], [1, 0], [0, 1],
             [1, 0], [1, 0], [1, 0], [0, 1], [1, 0], [1, 0], [1, 0], [0, 1]]],
            dtype=np.byte)

        for move, expectation in zip(moves, expected):
            net.move_to_neighbour(move)
            actual = evaluator.error_matrix(0)
            assert np.array_equal(actual, expectation)

    def test_multiple_reverts_error_matrix(
            self, single_layer_zero, network_type):
        evaluator = single_layer_zero['evaluator'][network_type]
        net = evaluator.network(0)

        moves = [Move(gate=0, source=1, terminal=False),
                 Move(gate=1, source=0, terminal=True),
                 Move(gate=0, source=0, terminal=True),
                 Move(gate=1, source=3, terminal=True),
                 Move(gate=1, source=4, terminal=True),
                 Move(gate=1, source=4, terminal=False)]

        expected = np.array([
            # expected after move 1
            [[1, 1], [1, 1], [0, 1], [0, 1], [1, 1], [1, 1], [0, 1], [0, 1],
             [1, 1], [1, 1], [0, 1], [0, 1], [1, 0], [1, 0], [0, 0], [0, 0]],
            # expected after move 2
            [[1, 1], [1, 1], [0, 1], [0, 1], [1, 1], [1, 0], [0, 1], [0, 0],
             [1, 1], [1, 1], [0, 1], [0, 1], [1, 1], [1, 0], [0, 1], [0, 0]],
            # expected after move 3
            [[1, 1], [1, 1], [1, 1], [0, 1], [1, 1], [1, 0], [1, 1], [0, 0],
             [1, 1], [1, 1], [1, 1], [0, 1], [1, 1], [1, 0], [1, 1], [0, 0]],
            # expected after move 4
            [[1, 1], [1, 1], [1, 1], [0, 1], [1, 1], [1, 1], [1, 1], [0, 1],
             [1, 1], [1, 1], [1, 1], [0, 1], [1, 0], [1, 0], [1, 0], [0, 0]],
            # expected after move 5
            [[1, 1], [1, 1], [1, 1], [0, 1], [1, 0], [1, 0], [1, 0], [0, 1],
             [1, 1], [1, 1], [1, 1], [0, 1], [1, 0], [1, 0], [1, 0], [0, 1]],
            # expected after move 6
            [[1, 0], [1, 0], [1, 0], [0, 1], [1, 0], [1, 0], [1, 0], [0, 1],
             [1, 0], [1, 0], [1, 0], [0, 1], [1, 0], [1, 0], [1, 0], [0, 1]]],
            dtype=np.byte)

        for move in moves:
            net.move_to_neighbour(move)

        for expectation in reversed(expected):
            actual = evaluator.error_matrix(0)
            assert np.array_equal(actual, expectation)
            net.revert_move()

    def test_error_matrix(self, any_test_network, network_type):
        evaluator = any_test_network['evaluator'][network_type]
        actual = evaluator.error_matrix(0)
        expected = any_test_network['error matrix'][network_type]
        assert np.array_equal(actual, expected)

    def test_metric_value(self, any_test_network, metric, network_type):
        # a value may not have been specified for all metrics
        # on this test network
        evaluator = any_test_network['evaluator'][network_type]
        actual = evaluator.metric_value(0, metric)

        # print(evaluator._device_states[0])
        # print(any_test_network['metric value'][network_type][str(metric)])

        metric_values = any_test_network['metric value'][network_type]
        expected = metric_values[str(metric)]
        assert np.array_equal(actual, expected)

    def test_pre_evaluated_network(self, any_test_network, network_type):
        if network_type == 'sample':
            evaluator1 = any_test_network['evaluator']['sample']
            evaluator2 = any_test_network['evaluator']['full']
            evaluator1.remove_all_networks()
            evaluator2.remove_all_networks()
            net = any_test_network['network']
            evaluator1.add_network(net)
            for i in range(10):
                net.move_to_neighbour(net.random_move())
                evaluator1.evaluate(0)
            net.revert_all_moves()
            assert np.array_equal(evaluator1.activation_matrix(0),
                                  any_test_network['activation matrix']['sample'])
            evaluator2.add_network(net)
            assert np.array_equal(evaluator2.activation_matrix(0),
                                  any_test_network['activation matrix']['full'])
