import numpy as np
from enum import Enum, unique


@unique
class Metric(Enum):
    E1 = 1,         # simple
    E2_MSB = 2,     # weighted
    E2_LSB = 3,
    E3_MSB = 4,     # hierarchical
    E3_LSB = 5,
    E4_MSB = 8,
    E4_LSB = 9,
    E5_MSB = 10,
    E5_LSB = 11,
    E6_MSB = 12,
    E6_LSB = 13,
    E7_MSB = 6,     # worst example
    E7_LSB = 7,
    ACCURACY = 14,
    PER_OUTPUT = 15,

    def __str__(self):
        return self.name.lower().replace('_', ' ')

E1 = Metric.E1
E2_MSB = Metric.E2_MSB
E2_LSB = Metric.E2_LSB
E3_MSB = Metric.E3_MSB
E3_LSB = Metric.E3_LSB
E4_MSB = Metric.E4_MSB
E4_LSB = Metric.E4_LSB
E5_MSB = Metric.E5_MSB
E5_LSB = Metric.E5_LSB
E6_MSB = Metric.E6_MSB
E6_LSB = Metric.E6_LSB
E7_MSB = Metric.E7_MSB
E7_LSB = Metric.E7_LSB
ACCURACY = Metric.ACCURACY
PER_OUTPUT = Metric.PER_OUTPUT


def all_metrics():
    for m in Metric:
        yield m


def metric_from_name(name):
    return Metric[name.upper().replace(' ', '_')]


def metric_name(metric):
    return str(metric)


def flood_count_msb(error_matrix, No):
    return ((No - np.argmax(np.fliplr(error_matrix), axis=1)) *
            (error_matrix.sum(axis=1) > 0))


def flood_count_lsb(error_matrix, No):
    return (No - np.argmax(error_matrix, axis=1)) * (error_matrix.sum(axis=1) > 0)


# Many of the calculations in this method rely on error_matrix
# only being comprised of 1s and 0s
def metric_value(error_matrix, metric):
    error = 0.0
    # for weighted and hierarchical methods
    Ne, No = error_matrix.shape
    msb_vector = np.arange(No) + 1
    lsb_vector = np.flipud(msb_vector)
    weight_denominator = No*(No+1)/2

    # ################# MULTI-VALUED METRICS ################# #

    if metric == PER_OUTPUT:
        return np.mean(error_matrix, axis=0)

    # ############### SINGLE-VALUED METRICS ############### #

    if metric == E1:
        return error_matrix.mean()

    if metric == E2_MSB:
        return np.dot(error_matrix, msb_vector).mean() / weight_denominator

    if metric == E2_LSB:
        return np.dot(error_matrix, lsb_vector).mean() / weight_denominator

    if metric == ACCURACY:
        return 1.0 - np.mean(np.any(error_matrix, axis=1))

    if metric == E3_MSB:
        return flood_count_msb(error_matrix, No).mean() / No

    if metric == E3_LSB:
        return flood_count_lsb(error_matrix, No).mean() / No

    if metric == E4_MSB or metric == E4_LSB:
        # Find the msb/lsb in error for each sample
        if metric == E4_MSB:
            errs = flood_count_msb(error_matrix, No)
        else:
            errs = flood_count_lsb(error_matrix, No)
        # make errors monotonic increasing
        for i in range(1, len(errs)):
            errs[i] = max(errs[i], errs[i-1])
        return sum(errs) / len(errs) / No

    if metric == E5_MSB or metric == E5_LSB:
        # Find the msb/lsb in error for each sample
        if metric == E5_MSB:
            worst = flood_count_msb(error_matrix, No)
        else:
            worst = flood_count_lsb(error_matrix, No)
        # Find the first sample with the largest error
        worst_example = np.argmax(worst)
        largest_error = worst[worst_example]
        error = ((largest_error - 1) * worst_example +
                 largest_error * (Ne - worst_example))
        return error / Ne / No

    if metric == E6_MSB or metric == E6_LSB:
        # Find the msb/lsb in error for each sample
        if metric == E6_MSB:
            worst = flood_count_msb(error_matrix, No)
        else:
            worst = flood_count_lsb(error_matrix, No)
        largest_error = np.amax(worst)
        num_wrong_on_worst_bit = np.count_nonzero(worst == largest_error)
        return (largest_error - 1 + num_wrong_on_worst_bit / Ne) / No

    if metric == E7_MSB:
        return flood_count_msb(error_matrix, No).max() / No

    if metric == E7_LSB:
        return flood_count_lsb(error_matrix, No).max() / No

    raise ValueError('Invalid metric - {}'.format(metric))
