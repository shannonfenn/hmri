from itertools import combinations
import numpy as np
from subprocess import check_call
import os
from math import log10, ceil
import re


def is_feature_set(fs, target):
    # generator for the example index pairs to check
    to_check = ((i1, i2) for i1, i2 in combinations(range(len(target)), 2)
                if target[i1] != target[i2])

    # If all features are non-discriminating for any
    # example pair then we do not have a feature set
    return not any(np.array_equal(fs[e1], fs[e2]) for e1, e2 in to_check)

# def minimal_feature_sets(features, target):
#     ''' Takes a featureset matrix and target vector and finds the
#         set of all minimal featuresets.
#     featureset  - assumed to be a 2D numpy array in example x feature format.
#     target      - assumed to be a 1D numpy array of the same number of rows
#                   as featureset
#     returns     - a list of lists of feature indices representing all feature
#                   sets of minimal cardinality. '''

#     # test if given set of features is a feature set for
#     # the target
#     if not is_feature_set(features, target):
#         print('Original set not a feature set for target {}!'.format(targetname))
#         return []
#     else:
#         # Get the dimensions of the example matrix
#         num_examples, num_features = features.shape
#         # make sure the target is a Ne-length vector
#         assert target.shape == (num_examples, )
#         # for all possible feature set sizes
#         for k in range(1, num_features+1):
#             print('Trying all ', k, '-feature-sets')
#             # make a list of all feature sets for this value of k
#             feature_sets = [fs for fs in combinations(range(num_features), k)
#                                if is_feature_set(features[:, fs], target)]
#             # stop when we have found at least one
#             if feature_sets:
#                 break

#     return feature_sets


def abk_file(features, target, file_name):
    n_examples, n_features = features.shape

    with open(file_name, 'x') as f:
        f.write(('FEATURESINROWS\n'
                 'TARGETPRESENT\n'
                 'LAST\n'
                 '{}\n'
                 '{}\n'
                 'dummy\t'
                 '{}\n').format(n_features, n_examples,
                                '\t'.join('s' + str(i) for i in range(n_examples))))

        # how many digits needed to represent the given number of features
        num_digits = int(ceil(log10(n_features)))

        # write non target features as rows
        for c in range(n_features):
            f.write('f{:0{width}d}\t'.format(c, width=num_digits))
            f.write('\t'.join(str(x) for x in features[:, c]))
            f.write('\n')

        # write target row
        f.write('\t' + '\t'.join(str(x) for x in target) + '\n')


def FABCPP_cmd_line(features, target, file_name_base):
    abk_file_name = file_name_base + '.abk'
    log_file_name = file_name_base + '.log'
    err_file_name = file_name_base + '.err'
    out_file_name = file_name_base + '.out'

    # check if files exist and raise exception

    # write abk file
    abk_file(features, target, abk_file_name)
    # run FABCPP
    with open(log_file_name, 'x') as log, open(err_file_name, 'x') as err:
        cmd_args = [os.path.expanduser('~/CIBMTools/FABCPP/Release/FABCPP'),
                    '-i', abk_file_name,
                    '-o', out_file_name,
                    # '-i' , 'Results_test_14-09-03_12-30_0/inter_0.abk',
                    # '-o' , 'Results_test_14-09-03_12-30_0/inter_0.out',
                    '-m', '1',
                    '-A', '1',
                    '-B', '0',
                    '-y', 'alfa',
                    '-O', 'max:feature_degree']
        check_call(cmd_args, stdout=log, stderr=err)

        # parse output
        with open(out_file_name) as out:
            n_features = features.shape[1]
            digits = int(ceil(log10(n_features)))
            # make regex for matching feature lines
            reg = re.compile('f[0-9]{{{}}}'.format(digits))

            # pull out all lines from output file which match the feature name regex
            k_feature_set = [int(l.strip()[1:]) for l in out if reg.match(l.strip())]

            return np.array([k_feature_set])

        # TODO: Use model 5 to compute other


def minimal_feature_sets(features, target, file_name_base):
    ''' Takes a featureset matrix and target vector and finds the
        set of all minimal featuresets.
    featureset  - assumed to be a 2D numpy array in example x feature format.
    target      - assumed to be a 1D numpy array of the same number of rows
                  as featureset
    returns     - a list of lists of feature indices representing feature
                  sets of minimal cardinality.

    NOTE: Only finds a single minFS at present'''

    # check if the features form a feature set, if not then no subset can either
    if is_feature_set(features, target):
        return FABCPP_cmd_line(features, target, file_name_base)
    else:
        with open(file_name_base + '.err', 'x') as f:
            f.write('No feature set: using entire set.')
        return list(range(features.shape[1]))
