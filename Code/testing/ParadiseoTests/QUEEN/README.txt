This package contains all files you need to start Exercice1 of the ParadisEO website.

#CONTAINS

In "src" directory, some files are already defined.

You will have to fill the files in the "application" directory.

Correction for the part I are available in the "correction" directory

#INSTALLATION

In a command line interpreter, go into the "build" directory and enter:

> cmake .. (On Unix)
> make

