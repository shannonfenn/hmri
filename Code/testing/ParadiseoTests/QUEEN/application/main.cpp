#include <eo>
#include <queen.h>
#include <queenInit.h>
#include <queenMutation.h>
#include <queenCrossover.h>
#include <queenEval.h>

int main(int argc, char* argv[]){

  //Define a QUEEN -> 1 line


  //Define an initializer -> 1 line


  //Define the evaluation function -> 1 line


  //Define mutation -> 1 line


  //Define crossover -> 1 line


  //Define a generational continuator (put 100 generation for example) -> 1 line


  //Define the transformation object (it contains, the crossover, the crossover rate, the mutation and the mutation rate) -> 1 line


  //Define a selection method that selects ONE individual by deterministic tournament(put the tournament size at 2 for example) -> 1 line 


  //Define a "eoSelectPerc" with the tournament with default parameter (allow to select the good size of individuals) -> 1 line


  //Define a generational replacement strategy -> 1 line


  //Define a pop of QUEEN -> 1 line


  //Fill the pop with 100 initialized and evaluated QUEEN
  //Use the initializer, the evaluation function and the push_back operator's vector -> A "for" included three insrtuctions

  
  //Print the pop -> 1 line


  //HERE you can test whether you succeded in initializing the population by compiling and executing this part of the program.
  
  //Print end of line (endl)

  
  /*Define an eoEasyEA with good parameter:
    - continuator
    - evaluation function
    - eoSelectPerc
    - transformation object
    - replacement
   */
  // -> 1 line


  //run the algorithm on the initialized population -> 1 line


  //Print the best element -> 1 line


  //If the fitness value is equal to 0, the best solution is found. Else try again.

  std::cout << std::endl;  
  return EXIT_SUCCESS;
}
