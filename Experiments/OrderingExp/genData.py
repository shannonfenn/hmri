import sys
import random

def generateAdditionData(Ni):
	# Upper limit
	upper = 2**Ni
	# generate tuples of (input1, input2, output)
	dataset = [(i1, i2, (i1+i2)%upper ) for i1 in range(upper) for i2 in range(upper)]

	return dataset


def main(argv):
	# process arguments
	if not ( 5 <= len(argv) <= 6):
		sys.exit("Usage " + argv[0] + " prefix #inputbits #sets #samplesPerSet")
		
	# parameters
	prefix = argv[1]
	Ni = int(argv[2])
	numSets = int(argv[3])
	samplesPerSet = int(argv[4])
	numExamples = 2**(2*Ni)
	
	if samplesPerSet > numExamples:
		sys.exit('Error: samplesPerSet ({}) larger than number of examples({})'.format(samplesPerSet, numExamples))
	
	# generate full dataset and write to file
	dataset = generateAdditionData(Ni)
	with open(prefix + 'data', 'w') as f:
		f.write( '{} {}\n'.format(2*Ni, Ni) )
		for tup in dataset:
			f.write(' '.join('{:0{width}b}{:0{width}b} {:0{width}b}'.format(tup[0], tup[1], tup[2], width=Ni)) + '\n')
	
	# generate index samples	
	samples = [random.sample(range(numExamples), samplesPerSet) for i in range(numSets)]

	for opt in ['r', 'oi', 'od']:
		# and write them to files
		for i in range(len(samples)):
			if opt == 'r':		# shuffle randomly
				random.shuffle(samples[i])
			elif opt == 'oi':	# sort by output increasing
				samples[i].sort(key=lambda x: dataset[x][2] )	
			elif opt == 'od':	# sort by output decreasing
				samples[i].sort(key=lambda x: dataset[x][2], reverse=True)
				
			with open(prefix + 'samples_' + str(i+1) + '_' + opt, 'w') as f:
				f.write(' '.join(str(s) for s in samples[i]))


if __name__ == '__main__':
	main(sys.argv)