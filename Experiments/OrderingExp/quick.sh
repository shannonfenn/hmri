#!/bin/bash

wait

STARTTIME=$(date +%s)

EXP_DIR=$(pwd)
BUILD_DIR="/home/shannon/HMRI/BooleanNet/C++/build-BooleanNet-Desktop-Release/"

cd $BUILD_DIR
make
make_ret=$?
cd $EXP_DIR


if [ $make_ret -ne 0 ]; then
	echo 'make failed, aborting.'
else
	cp "$BUILD_DIR"BooleanNet $EXP_DIR

	N=10	
	Ne=32
	Ns=15000
	
	mkdir -p QuickResults
	
	python genData.py "Data/" 4 $N $Ne 
	./subExp.sh 4 $Ne 60 1000 $Ns 0.05 0.997 $N
	mv QuickResults/results QuickResults/results_4_$Ne\_60_1000_$Ns\_0.05_0.997_$N


	CMDENDTIME=$(date +%s)
	echo "Finished : $[$CMDENDTIME - $STARTTIME] s"
fi
