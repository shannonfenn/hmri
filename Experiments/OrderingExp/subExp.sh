#!/bin/bash

# Parameters
Ni=$1
Ne=$2
Ng=$3
Nt=$4
Ns=$5
T=$6
Ts=$7
N=$8

STARTTIME=$(date +%s)

#echo "          metric             trgerror(guiding)    trgerror(simple)    error(guiding)     error(simple)      #iterBest     #iterFinish      time(s)" > QuickResults/results # Clear the results file
echo "          metric             trgerror(guiding)    trgerror(simple)    error(guiding)     error(simple)      accuracy        #iterBest     #iterFinish   errorsPerBit(trg) errorsPerBit(final)   time(s)" > QuickResults/results # Clear the results file

# generate N copies of commands	
echo -n "" > commands
for i in $(seq 1 $N);
do 
	echo "./BooleanNet givensamples Data/data Data/samples_$i\_r anneal $Ng $Nt $Ns $T $Ts simple" >> commands
	
	for ordering in r oi od;	
	do
		echo "echo -n $ordering\_; ./BooleanNet givensamples Data/data Data/samples\_$i\_$ordering anneal $Ng $Nt $Ns $T $Ts e4_msb" >> commands
		echo "echo -n $ordering\_; ./BooleanNet givensamples Data/data Data/samples\_$i\_$ordering anneal $Ng $Nt $Ns $T $Ts e4_lsb" >> commands
	done
done

# run these commands 8 at a time
parallel -j8 < commands >> QuickResults/results
# remove temp commands file
#rm commands

CMDENDTIME=$(date +%s)
echo "Sub Exp Finished : $[$CMDENDTIME - $STARTTIME] s"
