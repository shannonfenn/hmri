import subprocess
import random

def evaluate(permutation):
    #setup sample file
    with open('Data/sample', 'w') as f:
        f.write(' '.join(permutation))
    result = subprocess.check_output(["./BooleanNet", ])



def mutate(permutation):
    # select two random unique positions
    s = random.sample(range(len(permutation)), 2)
    # swap them
    permutation[s[0]], permutation[s[1]] = permutation[s[1]], permutation[s[0]]

def recombine(parent1, parent2):
    return UCX(parent1, parent2)

def UCX(parent1, parent2):
    pass
    
def main(argv):
    # parameters
    prefix = argv[1]
    Ni = 4
    Ne = 32
    upper = 2*Ni
    numExamples = 2**upper
    poolsize = 100
    
    # generate tuples of (input1, input2, output)
    dataset = [ (i1, i2, (i1+i2)%upper ) for i1 in range(upper) for i2 in range(upper) ]
    
    # generate pool of permutations of the sample
    sample = random.sample(range(numExamples), Ne)
    pool = [ copy(sample) for i in range(poolsize) ]
    for p in pool:
        random.shuffle(p)




if __name__ == '__main__':
	main(sys.argv)