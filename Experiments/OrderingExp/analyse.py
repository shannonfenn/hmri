from collections import defaultdict

import numpy
import sys
import json

if len(sys.argv) == 2:
	filename = sys.argv[1]
else:
	filename = 'Results/results'

f = open(filename)

categories = f.readline().split()[1:]			# column headers
results = [defaultdict(list) for c in categories]	# lookups for metrics
metrics = ['simple', 'r_e4_msb', 'oi_e4_msb', 'od_e4_msb', 'r_e4_lsb', 'oi_e4_lsb', 'od_e4_lsb']

maxwidth = 0

for l in f.readlines():
	line = l.split()
	metric = line[0]

	maxwidth = max(maxwidth, len(metric))

	line = line[1:]

	for i in range(len(line)):
		if line[i].startswith('['):
			vals = [ float(v) for v in line[i].strip('[]').split(',') ]
			if results[i][metric]:
				for a, b in zip(results[i][metric], vals):
					a.append(b)
			else:
				results[i][metric] = [ [v] for v in vals]
		else:
			results[i][metric].append(float(line[i]))

finerr 		= categories.index('error(simple)')
trgerr 		= categories.index('trgerror(simple)')
iterfin		= categories.index('#iterFinish')
iterbest 	= categories.index('#iterBest')
acc			= categories.index('accuracy')
perbittrg 	= categories.index('errorsPerBit(trg)')
perbitfin 	= categories.index('errorsPerBit(final)')
time 		= categories.index('time(s)')


for k, v in list(results[perbittrg].items()):
	print(k)
	print(numpy.mean(v, axis=1))
print('')
for k, v in list(results[perbitfin].items()):
	print(k)
	print(numpy.mean(v, axis=1))
print('')

plotting = True

try:
	from savefig import monkey_patch
	monkey_patch()
	import prettyplotlib as ppl
	import matplotlib.pyplot as plt
	from matplotlib import rcParams
	rcParams.update({'figure.autolayout': True})
	#import prettyplotlib as plt
except Exception:
	plotting = False

if plotting:
	fig, axs = plt.subplots(3, 2, sharex=True)	
	fig.autofmt_xdate()
	
	D = results[finerr]
	data = [D[m] for m in metrics]
	ppl.boxplot(axs[0,0], data, xticklabels=metrics)
	axs[0,0].set_ylim(bottom=0)
	axs[0,0].set_title('Final Error')
	
	D = results[trgerr]
	data = [D[m] for m in metrics]
	ppl.boxplot(axs[0,1], data)
	axs[0,1].set_ylim(bottom=0)
	axs[0,1].set_title('Training Error')

	D = results[acc]
	data = [D[m] for m in metrics]
	ppl.boxplot(axs[1,0], data)
	axs[1,0].set_ylim(bottom=0, top=1.0)
	axs[1,0].set_title('Accuracy')
	
	D = results[iterbest]
	data = [D[m] for m in metrics]
	ppl.boxplot(axs[1,1], data)
	axs[1,1].set_ylim(bottom=0)
	axs[1,1].set_title('Iteration for Best')

	D = results[finerr]
	data = [D[m].count(0.0) + 1e-15 for m in metrics]
	ppl.bar(axs[2,0], numpy.arange(len(data)) + 0.5, data, grid='y')
	axs[2,0].set_ylim(top=max(1, max(data)))
	axs[2,0].set_title('# Fully Generalised')

	#plt.subplot(326)
	D = results[trgerr]
	data = [D[m].count(0.0) + 1e-15 for m in metrics]
	ppl.bar(axs[2,1], numpy.arange(len(data)) + 0.5, data, grid='y')
	axs[2,1].set_ylim(top=max(1, max(data)))
	axs[2,1].set_title('# Fully Memorised')
	
	#plt.show()
	#plt.savefig(filename + '.eps', bbox_inches='tight')
	#plt.savefig(filename + '.png', bbox_inches='tight')
	plt.savefig(filename + '.png', bbox_inches='tight', orientation='portrait')
	
	# NEED TO FIX THE FOLLOWING
	fig2, axs2 = plt.subplots(2, 1, sharex=True)	
	fig2.autofmt_xdate()
	D = results[perbittrg]
	data = [ D[m] for m in metrics]
	width = 0.5/len(metrics)
	positions = [numpy.arange(len(data[0])) + i*width for i in range(len(data))]
	
	for d, pos in zip(data, positions):
#	for me, ma, mi in izip(data_means, data_maxes, data_mins):
		ppl.boxplot(axs2[0], d, sym='', positions=pos, widths=width, xticklabels=list(range(1, len(d)+1)))
	axs2[0].set_xlim(left=0, right=len(data[0]))
	#axs2[0].set_ylim(bottom=0, top=1.0)
	axs2[0].set_title('Mean error per bit (Trg)')
	
	#D = results[perbitfin]
	#data_means = [ numpy.mean(D[m], axis=1) for m in metrics]
	#data_maxes = [ numpy.max(D[m], axis=1) for m in metrics]
	#data_mins  = [ numpy.min(D[m], axis=1) for m in metrics]
	#width = 0.85/len(metrics)
	#ppl.bar(axs[3,1], numpy.arange(len(data)) + 0.5, data, grid='y')
	#axs[3,1].set_ylim(bottom=0)
	#axs[3,1].set_title('Mean error per bit (Final)')
	
	plt.savefig(filename + '_perbit.png', bbox_inches='tight', orientation='portrait')


print('\nFINAL ERROR')
for metric in metrics:
	print('{:{width}} : {:.3} +/- {:.3}'.format(metric, numpy.mean(results[finerr][metric]), numpy.std(results[finerr][metric]), width=maxwidth)) 
print('\n#COMPLETELY GENERALISED')
for metric in metrics:
	print('{:{width}} : {} / {}'.format(metric, results[finerr][metric].count(0.0), len(results[finerr][metric]), width=maxwidth)) 
print('\n#COMPLETELY MEMORISED')
for metric in metrics:
	print('{:{width}} : {} / {}'.format(metric, results[trgerr][metric].count(0.0), len(results[trgerr][metric]), width=maxwidth)) 
print('\n#GENERALISED / #MEMORISED')
for metric in metrics:
	if results[trgerr][metric].count(0.0) != 0:
		print('{:{width}} : {:.3}'.format(metric, results[finerr][metric].count(0.0) / float(results[trgerr][metric].count(0.0)), width=maxwidth)) 
	else:
		print('{:{width}} : {}'.format(metric, 0, width=maxwidth))
print('\n#GENERALISED / #MEMORISED / FIN_ERROR')
for metric in metrics:
	if results[trgerr][metric].count(0.0) != 0:
		print('{:{width}} : {:.3}'.format(metric, results[finerr][metric].count(0.0) / float(results[trgerr][metric].count(0.0)) / numpy.mean(results[finerr][metric]), width=maxwidth)) 
	else:
		print('{:{width}} : {}'.format(metric, 0, width=maxwidth))
print('\nNUM ITERATIONS TO TERMINATE')
for metric in metrics:
	print('{:{width}} : {:.3} +/- {:.3}'.format(metric, numpy.mean(results[iterfin][metric]), numpy.std(results[iterfin][metric]), width=maxwidth)) 
print('\nTime (s)')
for metric in metrics:
	print('{:{width}} : {:.3} +/- {:.3}'.format(metric, numpy.mean(results[time][metric]), numpy.std(results[time][metric]), width=maxwidth))

if '#iterBest' in categories:
	iterbest = categories.index('#iterBest')
	print('\nNUM ITERATIONS TO FIND BEST')
	for metric in metrics:
		print('{:{width}} : {:.3} +/- {:.3}'.format(metric, numpy.mean(results[iterbest][metric]), numpy.std(results[iterbest][metric]), width=maxwidth)) 

