import json
import sys
import numpy as np


def check_samples(samples):
    # check each row has distinct elements
    Ns, Ne = samples.shape
    for s in range(Ns):
        if len(set(samples[s])) < Ne:
            raise RuntimeError('Error: sample generated with duplicate indices')
        for s2 in range(s+1, Ns):
            if np.array_equal(samples[s], samples[s2]):
                print(('Warning: two identical samples generated (this is unlikely and '
                       'so has not been avoided) it is highly suggested to run this tool again.'))


def generate_and_dump_samples(Ni, num_samples, sample_size):
    # choose (Ns x Ne) random integers without replacement
    samples = np.zeros(shape=(num_samples, sample_size), dtype=np.int32)
    for i in range(num_samples):
        samples[i] = np.random.choice(2**Ni, size=sample_size, replace=False)

    # check the samples for errors
    check_samples(samples)

    # Dump to file
    with open('samples/samples_{}_{}_{}.json'.format(Ni, num_samples, sample_size), 'w') as fp:
        json.dump(samples.tolist(), fp)

if __name__ == '__main__':
    try:
        generate_and_dump_samples(int(sys.argv[1]), int(sys.argv[2]), int(sys.argv[3]))
    except:
        print('Usage: {} Ni num_samples sample_size'.format(sys.argv[0]))
