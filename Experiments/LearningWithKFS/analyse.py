from collections import defaultdict

import numpy
import sys
import json
from datetime import datetime, timedelta
import networkx as nx
from itertools import count

def gates_to_digraph(gates, Ni, No):
    DG = nx.DiGraph()
    DG.add_nodes_from(range(Ni))
    edges = []
    for g in range(len(gates)):
        edges.append((gates[g][0], g+Ni))
        edges.append((gates[g][1], g+Ni))
    DG.add_edges_from(edges)
    for i in range(Ni):
        letter = 'a' if i < Ni/2 else 'b'
        DG.node[i]['label'] = '{}{}'.format(letter, i)
        DG.node[i]['color'] = '#ff0000'
    for o in range(No):
        idx = DG.number_of_nodes() - No + o
        DG.node[idx]['label'] = 'o{}'.format(o)
        DG.node[idx]['color'] = '#0000ff'
    return DG

def find_dangling(net, No):
    dangling = set(net.nodes())
    for output in range(net.number_of_nodes() - No, net.number_of_nodes()):
        dangling -= {output}
        dangling -= set(nx.ancestors(net, output))
    return dangling

def generate_graphs(run_data):
	settings = run_data['settings']
	results = run_data['results']
	Ni = settings['Ni']
	No = settings['No']

	for result, i in zip(results, count()):
		graph = gates_to_digraph(result['Final Network'], Ni, No)
		dangling = find_dangling(graph, No)
		for d in dangling:
			graph.node[d]['color'] = '#00ff00'
		nx.write_graphml(graph, 'net_{}.graphml.gz'.format(i))

def collate(run_data):
	results = run_data['results']

	collated = defaultdict(list)
	for result in results:
		for key, val in result.items():
			collated[key].append(val)
	return collated

def compare(filenames):
	experiment_names = []
	collated_results = []
	for filename in filenames:
		with open(filename) as f:
			raw = json.load(f)
		collated_results.append( collate(raw) )
		experiment_names.append( raw['settings']['name'] )

	import prettyplotlib as ppl
	import matplotlib.pyplot as plt
	from matplotlib import rcParams
	rcParams.update({'figure.autolayout': True})

	attributes = ['Full Error (simple)',
				  'time']
	for attribute, plt_num in zip(attributes, count()):
		fig, axs = plt.subplots()	
		fig.autofmt_xdate()
		# fig.set_figwidth( 2 * fig.get_figwidth() )
		# fig.set_figheight( 2 * fig.get_figheight() )
		data = [result[attribute] for result in collated_results]
		positions = numpy.arange(len(data)) + 0.5
		ppl.boxplot(axs, data, positions=positions, xticklabels=experiment_names)
		axs.set_ylim(bottom=0)
		axs.set_title(attribute)
		plt.savefig(attribute + '.png', bbox_inches='tight', orientation='portrait')
	
	# 	data = [results[m][finerr] for m in metrics]
	# 	ppl.boxplot(axs[0,2], data, positions=positions)
	# 	axs[0,2].set_ylim(bottom=0)
	# 	axs[0,2].set_title('Full Error')
	
	# 	data = [results[m][trgacc] for m in metrics]
	# 	ppl.boxplot(axs[1,0], data, positions=positions)
	# 	axs[1,0].set_ylim(bottom=0, top=1.0)
	# 	axs[1,0].set_title('Training Accuracy')
		
	# 	data = [results[m][finacc] for m in metrics]
	# 	ppl.boxplot(axs[1,2], data, positions=positions)
	# 	axs[1,2].set_ylim(bottom=0, top=1.0)
	# 	axs[1,2].set_title('Full Accuracy')
				
	# 	data = [results[m][trgerr].count(0.0) + 1e-15 for m in metrics]
	# 	ppl.bar(axs[2,0], numpy.arange(len(data)) + 0.25, data, grid='y')
	# 	axs[2,0].set_ylim(top=max(1, max(data)))
	# 	axs[2,0].set_title('# Fully Memorised')
		
	# 	data = [results[m][finerr].count(0.0) + 1e-15 for m in metrics]
	# 	ppl.bar(axs[2,2], numpy.arange(len(data)) + 0.25, data, grid='y')
	# 	axs[2,2].set_ylim(top=max(1, max(data)))
	# 	axs[2,2].set_title('# Fully Generalised')
					
	# 	data = list(zip(*[results[m][perbittrg] for m in metrics]))
	# 	width = 0.5/len(data)
	# 	positions = [numpy.arange(len(data[0])) + i*width + 0.5 for i in range(len(data))]
	# 	for d, pos in zip(data, positions):
	# 		ppl.boxplot(axs[3,0], d, sym='', positions=pos, widths=width)
	# 	axs[3,0].set_xlim(left=0, right=len(data[0]))
	# 	axs[3,0].set_title('Mean error per bit (Trg)')
		
	# 	data = list(zip(*[results[m][perbitfin] for m in metrics]))
	# 	for d, pos in zip(data, positions):
	# 		ppl.boxplot(axs[3,2], d, sym='', positions=pos, widths=width)
	# 	axs[3,2].set_xlim(left=0, right=len(data[0]))
	# 	axs[3,2].set_title('Mean error per bit (Full)')
		
	# 	# make per bit error y-axis similar
	# 	ymax = max( [axs[3,x].get_ylim()[1] for x in range(3) ])
	# 	axs[3,0].set_ylim(top=ymax)
	# 	axs[3,1].set_ylim(top=ymax)
	# 	axs[3,2].set_ylim(top=ymax)

	# 	data = [results[m][iterbest] for m in metrics]
	# 	positions = numpy.arange(len(data)) + 0.5
	# 	ppl.boxplot(axs[4,0], data, positions=positions)
	# 	axs[4,0].set_ylim(bottom=0)
	# 	axs[4,0].set_title('Iteration for Best')
				
	# 	data = list(zip(*[results[m][depth] for m in metrics]))
	# 	width = 0.5/len(data)
	# 	positions = [numpy.arange(len(data[0])) + i*width + 0.5 for i in range(len(data))]
	# 	for d, pos in zip(data, positions):
	# 		ppl.boxplot(axs[4,1], d, sym='', positions=pos, widths=width)
	# 	axs[4,1].set_xlim(left=0, right=len(data[0]))
	# 	axs[4,1].set_title(depth)

	# 	plt.savefig(filename + '.png', bbox_inches='tight', orientation='portrait')
	

def main(argv):
	if len(argv) > 2:
		compare(argv[1:])
	elif len(argv) == 2:
		with open(argv[1]) as f:
			data = json.load(f)
			generate_graphs(data)
	else:
		print('ARGS!')

if __name__ == '__main__':
	main(sys.argv)
