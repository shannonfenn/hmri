#!../../HMRIPYENV/bin/python3

from copy import copy, deepcopy
from itertools import count
from datetime import datetime
from os import mkdir, getcwd
from os.path import join, isdir, relpath
import numpy as np
import json
import sys
import shutil
import subprocess
import logging

# Own code
from genData import *

def dump_results(results, stream, indent=2):
    ''' Dumps experiment results to json file. '''
    st = ' '*indent
    stream.write(st + '\"results\": [\n')
    # for each result
    for r, i in zip(results, count()):
        if i: stream.write(',\n')
        stream.write(st*2 + '{\n')
        # write only the first level of result dict in pretty form
        for (k, v), i2 in zip(iter(r.items()), count()):
            if i2: stream.write(',\n')        
            stream.write(st*3 + '\"{}\": '.format(k))
            # compact json dump for further levels
            json.dump(v, stream)
        stream.write('\n' + st*2 + '}') 
    stream.write('\n' + st + ']')

def dump_settings(settings, stream, indent=2):
    ''' Dumps experiment parameters to json file. '''
    st = ' '*indent
    stream.write(st + '\"settings\": {\n')
    # write only the first level of settings dict in pretty form
    for (k, v), i in zip(iter(settings.items()), count()):
        if i: stream.write(',\n')
        stream.write(st*2 + '\"{}\": '.format(k))
        # compact json dump for further levels
        json.dump(v, stream) 
    stream.write('\n' + st + '}')


def dump(data, stream):
    ''' Dumps experiment information to file. '''
    stream.write('{\n')
    dump_settings(data['settings'], stream)
    stream.write(',\n')
    dump_results(data['results'], stream)
    stream.write('\n}')

def check_git(repo_dir, code_dir):
    ''' Checks the git directory for uncommitted source modifications. 
        Since the experiment is tagged with a git hash it should not be
        run while the repo is dirty.'''
    exp_dir = relpath(getcwd(), repo_dir)
    changed_files = subprocess.check_output(['git', 'diff', '--name-only'], universal_newlines=True).splitlines()
    changed_files = [s for s in changed_files if s.startswith(code_dir) or (s.startswith(exp_dir) and s.endswith('.py')) ]

    warning_string = ('Warning, the following files in git repo have changes:\n'
                      '\t{}\nRun anyway? [y/N]: ').format('\n\t'.join(changed_files))

    if changed_files:
        return input(warning_string) in ['y', 'Y']
    else:
        return True

def create_result_dir(build_dir, experiment_filename, exp_name, now):
    ''' Generates a new timestamped directory for the results and copies 
        the experiment script, experiment file and git hash into it.'''
    # make directory for the results
    result_dir = 'Results_{}_{}'.format(exp_name, now.strftime('%y-%m-%d_%H-%M'))

    # find the first number i such that 'Results_datetime_i' is not
    # already a directory and make that new directory
    result_dir += '_'
    result_dir += next(str(i) for i in count() if not isdir(result_dir + str(i)))
    mkdir(result_dir)

    #copy experiment files and git hash into results directory
    shutil.copy(__file__, join(result_dir, 'exp_script') )
    shutil.copy(experiment_filename, join(result_dir, experiment_filename) )
    with open(join(result_dir, 'git_hash_at_time_of_exp'), 'w') as hashfile:
        hashfile.write(subprocess.check_output(['git', 'rev-parse', 'HEAD'], universal_newlines=True))

    # return the directory name for the results to go in
    return result_dir

def load_experiment(experiment_filename) :
    ''' Loads experiment parameters from json file.'''
    with open(experiment_filename) as exp_file:
        settings = json.load(exp_file)
        ds_filename = settings['dataset filename']
        with open(ds_filename) as ds_file:
            ds_settings = json.load(ds_file)

    settings.update(ds_settings)
    return settings

def initialise_logging(settings, result_dir):    
    ''' sets up logging with user defined level. '''
    level_dict = {'warning' : logging.WARNING, 
                  'info': logging.INFO, 
                  'debug': logging.DEBUG,
                  'none': logging.ERROR}
    log_level = level_dict[settings['logging level'].lower()] if 'logging level' in settings else logging.WARNING
    logging.basicConfig(filename=join(result_dir, 'log'), level=log_level)


def compile_cython(build_dir, result_dir):
    ''' Compiles the cython code and appends the path of the resulting
    lib to the system path.'''
    # create cython build output file
    with open(join(result_dir, 'cython_build_output'), 'w') as f:
        # build cython code
        subprocess.check_call(['python3', 'setup.py', 'build_ext', '--inplace'], 
                            cwd=build_dir,
                            stdout=f, stderr=f,
                            universal_newlines=True)

    sys.path.append('../../Code/BooleanNet/Python/')

# def run_experiment(repo_dir, build_subdir, experiment_filename):
#     ''' Runs the provided experiment and dumps results and settings
#         to a new directory.'''
    

if __name__ == '__main__':
    # Check version
    if sys.version_info.major != 3:
        sys.exit("Requires python 3.")

    repo_dir = '/home/shannon/HMRI'
    build_subdir = 'Code/BooleanNet/Python'
    if len(sys.argv) == 2:
        experiment_filename = sys.argv[1]
    else:
        print('No experiment given, attempting to use \'experiment.json\'.')
        experiment_filename = 'experiment.json'

    # run_experiment(repo_dir, build_subdir, experiment_filename)
    
    build_dir = join(repo_dir, build_subdir)

    start_time = datetime.now()

    if not check_git(repo_dir, build_subdir):
        sys.exit()

    settings = load_experiment(experiment_filename)

    result_dir = create_result_dir(build_dir,
                                   'experiment.json',
                                   settings['name'],
                                   datetime.now())

    compile_cython(build_dir, result_dir)

    # import own code - must be done after cython compile
    from learnBoolNet import learn_bool_net

    initialise_logging(settings, result_dir)

    end_time_set = datetime.now()

    function = np.array(settings['function'])
    num_samples = settings['num samples']
    sample_size = settings['sample size']

    settings['inter file base'] = join(result_dir, 'inter_')

    training_indices = np.random.randint(len(function), size=(num_samples, sample_size))
    training_sets = function[training_indices]
    # samples_indices = [random.sample(len(function)), sample_size) for i in xrange(num_samples)]

    # run for each training set
    results = [learn_bool_net(settings, trg_set, function) for trg_set in training_sets]

    # add training sets to settings so they can be logged
    settings['sample indices'] = training_indices.tolist()
    settings['sample sets'] = training_sets.tolist()

    end_time_exp = datetime.now()

    # TODO: interface directly with pythong analysis code

    with open(join(result_dir, 'results.json'), 'w') as stream:
        # json.dump({'settings': settings, 'results': results}, stream)
        dump({'settings': settings, 'results': results}, stream)

    end_time_print = datetime.now()
    
    print(('time taken \n\tsetup: {}\n\texp: {}\n\tprinting: {}'
        '\n\ttotal: {}').format(end_time_set - start_time,
                                end_time_exp - end_time_set,
                                end_time_print - end_time_exp,
                                end_time_print - start_time))