from copy import deepcopy               # for copying settings dicts
from itertools import count             # for indexing in for loops
from datetime import datetime           # for date for result dir
from os import mkdir, getcwd            # for mkdir and cwd
from os.path import join, isdir, relpath, splitext   # for path manipulation
from collections import MutableMapping  # for updating nested dicts
from numpy.random import randint
import numpy as np                      # inputs and outputs are ndarrays
import json                             # for outputting results
import yaml                             # for loading experiment files
import sys                              # for path, exit
import shutil                           # for copying
import subprocess                       # for git and cython compile
import logging                          # for logging, duh
import argparse
import re
from progress.bar import Bar            # for progress bars

sys.path.append('../../Code/BooleanNet/Python/')
from learnBoolNet import learn_bool_net


def update_nested(d, u):
    ''' this updates a dict with another where the two may contain nested
        dicts themselves (or more generally nested mutable mappings). '''
    for k, v in u.items():
        if isinstance(v, MutableMapping):
            r = update_nested(d.get(k, {}), v)
            d[k] = r
        else:
            d[k] = u[k]
    return d


def dump_results_partial(results, stream):
    ''' Dumps experiment results to json file. '''
    # for each result
    for r, i in zip(results, count()):
        if i > 0:
            stream.write(',\n')
        stream.write('  {\n')
        # write only the first level of result dict in pretty form
        for (k, v), i2 in zip(iter(r.items()), count()):
            if i2:
                stream.write(',\n')
            stream.write('    \"{}\": '.format(k))
            # compact json dump for further levels
            json.dump(v, stream)
        stream.write('\n  }')


def dump_settings(settings, stream):
    ''' Dumps experiment parameters to json file. '''
    stream.write('{\n')
    # write only the first level of settings dict in pretty form
    for (k, v), i in zip(iter(settings.items()), count()):
        if i:
            stream.write(',\n')
        stream.write('  \"{}\": '.format(k))
        # compact json dump for further levels
        json.dump(v, stream)
    stream.write('\n}')


def check_git(repo_dir, code_dir):
    ''' Checks the git directory for uncommitted source modifications.
        Since the experiment is tagged with a git hash it should not be
        run while the repo is dirty.'''
    exp_dir = relpath(getcwd(), repo_dir)
    changed_files = subprocess.check_output(
        ['git', 'diff', '--name-only'], universal_newlines=True).splitlines()
    code_re = re.compile('\\A({}|{}.+\\.py\\Z)'.format(code_dir, exp_dir))
    changed_files = [s for s in changed_files if code_re.match(s)]
    return True

    # warning_string = ('Warning, the following files in git repo '
    #                   'have changes:\n\t{}\nRun anyway? [y/N]: '
    #                   ).format('\n\t'.join(changed_files))

    # if changed_files:
    #     return input(warning_string) in ['y', 'Y']
    # else:
    #     return True


def create_result_dir(build_dir, experiment_filename, exp_name, now):
    ''' Generates a new timestamped directory for the results and copies
        the experiment script, experiment file and git hash into it.'''
    # make directory for the results
    result_dir = 'Results_{}_{}'.format(
        exp_name, now.strftime('%y-%m-%d_%H-%M'))

    # find the first number i such that 'Results_datetime_i' is not
    # already a directory and make that new directory
    result_dir += '_'
    result_dir += next(str(i) for i in count()
                       if not isdir(result_dir + str(i)))
    mkdir(result_dir)

    # copy experiment files and git hash into results directory
    shutil.copy(__file__, join(result_dir, 'exp_script'))
    shutil.copy(experiment_filename, join(result_dir, experiment_filename))
    with open(join(result_dir, 'git_hash_at_time_of_exp'), 'w') as hashfile:
        hashfile.write(subprocess.check_output(['git', 'rev-parse', 'HEAD'],
                       universal_newlines=True))

    # return the directory name for the results to go in
    return result_dir


def initialise_logging(settings, result_dir):
    ''' sets up logging with user defined level. '''
    level_dict = {'warning': logging.WARNING,
                  'info': logging.INFO,
                  'debug': logging.DEBUG,
                  'none': logging.ERROR}
    if 'logging level' in settings:
        log_level = level_dict[settings['logging level'].lower()]
    else:
        log_level = logging.WARNING
    logging.basicConfig(filename=join(result_dir, 'log'), level=log_level)


# def compile_cython(build_dir, result_dir):
#     ''' Compiles the cython code and appends the path of the resulting
#     lib to the system path.'''
#     # create cython build output file
#     with open(join(result_dir, 'cython_build_output'), 'w') as f:
#         # build cython code
#         subprocess.check_call(
#             ['python3', 'setup.py', 'build_ext', '--inplace'],
#             cwd=build_dir, stdout=f, stderr=f,
#             universal_newlines=True)


def parse_arguments():
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('experiment',
                        type=argparse.FileType('r'),
                        default='experiment.yaml')
    parser.add_argument('--evaluator', default='py', choices=['py', 'cy', 'gpu'])

    args = parser.parse_args()

    print(args.evaluator)

    if args.evaluator == 'gpu':
        from NetworkEvaluatorGPU import NetworkEvaluatorGPU
        evaluator_class = NetworkEvaluatorGPU
    elif args.evaluator == 'cy':
        import pyximport
        pyximport.install()
        from NetworkEvaluatorCython import NetworkEvaluatorCython
        evaluator_class = NetworkEvaluatorCython
    else:
        from NetworkEvaluator import NetworkEvaluator
        evaluator_class = NetworkEvaluator

    return args.experiment, evaluator_class


def initialise(repo_dir, build_subdir, experiment_file):
    ''' This is responsible for setting up the output directory,
        checking the git repository for cleanliness, setting up
        logging and compiling the cython code for boolean nets.'''
    # Check version
    if sys.version_info.major != 3:
        sys.exit("Requires python 3.")

    build_dir = join(repo_dir, build_subdir)

    if not check_git(repo_dir, build_subdir):
        sys.exit()

    # load experiment file
    settings = yaml.load(experiment_file, Loader=yaml.CSafeLoader)

    # create result directory
    result_dir = create_result_dir(build_dir,
                                   experiment_file.name,
                                   settings['name'],
                                   datetime.now())
    # compile cython code
    # this should only be done depending on the evaluator
    # compile_cython(build_dir, result_dir)

    # initialise logging
    initialise_logging(settings, result_dir)

    return settings, result_dir


def partition(function, training_indices):
    ''' Parititions the given function into training and test sets,
        based on the given training indices.'''
    # Parameters
    N = function.shape[0]
    Ns, Nt = training_indices.shape
    # Generate the test indices array, each row should contain all
    # indices not in the equivalent row of training_indices
    test_indices = np.zeros(shape=(Ns, N - Nt), dtype=int)
    for s in range(Ns):
        test_indices[s] = np.setdiff1d(np.arange(N), training_indices[s])
    # Using numpy's advanced indexing we can get the sets
    training_sets = function[training_indices]
    test_sets = function[test_indices]
    return training_sets, test_sets


def run_configuration(settings, results_stream, settings_stream,
                      evaluator_class):
    ''' runs a given configuration and dumps settings and results to
        the given streams. '''

    # load data set from file
    with open(settings['dataset filename']) as ds_file:
        ds_settings = json.load(ds_file)
    settings.update(ds_settings)
    sample_settings = settings['sampling']

    # load function
    function = np.array(settings['function'])

    # load samples from file
    if sample_settings['method'] == 'given':
        # prepate filename
        ds_base_name, ds_ext = splitext(settings['dataset filename'])
        sample_filename = (ds_base_name +
                           sample_settings['filename suffix'] +
                           ds_ext)
        # open and load file
        with open(sample_filename) as sample_file:
            training_indices = np.array(json.load(sample_file))
    # generate samples
    elif sample_settings['method'] == 'generated':
        s_num = sample_settings['number']
        s_size = sample_settings['size']
        # generate
        training_indices = randint(len(function), size=(s_num, s_size))
    else:
        raise ValueError('Invalid sampling method {}'.format(
                         sample_settings['method']))

    # partition the sets based on loaded indices
    training_sets, test_sets = partition(function, training_indices)

    # add training sets to settings so they can be logged
    settings['sample indices'] = training_indices.tolist()
    settings['sample sets'] = training_sets.tolist()

    # dump the configuration settings out
    dump_settings(settings, settings_stream)

    # progress bar
    bar = Bar('Running Configuration',
              max=len(training_sets),
              suffix='%(eta)ds')
    bar.update()
    # run for each training set
    results = []
    for trg_set, test_set, set_no in zip(training_sets, test_sets, count()):
        results.append(
            learn_bool_net(settings, trg_set, test_set, evaluator_class))
        results[-1]['trg_set_no'] = set_no
        update_nested(results[-1], configuration)
        bar.next()

    # write this configuration's results out
    dump_results_partial(results, results_stream)


# ############################## MAIN ####################################### #
if __name__ == '__main__':

    experiment_file, evaluator_class = parse_arguments()

    # run_experiment(repo_dir, build_subdir, experiment_filename)

    settings, result_dir = initialise('/home/shannon/HMRI',
                                      'Code/BooleanNet/Python',
                                      experiment_file)

    settings['inter file base'] = join(result_dir, 'inter_')

    # ######## THIS IS WHERE CONFIGURATIONS START COMING IN ############ #
    s = join(result_dir, 'settings.json')
    r = join(result_dir, 'results.json')
    with open(s, 'w') as settings_stream, open(r, 'w') as results_stream:

        # the configurations approach involves essentially having
        # a new settings dict for each configuration and updating
        # it with values in each dict in the configurations list
        configuration_settings = deepcopy(settings)
        # no need to keep this sub-dict around
        configuration_settings.pop('configurations')

        first = True
        for configuration, c_count in zip(settings['configurations'], count()):

            print('Configuration {} of {}'.format(
                c_count+1, len(settings['configurations'])))

            settings_stream.write('[\n' if first else ',\n')
            results_stream.write('[\n' if first else ',\n')
            first = False

            # update the settings dict with the values for this configuration
            update_nested(configuration_settings, configuration)

            # run the configuration
            run_configuration(configuration_settings, results_stream,
                              settings_stream, evaluator_class)

        settings_stream.write('\n]')
        results_stream.write('\n]')
