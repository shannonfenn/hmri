from copy import deepcopy               # for copying settings dicts
from datetime import datetime           # for date for result dir
from os import mkdir, getcwd            # for mkdir and cwd
from os.path import join, isdir, relpath, splitext   # for path manipulation
from collections import MutableMapping  # for updating nested dicts
from numpy.random import randint
from multiprocessing import Pool
import numpy as np                      # inputs and outputs are ndarrays
import json                             # for outputting results
import yaml                             # for loading experiment files
import sys                              # for path, exit
import shutil                           # for copying
import subprocess                       # for git and cython compile
import logging                          # for logging, duh
import argparse
import re
import itertools

sys.path.append('../../Code/BooleanNet/Python/')
from learnBoolNet import learn_bool_net


def update_nested(d, u):
    ''' this updates a dict with another where the two may contain nested
        dicts themselves (or more generally nested mutable mappings). '''
    for k, v in u.items():
        if isinstance(v, MutableMapping):
            r = update_nested(d.get(k, {}), v)
            d[k] = r
        else:
            d[k] = u[k]
    return d


def dump_results(results, stream):
    ''' Dumps experiment results to json file. '''
    # for each result
    stream.write('[\n')
    for i, r in enumerate(results):
        if i > 0:
            stream.write(',\n')
        stream.write('  {\n')
        # write only the first level of result dict in pretty form
        for i2, (k, v) in enumerate(r.items()):
            if i2:
                stream.write(',\n')
            stream.write('    \"{}\": '.format(k))
            # compact json dump for further levels
            json.dump(v, stream)
        stream.write('\n  }')
    stream.write('\n]')


def dump_settings(settings, stream):
    ''' Dumps experiment parameters to json file. '''
    stream.write('{\n')
    # write only the first level of settings dict in pretty form
    for i, (k, v) in enumerate(settings.items()):
        if i:
            stream.write(',\n')
        stream.write('  \"{}\": '.format(k))
        # compact json dump for further levels
        json.dump(v, stream)
    stream.write('\n}')


def check_git(repo_dir, code_dir):
    ''' Checks the git directory for uncommitted source modifications.
        Since the experiment is tagged with a git hash it should not be
        run while the repo is dirty.'''
    exp_dir = relpath(getcwd(), repo_dir)
    changed_files = subprocess.check_output(
        ['git', 'diff', '--name-only'], universal_newlines=True).splitlines()
    code_re = re.compile('\\A({}|{}.+\\.py\\Z)'.format(code_dir, exp_dir))
    changed_files = [s for s in changed_files if code_re.match(s)]

    print(('Warning, the following files in git repo '
           'have changes:\n\t{}').format('\n\t'.join(changed_files)))


def create_result_dir(build_dir, experiment_filename, exp_name, now):
    ''' Generates a new timestamped directory for the results and copies
        the experiment script, experiment file and git hash into it.'''
    # make directory for the results
    result_dir = 'Results_{}_{}_'.format(
        now.strftime('%y-%m-%d'), exp_name)

    # find the first number i such that 'Results_datetime_i' is not
    # already a directory and make that new directory
    result_dir += next(str(i) for i in itertools.count()
                       if not isdir(result_dir + str(i)))
    mkdir(result_dir)

    # copy experiment files and git hash into results directory
    shutil.copy(__file__, join(result_dir, 'exp_script'))
    shutil.copy(experiment_filename, join(result_dir, experiment_filename))
    with open(join(result_dir, 'git_hash_at_time_of_exp'), 'w') as hashfile:
        hashfile.write(subprocess.check_output(['git', 'rev-parse', 'HEAD'],
                       universal_newlines=True))

    # return the directory name for the results to go in
    return result_dir


def initialise_logging(settings, result_dir):
    ''' sets up logging with user defined level. '''
    level_dict = {'warning': logging.WARNING,
                  'info': logging.INFO,
                  'debug': logging.DEBUG,
                  'none': logging.ERROR}
    if 'logging level' in settings:
        log_level = level_dict[settings['logging level'].lower()]
    else:
        log_level = logging.WARNING
    logging.basicConfig(filename=join(result_dir, 'log'), level=log_level)


def parse_arguments():
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('experiment',
                        type=argparse.FileType('r'),
                        default='experiment.yaml')
    parser.add_argument('--evaluator', default='py', choices=['py', 'cy', 'gpu'])
    parser.add_argument('--numprocs', '-n', metavar='N', type=int,
                        default=1, choices=range(1, 17))

    args = parser.parse_args()

    if args.evaluator == 'gpu':
        from NetworkEvaluatorGPU import NetworkEvaluatorGPU
        evaluator_class = NetworkEvaluatorGPU
    elif args.evaluator == 'cy':
        import pyximport
        pyximport.install()
        from NetworkEvaluatorCython import NetworkEvaluatorCython
        evaluator_class = NetworkEvaluatorCython
    else:
        from NetworkEvaluator import NetworkEvaluator
        evaluator_class = NetworkEvaluator

    return args.experiment, evaluator_class, args.numprocs


def initialise(repo_dir, build_subdir, experiment_file):
    ''' This is responsible for setting up the output directory,
        checking the git repository for cleanliness, setting up
        logging and compiling the cython code for boolean nets.'''
    # Check version
    if sys.version_info.major != 3:
        sys.exit("Requires python 3.")

    build_dir = join(repo_dir, build_subdir)

    check_git(repo_dir, build_subdir)

    # load experiment file
    settings = yaml.load(experiment_file, Loader=yaml.CSafeLoader)

    settings['dataset dir'] = join(repo_dir, 'Experiments/DataSets/')

    # create result directory
    result_dir = create_result_dir(build_dir,
                                   experiment_file.name,
                                   settings['name'],
                                   datetime.now())

    # initialise logging
    initialise_logging(settings, result_dir)

    return settings, result_dir


def partition(function, training_indices):
    ''' Parititions the given function into training and test sets,
        based on the given training indices.'''
    # Parameters
    N = function.shape[0]
    Ns, Nt = training_indices.shape
    # Generate the test indices array, each row should contain all
    # indices not in the equivalent row of training_indices
    test_indices = np.zeros(shape=(Ns, N - Nt), dtype=int)
    for s in range(Ns):
        test_indices[s] = np.setdiff1d(np.arange(N), training_indices[s])
    # Using numpy's advanced indexing we can get the sets
    training_sets = function[training_indices]
    test_sets = function[test_indices]
    return training_sets, test_sets


def load_datasets(settings):
    # load data set from file
    dataset_dir = settings['dataset dir']
    with open(join(dataset_dir, 'functions', settings['dataset'])) as ds_file:
        ds_settings = json.load(ds_file)
    settings.update(ds_settings)
    sample_settings = settings['sampling']

    # load function
    function = np.array(settings['function'])
    # load samples from file
    if sample_settings['method'] == 'given':
        # prepate filename
        sample_filename = join(dataset_dir, 'samples', 'samples_{}_{}_{}.json'.format(
            settings['Ni'], sample_settings['Ns'], sample_settings['Ne']))
        # open and load file
        with open(sample_filename) as sample_file:
            training_indices = np.array(json.load(sample_file))
    # generate samples
    elif sample_settings['method'] == 'generated':
        s_num = sample_settings['number']
        s_size = sample_settings['size']
        # generate
        training_indices = randint(len(function), size=(s_num, s_size))
    else:
        raise ValueError('Invalid sampling method {}'.format(
                         sample_settings['method']))
    # partition the sets based on loaded indices
    training_sets, test_sets = partition(function, training_indices)
    # add training sets to settings so they can be logged
    settings['sample indices'] = training_indices.tolist()
    settings['sample sets'] = training_sets.tolist()

    return training_sets, test_sets


def run_parallel(tasks, num_processes):
    ''' runs the given configurations '''
    print('parallelising')
    with Pool(processes=num_processes) as pool:
        results = pool.starmap(learn_bool_net, tasks)
    return results


def run_sequential(tasks):
    ''' runs the given configurations '''
    print('running sequentially')
    results = itertools.starmap(learn_bool_net, tasks)
    return results


def generate_configurations(settings, settings_stream, results_stream):
    # the configurations approach involves essentially having
    # a new settings dict for each configuration and updating
    # it with values in each dict in the configurations list
    configurations = settings['configurations']

    settings = deepcopy(settings)
    # no need to keep this sub-dict around
    settings.pop('configurations')

    # Build up the task list
    tasks = []
    first = True
    for configuration in configurations:
        settings_stream.write('[\n' if first else ',\n')
        first = False

        # keep each configuration isolated
        configuration_settings = deepcopy(settings)

        # update the settings dict with the values for this configuration
        update_nested(configuration_settings, configuration)

        # load the data for this configuration
        training_sets, test_sets = load_datasets(configuration_settings)
        # dump the configuration settings out
        dump_settings(configuration_settings, settings_stream)

        # for each training set
        for set_no, (trg_set, test_set) in enumerate(zip(training_sets, test_sets)):
            configuration_settings['training set number'] = set_no
            tasks.append((deepcopy(configuration_settings), trg_set, test_set, evaluator_class))
    settings_stream.write('\n]')
    return tasks


# ############################## MAIN ####################################### #
if __name__ == '__main__':

    experiment_file, evaluator_class, numprocs = parse_arguments()

    # run_experiment(repo_dir, build_subdir, experiment_filename)

    settings, result_dir = initialise('/home/shannon/HMRI',
                                      'Code/BooleanNet/Python',
                                      experiment_file)

    settings['inter file base'] = join(result_dir, 'inter_')

    # ######## THIS IS WHERE CONFIGURATIONS START COMING IN ############ #
    s = join(result_dir, 'settings.json')
    r = join(result_dir, 'results.json')
    with open(s, 'w') as settings_stream, open(r, 'w') as results_stream:
        # generate learning tasks
        tasks = generate_configurations(settings, settings_stream, results_stream)
        print('{} runs generated.'.format(len(tasks)))
        # Run the actual learning as a parallel process
        if numprocs > 1:
            results = run_parallel(tasks, numprocs)
        else:
            results = run_sequential(tasks)
        dump_results(results, results_stream)

    print("Experiment complete.")
