import sys
import random
import json

# def addition_data_decimal(Ni):
#     # Upper limit
#     upper = 2**Ni
#     # generate tuples of (input1, input2, output)
#     dataset = [(i1, i2, (i1+i2)%upper ) for i1 in xrange(upper) for i2 in xrange(upper)]
#     return dataset


def to_binary(value, num_bits):
    # little-endian
    return [int(i) for i in '{:0{w}b}'.format(value, w=num_bits)][::-1]


def addition_function(num_bits_per_operand):
    # Upper limit
    upper = 2**num_bits_per_operand
    # generate dict for function
    function = {i1*upper + i2: (i1+i2) % upper
                for i1 in range(upper)
                for i2 in range(upper)}
    return function


def samples(function, numSets, samplesPerSet):
    return [random.sample(range(len(function)), samplesPerSet) for i in range(numSets)]


if __name__ == '__main__':
    n = int(sys.argv[1])
    filename = sys.argv[2]

    func = addition_function(n)
    d = {'Ni': n*2,
         'No': n,
         'function': [[to_binary(k, n*2), to_binary(v, n)]
                      for k, v in func.items()]}

    # use JSON since YAML is so damn slow to en/decode
    with open(filename, 'w') as f:
        json.dump(d, f)
