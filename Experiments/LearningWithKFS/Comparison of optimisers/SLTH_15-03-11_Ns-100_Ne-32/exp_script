from datetime import datetime               # for date for result dir
from os import mkdir, getcwd                # for mkdir and cwd
from os.path import join, isdir, relpath    # for path manipulation
from multiprocessing import Pool
import yaml                                 # for loading experiment files
import sys                                  # for path, exit
import shutil                               # for copying
import subprocess                           # for git and cython compile
import logging                              # for logging, duh
import argparse
import re
import itertools

sys.path.append('../../Code/Python/')
from BoolNet.learnBoolNet import learn_bool_net
import experiment as ex


def check_git(repo_dir, code_dir):
    ''' Checks the git directory for uncommitted source modifications.
        Since the experiment is tagged with a git hash it should not be
        run while the repo is dirty.'''
    exp_dir = relpath(getcwd(), repo_dir)
    changed_files = subprocess.check_output(
        ['git', 'diff', '--name-only'], universal_newlines=True).splitlines()
    code_re = re.compile('\\A({}|{}.+\\.py\\Z)'.format(code_dir, exp_dir))
    changed_files = [s for s in changed_files if code_re.match(s)]

    if changed_files:
        print(('Warning, the following files in git repo '
               'have changes:\n\t{}').format('\n\t'.join(changed_files)))


def create_result_dir(build_dir, experiment_filename, exp_name, now):
    ''' Generates a new timestamped directory for the results and copies
        the experiment script, experiment file and git hash into it.'''
    # make directory for the results
    result_dir = 'Results_{}_{}_'.format(
        exp_name, now.strftime('%y-%m-%d'))

    # find the first number i such that 'Results_datetime_i' is not
    # already a directory and make that new directory
    result_dir += next(str(i) for i in itertools.count()
                       if not isdir(result_dir + str(i)))
    mkdir(result_dir)

    # copy experiment files and git hash into results directory
    shutil.copy(__file__, join(result_dir, 'exp_script'))
    shutil.copy(experiment_filename, join(result_dir, experiment_filename))
    with open(join(result_dir, 'git_hash_at_time_of_exp'), 'w') as hashfile:
        hashfile.write(subprocess.check_output(['git', 'rev-parse', 'HEAD'],
                       universal_newlines=True))

    # return the directory name for the results to go in
    return result_dir


def initialise_logging(settings, result_dir):
    ''' sets up logging with user defined level. '''
    level_dict = {'warning': logging.WARNING,
                  'info': logging.INFO,
                  'debug': logging.DEBUG,
                  'none': logging.ERROR}
    try:
        log_level = level_dict[settings['logging_level'].lower()]
    except:
        log_level = logging.WARNING
    logging.basicConfig(filename=join(result_dir, 'log'), level=log_level)


def parse_arguments():
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('experiment',
                        type=argparse.FileType('r'),
                        default='experiment.yaml')
    parser.add_argument('--evaluator', default='cy', choices=['py', 'cy', 'gpu'])
    parser.add_argument('--numprocs', '-n', metavar='N', type=int,
                        default=8, choices=range(1, 17))

    args = parser.parse_args()

    if args.evaluator == 'gpu':
        from BoolNet.NetworkEvaluatorGPU import NetworkEvaluatorGPU
        evaluator_class = NetworkEvaluatorGPU
    elif args.evaluator == 'cy':
        import pyximport
        pyximport.install()
        from BoolNet.NetworkEvaluatorCython import NetworkEvaluatorCython
        evaluator_class = NetworkEvaluatorCython
    else:
        from BoolNet.NetworkEvaluator import NetworkEvaluator
        evaluator_class = NetworkEvaluator

    return args.experiment, evaluator_class, args.numprocs


def initialise(repo_dir, build_subdir, experiment_file):
    ''' This is responsible for setting up the output directory,
        checking the git repository for cleanliness, setting up
        logging and compiling the cython code for boolean nets.'''
    # Check version
    if sys.version_info.major != 3:
        sys.exit("Requires python 3.")

    build_dir = join(repo_dir, build_subdir)

    check_git(repo_dir, build_subdir)

    # load experiment file
    settings = yaml.load(experiment_file, Loader=yaml.CSafeLoader)

    settings['dataset_dir'] = join(repo_dir, 'Experiments/DataSets/')

    # create result directory
    result_dir = create_result_dir(build_dir,
                                   experiment_file.name,
                                   settings['name'],
                                   datetime.now())

    # create a temp subdir
    mkdir(join(result_dir, 'temp'))

    # initialise logging
    initialise_logging(settings, result_dir)

    return settings, result_dir


def run_parallel(tasks, num_processes):
    ''' runs the given configurations '''
    print('parallelising')
    with Pool(processes=num_processes) as pool:
        results = pool.starmap(learn_bool_net, tasks)
    return results


def run_sequential(tasks):
    ''' runs the given configurations '''
    print('running sequentially')
    results = itertools.starmap(learn_bool_net, tasks)
    return results


# ############################## MAIN ####################################### #
if __name__ == '__main__':

    experiment_file, evaluator_class, numprocs = parse_arguments()

    # run_experiment(repo_dir, build_subdir, experiment_filename)

    settings, result_dir = initialise('/home/shannon/HMRI',
                                      'Code/BooleanNet/Python',
                                      experiment_file)

    settings['inter_file_base'] = join(result_dir, 'temp', 'inter_')

    s = join(result_dir, 'settings.json')
    r = join(result_dir, 'results.json')
    with open(s, 'w') as settings_stream, open(r, 'w') as results_stream:
        # generate learning tasks
        configurations = ex.generate_configurations(settings, evaluator_class)
        ex.dump_configurations(configurations, settings_stream)
        print('{} runs generated.'.format(len(configurations)))
        # Run the actual learning as a parallel process
        if numprocs > 1:
            results = run_parallel(configurations, numprocs)
        else:
            results = run_sequential(configurations)
        ex.dump_results(results, results_stream)

    print('Experiment complete. Results in \"{}\"'.format(result_dir))
