#!/bin/bash
STARTTIME=$(date +%s)

EXP_DIR=$(pwd)
BUILD_DIR="/home/shannon/HMRI/BooleanNet/C++/build-BooleanNet-Desktop-Release/"

cd $BUILD_DIR
make
make_ret=$?
cd $EXP_DIR

if [ $make_ret -ne 0 ]; then
	echo 'make failed, aborting.'
else
	cp "$BUILD_DIR"BooleanNet $EXP_DIR

	echo "          metric             trgerror(guiding)    trgerror(simple)    error(guiding)     error(simple)      accuracy        #iterBest     #iterFinish      time(s)" > results # Clear the results file

	parallel -j7 < commands >> results

	CMDENDTIME=$(date +%s)

	echo "Finished : $[$CMDENDTIME - $STARTTIME]s"
fi
