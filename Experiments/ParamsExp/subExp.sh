#!/bin/bash

# Parameters
Ni=$1
Ne=$2
Ng=$3
Nt=$4
Ns=$5
T=$6
Ts=$7
N=$8
methods=$9

STARTTIME=$(date +%s)

echo "          metric             trgerror(guiding)    trgerror(simple)    error(guiding)     error(simple)      accuracy        #iterBest     #iterFinish      time(s)" > results # Clear the results file

# generate N copies of commands	
echo -n "" > commands
for i in $(seq 1 $N);
do 
	for method in $methods
	do
		echo "./BooleanNet anneal givensamples Data/data Data/data_samples_$i $Ng $Nt $Ns $T $Ts $method" >> commands
	done
done

# run these commands 8 at a time
parallel -j8 < commands >> Results/results
# remove temp commands file

CMDENDTIME=$(date +%s)
echo "Sub Exp Finished : $[$CMDENDTIME - $STARTTIME] s"
