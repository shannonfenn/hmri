import sys

if len(sys.argv) == 2:
	Ni = int(sys.argv[1])

	f = open('Data/data', 'w')

	f.write( '{} {}\n'.format(2*Ni, Ni) )

	for i in range(2**Ni):
		s1 = ' '.join( '{:0{width}b}'.format(i, width=Ni) )
		for k in range(2**Ni):
			s2 = ' '.join( '{:0{width}b}'.format(k, width=Ni) )
			s3 = ' '.join( '{:0{width}b}'.format(i+k, width=Ni+1)[1:] )
			f.write(s1 + ' ' + s2 + ' ' + s3 + '\n')
	f.close()
else:
	print('Usage: ' + sys.argv[0] + ' num_bits_per_operand')
