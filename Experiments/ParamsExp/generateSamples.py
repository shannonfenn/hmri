import sys
import argparse
import random

if not ( 4 <= len(sys.argv) <= 5):
	sys.exit("Usage " + sys.argv[0] + " filename #sets #samplesPerSet [ordering]")
	
filename = sys.argv[1]
numSets = int(sys.argv[2])
samplesPerSet = int(sys.argv[3])
if len(sys.argv) == 5:
	ordering = int(sys.argv[4])
else:
	ordering = 0
	
numExamples = sum(1 for line in open(filename)) - 1

if samplesPerSet > numExamples:
	sys.exit('Error: samplesPerSet ({}) larger than number of examples({})'.format(samplesPerSet, numExamples))

examples = list(range(numExamples))

for i in range(1, numSets+1):
	f = open(filename + '_samples_' + str(i), 'w')
	samples = random.sample(examples, samplesPerSet)
	if ordering > 0:
		samples.sort()
	elif ordering < 0:
		samples.sort(reverse=True)
	f.write(' '.join(str(s) for s in samples))
	f.close()
