#!/bin/bash
STARTTIME=$(date +%s)

EXP_DIR=$(pwd)
BUILD_DIR="/home/shannon/HMRI/BooleanNet/C++/build-BooleanNet-Desktop-Release/"

cd $BUILD_DIR
make
make_ret=$?
cd $EXP_DIR


if [ $make_ret -ne 0 ]; then
	echo 'make failed, aborting.'
else
	cp "$BUILD_DIR"BooleanNet $EXP_DIR

	# Parameters
	Ni=6
	Ne=64
	Ng=48
#	Ng=45
#	Ns=250000000
	Ns=1000000
	T=5
#	T=0

	# generate data file
	rm Data/*
	rm Results/*.log
	rm Results/*.err
	rm Results/*.png
	python generateData.py $Ni
	# generate random samples
	python generateSamples.py Data/data 1 $Ne

	echo -n "" > commands
	
	methods="simple weighted_lin_msb weighted_lin_lsb hierarchical_lin_msb hierarchical_lin_lsb weighted_exp_msb weighted_exp_lsb hierarchical_exp_msb hierarchical_exp_lsb"

	for method in $methods
	do	
		echo "./BooleanNet anneal givensamples Data/data Data/data_samples_1 $Ng $Ns $T 0.999 $method ErrorPlots/$method.err ErrorPlots/$method.log" >> commands
	done
	# run these commands 8 at a time
	parallel -j7 < commands >> Results/results
	# remove temp commands file
	rm commands

	CMDENDTIME=$(date +%s)
	echo "Commands finished : $[$CMDENDTIME - $STARTTIME]s"
	
	for method in $methods
	do	
		echo "call \"gplot\" \"'ErrorPlots/$method.err'\" \"'ErrorPlots/$method.png'\"" | gnuplot
	done
	
	PLTENDTIME=$(date +%s)

	echo "Plotting finished: $[$PLTENDTIME - $CMDENDTIME]s"
	echo "Total: $[$PLTENDTIME - $STARTTIME]s"

fi
