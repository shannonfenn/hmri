#!/bin/bash

wait

STARTTIME=$(date +%s)

EXP_DIR=$(pwd)
BUILD_DIR="/home/shannon/HMRI/BooleanNet/C++/build-BooleanNet-Desktop-Release/"

cd $BUILD_DIR
make
make_ret=$?
cd $EXP_DIR


if [ $make_ret -ne 0 ]; then
	echo 'make failed, aborting.'
else
	cp "$BUILD_DIR"BooleanNet $EXP_DIR

	mkdir -p Results

	N=10

	methods="simple weighted_lin_msb weighted_lin_lsb hierarchical_lin_msb hierarchical_lin_lsb worst_sample_lin_msb worst_sample_lin_lsb e4_msb e4_lsb"

        ./genDat.sh 8 512 $N
        ./subExp.sh 8 512 99 1000 6000 0.1 0.997 $N "$methods"
        mv Results/results Results/results_8_512_99_1000_6000_0.1_0.997_$N
        ./subExp.sh 8 512 66 1000 6000 0.1 0.997 $N "$methods"
        mv Results/results Results/results_8_512_66_1000_6000_0.1_0.997_$N

	./genDat.sh 8 256 $N
	./subExp.sh 8 256 99 1000 6000 0.1 0.997 $N "$methods"
	mv Results/results Results/results_8_256_99_1000_6000_0.1_0.997_$N
	./subExp.sh 8 256 66 1000 6000 0.1 0.997 $N "$methods"
	mv Results/results Results/results_8_256_66_1000_6000_0.1_0.997_$N
	
	./genDat.sh 8 64 $N
	./subExp.sh 8 64 99 1000 6000 0.1 0.997 $N "$methods"
	mv Results/results Results/results_8_64_99_1000_6000_0.1_0.997_$N
	./subExp.sh 8 64 66 1000 6000 0.1 0.997 $N "$methods"
	mv Results/results Results/results_8_64_66_1000_6000_0.1_0.997_$N
	
	./genDat.sh 6 128 $N
	./subExp.sh 6 128 72 1000 6000 0.075 0.997 $N "$methods"
	mv Results/results Results/results_6_128_72_1000_6000_0.075_0.997_$N
	./subExp.sh 6 128 48 1000 6000 0.075 0.997 $N "$methods"
	mv Results/results Results/results_6_128_48_1000_6000_0.075_0.997_$N
	
	./genDat.sh 6 32 $N
	./subExp.sh 6 32 72 1000 6000 0.075 0.997 $N "$methods"
	mv Results/results Results/results_6_32_72_1000_6000_0.075_0.997_$N
	./subExp.sh 6 32 48 1000 6000 0.075 0.997 $N "$methods"
	mv Results/results Results/results_6_32_48_1000_6000_0.075_0.997_$N
	
	./genDat.sh 4 256 $N
	./subExp.sh 4 256 45 1000 7500 0.05 0.997 $N "$methods"
	mv Results/results Results/results_4_256_45_1000_7500_0.05_0.997_$N
	./subExp.sh 4 256 30 1000 7500 0.05 0.997 $N "$methods"
	mv Results/results Results/results_4_256_30_1000_7500_0.05_0.997_$N

	./genDat.sh 4 64 $N
	./subExp.sh 4 64 45 1000 7500 0.05 0.997 $N "$methods"
	mv Results/results Results/results_4_64_45_1000_7500_0.05_0.997_$N
	./subExp.sh 4 64 30 1000 7500 0.05 0.997 $N "$methods"
	mv Results/results Results/results_4_64_30_1000_7500_0.05_0.997_$N
	
	./genDat.sh 4 16 $N
	./subExp.sh 4 16 45 1000 7500 0.05 0.997 $N "$methods"
	mv Results/results Results/results_4_16_45_1000_7500_0.05_0.997_$N
	./subExp.sh 4 16 30 1000 7500 0.05 0.997 $N "$methods"
	mv Results/results Results/results_4_16_30_1000_7500_0.05_0.997_$N
	
	rm commands

	CMDENDTIME=$(date +%s)
	echo "Finished : $[$CMDENDTIME - $STARTTIME] s"
fi
