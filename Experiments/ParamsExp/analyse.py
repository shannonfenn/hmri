from collections import defaultdict
import numpy
import sys


if len(sys.argv) == 2:
	filename = sys.argv[1]
else:
	filename = 'Results/results'

f = open(filename)

categories = f.readline().split()[1:]			# column headers
results = [defaultdict(list) for c in categories]	# lookups for metrics

#metrics = ['simple', 'weighted_lin_msb', 'weighted_lin_lsb', 'weighted_exp_msb', 'weighted_exp_lsb', 'hierarchical_lin_msb', 'hierarchical_lin_lsb', 'hierarchical_exp_msb', 'hierarchical_exp_lsb', 'e4_msb', 'e4_lsb']
#m_short = ['s', 'w_msb', 'w_lsb', 'w_e_msb', 'w_e_lsb',  'h_msb', 'h_lsb', 'h_e_msb', 'h_e_lsb', 'e4_msb', 'e4_lsb']
metrics = ['simple', 'weighted_lin_msb', 'weighted_lin_lsb', 'hierarchical_lin_msb', 'hierarchical_lin_lsb', 'e4_msb', 'e4_lsb', 'worst_sample_lin_msb', 'worst_sample_lin_lsb']
m_short = ['s', 'w_msb', 'w_lsb', 'h_msb', 'h_lsb', 'e4_msb', 'e4_lsb', 'wrst_msb', 'wrst_lsb']
#metrics = ['simple', 'weighted_lin_msb', 'weighted_lin_lsb', 'hierarchical_lin_msb', 'hierarchical_lin_lsb', 'e4_msb', 'e4_lsb']
#m_short = ['s', 'w_msb', 'w_lsb', 'h_msb', 'h_lsb', 'e4_msb', 'e4_lsb']
#metrics = ['simple', 'weighted_lin_msb', 'weighted_lin_lsb', 'hierarchical_lin_msb', 'hierarchical_lin_lsb']
#m_short = ['s', 'w_msb', 'w_lsb', 'h_msb', 'h_lsb']

maxwidth = 0

for l in f.readlines():
	line = l.split()
	metric = line[0]

	maxwidth = max(maxwidth, len(metric))

	line = line[1:]

	for i in range(len(line)):
		results[i][metric].append(float(line[i]))

finerr = categories.index('error(simple)')
trgerr = categories.index('trgerror(simple)')
iterfin = categories.index('#iterFinish')
iterbest = categories.index('#iterBest')
time = categories.index('time(s)')

plotting = True

try:
	from savefig import monkey_patch
	monkey_patch()
	import matplotlib.pyplot as plt
	from matplotlib import rcParams
	rcParams.update({'figure.autolayout': True})
except Exception:
	plotting = False

if plotting:
	plt.subplot(321)
	D = results[finerr]
	data = [D[m] for m in metrics]
	plt.boxplot(data)
	plt.ylim((0,plt.ylim()[1]))
	plt.xticks(list(range(1,len(m_short)+1)), m_short, rotation=90)
	plt.title('Final Error')
	
	plt.subplot(322)
	D = results[trgerr]
	data = [D[m] for m in metrics]
	plt.boxplot(data)
	plt.ylim((0,plt.ylim()[1]))
	plt.xticks(list(range(1,len(m_short)+1)), m_short, rotation=90)
	plt.title('Training Error')

	plt.subplot(323)
	D = results[iterfin]
	data = [D[m] for m in metrics]
	plt.boxplot(data)
	plt.xticks(list(range(1,len(m_short)+1)), m_short, rotation=90)
	plt.title('Total Steps')
	
	plt.subplot(324)
	D = results[iterbest]
	data = [D[m] for m in metrics]
	plt.boxplot(data)
	plt.xticks(list(range(1,len(m_short)+1)), m_short, rotation=90)
	plt.title('Iteration for Best')

	plt.subplot(325)
	D = results[finerr]
	data = [D[m].count(0.0) + 1e-15 for m in metrics]
	plt.bar(list(range(len(data))), data)
	x1,x2,y1,y2 = plt.axis()
	plt.axis((x1,x2,0,max(1, y2)))
	plt.xticks(numpy.arange(len(m_short)) + 0.5, m_short, rotation=90)
	plt.title('# Fully Generalised')

	plt.subplot(326)
	D = results[trgerr]
	data = [D[m].count(0.0) + 1e-15 for m in metrics]
	plt.bar(list(range(len(data))), data)
	x1,x2,y1,y2 = plt.axis()
	plt.axis((x1,x2,0,max(1, y2)))
	plt.xticks(numpy.arange(len(m_short)) + 0.5, m_short, rotation=90)
	plt.title('# Fully Memorised')

	#plt.show()
	#plt.savefig(filename + '.eps', bbox_inches='tight')
	plt.savefig(filename + '.png', bbox_inches='tight')

print('\nFINAL ERROR')
for metric in metrics:
	print('{:{width}} : {:.3} +/- {:.3}'.format(metric, numpy.mean(results[finerr][metric]), numpy.std(results[finerr][metric]), width=maxwidth)) 
print('\n#COMPLETELY GENERALISED')
for metric in metrics:
	print('{:{width}} : {} / {}'.format(metric, results[finerr][metric].count(0.0), len(results[finerr][metric]), width=maxwidth)) 
print('\n#COMPLETELY MEMORISED')
for metric in metrics:
	print('{:{width}} : {} / {}'.format(metric, results[trgerr][metric].count(0.0), len(results[trgerr][metric]), width=maxwidth)) 
print('\n#GENERALISED / #MEMORISED')
for metric in metrics:
	if results[trgerr][metric].count(0.0) != 0:
		print('{:{width}} : {:.3}'.format(metric, results[finerr][metric].count(0.0) / float(results[trgerr][metric].count(0.0)), width=maxwidth)) 
	else:
		print('{:{width}} : {}'.format(metric, 0, width=maxwidth))
print('\n#GENERALISED / #MEMORISED / FIN_ERROR')
for metric in metrics:
	if results[trgerr][metric].count(0.0) != 0:
		print('{:{width}} : {:.3}'.format(metric, results[finerr][metric].count(0.0) / float(results[trgerr][metric].count(0.0)) / numpy.mean(results[finerr][metric]), width=maxwidth)) 
	else:
		print('{:{width}} : {}'.format(metric, 0, width=maxwidth))
print('\nNUM ITERATIONS TO TERMINATE')
for metric in metrics:
	print('{:{width}} : {:.3} +/- {:.3}'.format(metric, numpy.mean(results[iterfin][metric]), numpy.std(results[iterfin][metric]), width=maxwidth)) 
print('\nTime (s)')
for metric in metrics:
	print('{:{width}} : {:.3} +/- {:.3}'.format(metric, numpy.mean(results[time][metric]), numpy.std(results[time][metric]), width=maxwidth))

if '#iterBest' in categories:
	iterbest = categories.index('#iterBest')
	print('\nNUM ITERATIONS TO FIND BEST')
	for metric in metrics:
		print('{:{width}} : {:.3} +/- {:.3}'.format(metric, numpy.mean(results[iterbest][metric]), numpy.std(results[iterbest][metric]), width=maxwidth)) 

