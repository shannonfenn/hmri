import subprocess
import datetime
import os
import os.path
import genData
import collections
import glob
import shutil

ParameterSet = collections.namedtuple('ParameterSet',
                          ['numBits',
                           'numExamples',
                           'numGatesInitial',
                           'numGates',
                           'numTemps',
                           'numStepsPerTemp',
                           'initialTemp',
                           'tempScaler',
                           'numExperiments'])

def setupAndGetResultDir(buildDir, now):
    # build binary
    subprocess.check_call('make', cwd=buildDir)
    # move it to this directory
    subprocess.check_call(['cp', buildDir  + 'BooleanNet', '.'])
    # make directory for the results
    resultDir = 'Results_' + now.strftime('%y-%m-%d_%H-%M')
    
    # find the first number i such that 'Results_datetime_i' is not
    # already a directory and make that new directory
    i = 0
    while os.path.isdir(resultDir + '_' + str(i)):
        i += 1
    resultDir += '_' + str(i)
    os.mkdir(resultDir)
    
    #copy this script into results directory
    shutil.copy(__file__, os.path.join(resultDir, 'scriptusedforexperiment') )
    
    # make or clear data folder
    if os.path.isdir('Data'):
        for f in glob.glob('Data/*'):
            os.remove(f)
    else:
        os.mkdir('Data')
    
    # return the directory name for the results to go in
    return resultDir

def createCommandsFile(filename, optimiser, methods, parameters):
    with open(filename, 'w') as f:
        for i in range(parameters.numExperiments):
            for method in methods:
                f.write('./BooleanNet givensamples Data/data Data/samples_' + str(i) + ' ' + optimiser + ' ')
                if optimiser == 'ganneal':
                    f.write(str(parameters.numGatesInitial) + ' ')
                f.write(' '.join(str(p) for p in parameters[3:8]))
                f.write(' ' + method + '; echo, \n')
                if i < parameters.numExperiments - 1:
                    f.write('; echo, ')
                f.write('\n')
                    

def run(buildDir, optimisers, methods, params):
    now = datetime.datetime.now()

    resultDir = setupAndGetResultDir(buildDir, now)

    genData.generateAndWriteDataAndSamples('Data/', params.numBits, params.numExperiments, params.numExamples)

    for opt in optimisers:
        # Create result and error files and prep them
        with open(os.path.join(resultDir, 'results'), 'w') as f:
            f.write('[')    # prep for JSON
        with open(os.path.join(resultDir, 'errors'), 'w') as f:
            f.write('')
        # create commands file
        createCommandsFile('commands', opt, methods, parameters)
        # run these commands 8 at a time using GNUparallel
        #subprocess.check_call(['parallel', '-j8', '<', 'commands', '1>>', os.path.join(resultDir, 'results'), '2>>', os.path.join(resultDir, 'errors')])
        print(' '.join(['parallel', '-j8', '<', 'commands', '1>>', os.path.join(resultDir, 'results'), '2>>', os.path.join(resultDir, 'errors')]))
        #parallel -j8 < commands 1>> Results/results 2>> Results/errors
        
        # finish JSON list in results file
        with open(os.path.join(resultDir, 'results'), 'a') as f:
            f.write(']')
        # rename results and errors files
        os.rename(os.path.join(resultDir, 'results'), os.path.join(resultDir, 'results_' + opt))
        os.rename(os.path.join(resultDir, 'errors'), os.path.join(resultDir, 'errors_' + opt))
        # remove temp commands file
        os.remove('commands')

if __name__ == '__main__':
    buildDir = '../../Code/BooleanNet/C++/build-BooleanNet-Desktop-Release/'

    optimisers = ['anneal', 'ganneal']
    methods = ['simple',
           'weighted_lin_lsb',
           'hierarchical_lin_lsb',
           'e4_lsb',
           'e5_lsb',
           'e6_lsb',
           'e7_lsb']
    parameters = ParameterSet(8,    # numBits
                          128,  # numExamples
                          8,    # numGatesInitial
                          300,  # numGates
                          10000,# numTemps
                          150,  # numStepsPerTemp
                          0.05, # initialTemp
                          0.997,# tempScaler
                          100)  # numExperiments
    run(buildDir, optimisers, methods, parameters)