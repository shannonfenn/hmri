

gates = [[2, 6], [5, 5], [7, 9], [4, 0], [3, 10], [11, 4], [6, 13], [0, 0], [13, 12], [2, 6], [11, 0], [17, 7], [18, 12], [8, 15], [8, 20], [10, 19], [4, 9], [5, 12], [25, 9], [26, 4], [25, 13], [28, 5], [12, 11], [9, 19], [9, 31], [6, 11], [15, 8], [31, 34], [7, 13], [28, 15], [14, 9], [24, 32], [22, 36], [27, 26], [13, 31], [1, 1], [2, 33], [14, 39], [12, 30], [18, 20], [11, 19], [7, 29], [20, 32], [0, 45], [36, 36], [36, 17], [26, 49], [45, 44], [22, 8], [47, 19], [57, 3], [38, 31], [48, 47], [49, 46], [3, 10], [40, 60], [57, 5], [47, 50], [13, 16], [50, 40], [18, 37], [22, 45], [59, 44], [63, 25]]
examples = {"01101110": "0100", "00010010": "0011", "11101110": "1100", "01011110": "0011", "10110001": "1100", "10011001": "0010", "00001111": "1111", "00011110": "1111", "11001000": "0100", "00001011": "1011", "11001101": "1001", "01101001": "1111", "11101001": "0111", "01111110": "0101", "00000000": "0000", "01011111": "0100"}

examples = [ ( [int(c) for c in reversed(i)], [int(c) for c in reversed(o)] ) for i, o in examples.items()]

trgerr = 0
for e in examples:
    state = e[0]
    for g in gates:
        state.append( int(not(state[g[0]] and state[g[1]])) )
    
    trgerr += sum([expected != actual for expected, actual in zip(e[1], state[-4:]) ])

print(float(trgerr) / len(examples))

finerr = 0
for i1 in range(2**4):
    for i2 in range(2**4):
        state = [int(c) for c in '{:0{width}b}{:0{width}b}'.format(i1, i2, width=4)]
        out = [int(c) for c in '{:0{width}b}'.format((i1+i2)%(2**4), width=4) ]

        for g in gates:
            state.append( int(not(state[g[0]] and state[g[1]])) )
        finerr += sum([expected != actual for expected, actual in zip(out, state[-4:]) ])

print(finerr)

print(float(finerr) / (2**8))