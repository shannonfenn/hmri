#!/bin/bash
STARTTIME=$(date +%s)

EXP_DIR=$(pwd)
BUILD_DIR="/home/shannon/HMRI/BooleanNet/C++/build-BooleanNet-Desktop-Release/"

cd $BUILD_DIR
make
make_ret=$?
cd $EXP_DIR


if [ $make_ret -ne 0 ]; then
	echo 'make failed, aborting.'
else
	cp "$BUILD_DIR"BooleanNet $EXP_DIR

	# Parameters
	Ni=4
#	Ne=16
	Ne=128
#	Ng=64
	NgStart=8
	Ng=128
	Nt=1000
#	Ns=15000
	Ns=1500
	T=0.05
#	T=0
	Ts=0.997
	# generate N copies of commands	
	N=1000

	mkdir -p Results
	
	for opt in anneal ganneal
	do

		echo "[" > Results/results # Clear the results file and start JSON list
		echo "" > Results/errors

		# generate data file and random samples
		rm Data/*
		python genData.py Data/ $Ni $N $Ne

	#	methods="simple weighted_lin_msb weighted_lin_lsb hierarchical_lin_msb hierarchical_lin_lsb weighted_exp_msb weighted_exp_lsb hierarchical_exp_msb hierarchical_exp_lsb"
	#	methods="simple weighted_lin_msb weighted_lin_lsb hierarchical_lin_msb hierarchical_lin_lsb e4_msb e4_lsb e5_msb e5_lsb e6_msb e6_lsb e7_msb e7_lsb"
		methods="simple weighted_lin_lsb hierarchical_lin_lsb e4_lsb e5_lsb e6_lsb e7_lsb"

		# Clear commands file
		echo -n "" > commands
		for i in $(seq 1 $N);
		do 
			for method in $methods
			do
				if [ "$opt" == "ganneal" ]; then
					#echo "./BooleanNet anneal givensamples Data/data Data/data_samples_$i $Ng $Ns $T 0.994 $method Results/$method\_$i\_$k.err ''" >> commands
					echo "./BooleanNet givensamples Data/data Data/samples_$i $opt $NgStart $Ng $Nt $Ns $T $Ts $method; echo ," >> commands
				else
					echo "./BooleanNet givensamples Data/data Data/samples_$i $opt $Ng $Nt $Ns $T $Ts $method; echo ," >> commands
				fi
			done
		done

		# run these commands 8 at a time
		parallel -j8 < commands 1>> Results/results 2>> Results/errors

		#remove trailing comma
		sed -i '$ d' Results/results
		echo "]" >> Results/results		# finish JSON list

		mv Results/results Results/results_$opt
		mv Results/errors Results/errors_$opt
		# remove temp commands file
		rm commands
	done

	cp $0 Results/$0

	python analyse.py Results/results_anneal > Results/results_anneal.txt
	python analyse.py Results/results_ganneal > Results/results_ganneal.txt

	mailx -s "Experiment done" < /dev/null "shannon.fenn@gmail.com"

	CMDENDTIME=$(date +%s)
	echo "Commands finished : $[$CMDENDTIME - $STARTTIME]s"
fi
