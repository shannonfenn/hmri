import sys
import random

def generateAdditionData(Ni):
	# Upper limit
	upper = 2**Ni
	# generate tuples of (input1, input2, output)
	dataset = [(i1, i2, (i1+i2)%upper ) for i1 in range(upper) for i2 in range(upper)]
	return dataset

def writeDataToFile(prefix, Ni, dataset):
	with open(prefix + 'data', 'w') as f:
		f.write( '{} {}\n'.format(2*Ni, Ni) )
		for tup in dataset:
			sin = '{:0{width}b}{:0{width}b}'.format(tup[0], tup[1], width=Ni)[::-1]
			sout = '{:0{width}b}'.format(tup[2], width=Ni)[::-1]
			f.write(' '.join(sin + ' ' + sout) + '\n')
			#f.write(' '.join('{:0{width}b}{:0{width}b} {:0{width}b}'.format(tup[0], tup[1], tup[2], width=Ni)) + '\n')
			
def generateAndWriteSamplesToFile(prefix, dataset, numExamples, numSets, samplesPerSet):
	# generate index samples	
	samples = [random.sample(range(numExamples), samplesPerSet) for i in range(numSets)]
	# and write them to files
	for i in range(len(samples)):
		# shuffle randomly
		random.shuffle(samples[i])
		with open(prefix + 'samples_' + str(i+1), 'w') as f:
			f.write(' '.join(str(s) for s in samples[i]))

def generateAndWriteDataAndSamples(prefix, Ni, numSets, samplesPerSet):
	numExamples = 2**(2*Ni)
	if samplesPerSet > numExamples:
		sys.exit('Error: samplesPerSet ({}) larger than number of examples({})'.format(samplesPerSet, numExamples))
	# generate full dataset and write to file
	dataset = generateAdditionData(Ni)
	writeDataToFile(prefix, Ni, dataset)
	generateAndWriteSamplesToFile(prefix, dataset, numExamples, numSets, samplesPerSet)
	
if __name__ == '__main__':
	# process arguments
	if not ( 5 <= len(argv) <= 6):
		sys.exit("Usage " + argv[0] + " prefix #inputbits #sets #samplesPerSet")
	
	generateAndWriteDataAndSamples(argv[1], int(argv[2]), int(argv[3]), int(argv[4]))
	