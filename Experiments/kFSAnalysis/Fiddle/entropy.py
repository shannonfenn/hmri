

from math import log

def toBinary(value, precision):
    return [int(c) for c in "{:0{w}b}".format(value, w=precision)]

def calcEntropy(examples, featureset_indices):
    entropy = 0
    k = len(featureset_indices)
    n = len(examples)
    num_vertices = 2**k
    for vertex in range(num_vertices):
        vertex_coordinates = toBinary(vertex, k)
        num_at_vertex = 0
        for example in examples:
            # if the example is located at that vertex in the hypercube
            if all(example[f] == v for f, v in zip(featureset_indices, vertex_coordinates)):
                num_at_vertex += 1
        # calculate entropy contribution of this vertex
        # p * lg(1/p)   where   p = #atVertex / #samples        
        if num_at_vertex > 0:
            entropy += (num_at_vertex / n) * log( n / num_at_vertex )
    return entropy




examples = ['0   0   1   1   0   0   0   0   1   0   1   1   1   0   0   1   1   1   1   0   1   0   1   1   1   0   0   1   1   1   1   1',
'1   0   1   0   0   0   1   1   0   0   0   0   1   1   1   0   1   1   1   0   0   1   0   1   1   1   0   0   0   1   1   0',
'1   0   1   1   0   1   0   0   0   1   0   0   0   0   1   1   0   0   0   1   1   1   0   0   0   0   0   0   0   1   1   0',
'1   1   1   0   0   0   1   0   1   1   1   1   1   1   0   0   1   0   0   0   1   1   1   0   0   1   1   1   0   0   0   1',
'0   1   1   0   0   1   0   1   1   1   0   1   1   1   0   0   1   1   0   1   1   1   0   1   0   1   1   1   0   1   1   1',
'1   0   0   0   0   0   0   0   1   1   0   1   1   1   0   1   1   1   1   1   0   0   1   1   1   1   0   0   0   1   1   1',
'1   0   0   1   1   0   1   0   1   0   1   1   0   1   0   0   0   0   0   0   1   0   0   1   1   0   0   1   0   0   0   0',
'0   1   0   0   1   0   1   1   0   0   1   1   1   1   1   0   0   0   0   0   1   1   0   1   0   0   0   0   1   0   1   0']
examples = [[int(c) for c in e.split()] for e in examples]
examples = sorted(zip(*examples))

fs1 = [0, 1, 2, 4, 5, 6]
print('fs1 {}'.format(fs1))
examples_fs1 = [ [e[f] for f in fs1] for e in examples]
for e in sorted(examples_fs1):
    print(e)
print('entropy: {}'.format(calcEntropy(examples, fs1)))


fs2 = [1, 2, 3, 5, 6, 7]
print('fs2 {}'.format(fs2))
examples_fs2 = [ [e[f] for f in fs2] for e in examples]
for e in sorted(examples_fs2):
    print(e)
print('entropy: {}'.format(calcEntropy(examples, fs2)))