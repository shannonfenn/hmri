import yaml
import itertools
import entropy
from collections import OrderedDict

def atLeastNEqual(pairs, N):
    if N==0:
        return True
    count = 0
    for p0, p1 in pairs:
        if p0 == p1:
            count += 1
            if count >= N:
                return True
    return False

def atLeastNUnequal(pairs, N):
    if N==0:
        return True
    count = 0
    for p0, p1 in pairs:
        if p0 != p1:
            count += 1
            if count >= N:
                return True
    return False

def isAlphaBetaFeatureSet(featureset, target, alpha, beta):
    alphapairs = []
    betapairs = []
    for ind in itertools.combinations(list(range(len(target))), 2):
        if target[ind[0]] != target[ind[1]]:
            alphapairs.append(ind)
        else:
            betapairs.append(ind)

    for e1, e2 in alphapairs:
        if not atLeastNUnequal( ((feature[e1], feature[e2]) for feature in featureset), alpha):
            return False
    for e1, e2 in betapairs:
        if not atLeastNEqual( ((feature[e1], feature[e2]) for feature in featureset), beta):
            return False
    return True


def isAlphaFeatureSet(featureset, target, alpha):
    to_check = [ ind for ind in itertools.combinations(list(range(len(target))), 2) if target[ind[0]] != target[ind[1]] ]

    for e1, e2 in to_check:
        if sum( feature[e1] == feature[e2] for feature in featureset ) < alpha:
            return False
    return True


def isFeatureSet(featureset, target):
    to_check = [ ind for ind in itertools.combinations(list(range(len(target))), 2) if target[ind[0]] != target[ind[1]] ]

    # We need a discriminating feature for each pair of non-identical examples
    for e1, e2 in to_check:
        discriminating_feature_found = False
        # Check each feature, if all features are non-discriminating for this
        # example then we do not have a feature set
        if all( feature[e1] == feature[e2] for feature in featureset ):
            return False
    # A descriminating feature must have been found for each example pair
    return True

with open('data.yaml') as f:
    data = yaml.load(f)

# pull out features
targets = data['targets']
nontargets = data['nontarget']
# place them into an ordered dictionary
targets = OrderedDict(sorted(list(targets.items()), key=lambda t: t[0]))
nontargets = OrderedDict(sorted(list(nontargets.items()), key=lambda t: t[0]))
# get the transposed nontarget matrix (by example first then feature)
examples = list(zip(*list(nontargets.values())))

for targetname, target in targets.items():
    print('\nTarget: ' + targetname)
    # Is the whole thing even a featureset?
    if not isFeatureSet(list(nontargets.values()), target):
        print('Original set not a feature set for target {}!'.format(targetname))
    else:
        found = False
        # Test each pair of features
        for k in range(len(nontargets)):
            for featureSetNames in itertools.combinations(nontargets, k):
                if isFeatureSet([nontargets[f] for f in featureSetNames], target):
                    found = True
                    FS_indices = [list(nontargets.keys()).index(name) for name in featureSetNames]
                    e = entropy.calcEntropy(examples, FS_indices)
                    print('{}FS found: {} entropy: {}'.format(k, featureSetNames, e))
            # Don't look for higher order feature sets if one has been found
            if found:
                break
