from collections import defaultdict

import numpy
import sys
import json


def main(filename):
	
	with open(filename) as f:
		raw_data = json.load(f)	
	#categories = f.readline().split()[1:]			# column headers
	#results = [defaultdict(list) for c in categories]	# lookups for metrics
	#metrics = ['simple', 'r_e4_msb', 'oi_e4_msb', 'od_e4_msb', 'r_e4_lsb', 'oi_e4_lsb', 'od_e4_lsb']
		
		results = {}
		
		for rd in raw_data:
			dat = rd["results"]
			metric = dat["metric"]
			dat.pop("metric")
			if metric not in results:
				results[metric] = defaultdict(list)
			for k, v in list(dat.items()):
				results[metric][k].append(v)
					
		metrics = sorted(results.keys())
			
		finerr 		= 'Full Error (simple)'
		trgerr 		= 'Training Error (simple)'
		trgacc		= 'Training Accuracy'
		finacc		= 'Full Accuracy'
		iterbest 	= 'Iteration for best'
		iterfin		= 'Total iterations'
		perbittrg 	= 'Training Error per bit'
		perbitfin 	= 'Full Error per bit'
		depth		= 'Max Depth per bit'
		time 		= 'time'
		
		for k, v in results.items():
			v[perbittrg] = list(zip(*v[perbittrg]))
			v[perbitfin] = list(zip(*v[perbitfin]))
			v[depth] = list(zip(*v[depth]))
		
		plotting = True
		try:
			from savefig import monkey_patch
			monkey_patch()
			import prettyplotlib as ppl
			import matplotlib.pyplot as plt
			from matplotlib import rcParams
			rcParams.update({'figure.autolayout': True})
			#import prettyplotlib as plt
		except Exception:
			print("Error importing plotting libs.")
			plotting = False
		
		if plotting:
			fig, axs = plt.subplots(5, 3, sharex=True)	
			fig.autofmt_xdate()
			fig.set_figwidth( 2 * fig.get_figwidth() )
			fig.set_figheight( 2 * fig.get_figheight() )
			
			data = [results[m][trgerr] for m in metrics]
			positions = numpy.arange(len(data)) + 0.5
			ppl.boxplot(axs[0,0], data, positions=positions, xticklabels=metrics)
			axs[0,0].set_ylim(bottom=0)
			axs[0,0].set_title('Training Error')
			
			data = [results[m][finerr] for m in metrics]
			ppl.boxplot(axs[0,2], data, positions=positions)
			axs[0,2].set_ylim(bottom=0)
			axs[0,2].set_title('Full Error')
		
			data = [results[m][trgacc] for m in metrics]
			ppl.boxplot(axs[1,0], data, positions=positions)
			axs[1,0].set_ylim(bottom=0, top=1.0)
			axs[1,0].set_title('Training Accuracy')
			
			data = [results[m][finacc] for m in metrics]
			ppl.boxplot(axs[1,2], data, positions=positions)
			axs[1,2].set_ylim(bottom=0, top=1.0)
			axs[1,2].set_title('Full Accuracy')
					
			data = [results[m][trgerr].count(0.0) + 1e-15 for m in metrics]
			ppl.bar(axs[2,0], numpy.arange(len(data)) + 0.25, data, grid='y')
			axs[2,0].set_ylim(top=max(1, max(data)))
			axs[2,0].set_title('# Fully Memorised')
			
			data = [results[m][finerr].count(0.0) + 1e-15 for m in metrics]
			ppl.bar(axs[2,2], numpy.arange(len(data)) + 0.25, data, grid='y')
			axs[2,2].set_ylim(top=max(1, max(data)))
			axs[2,2].set_title('# Fully Generalised')
						
			data = list(zip(*[results[m][perbittrg] for m in metrics]))
			width = 0.5/len(data)
			positions = [numpy.arange(len(data[0])) + i*width + 0.5 for i in range(len(data))]
			for d, pos in zip(data, positions):
				ppl.boxplot(axs[3,0], d, sym='', positions=pos, widths=width)
			axs[3,0].set_xlim(left=0, right=len(data[0]))
			axs[3,0].set_title('Mean error per bit (Trg)')
			
			data = list(zip(*[results[m][perbitfin] for m in metrics]))
			for d, pos in zip(data, positions):
				ppl.boxplot(axs[3,2], d, sym='', positions=pos, widths=width)
			axs[3,2].set_xlim(left=0, right=len(data[0]))
			axs[3,2].set_title('Mean error per bit (Full)')
			
			# make per bit error y-axis similar
			ymax = max( [axs[3,x].get_ylim()[1] for x in range(3) ])
			axs[3,0].set_ylim(top=ymax)
			axs[3,1].set_ylim(top=ymax)
			axs[3,2].set_ylim(top=ymax)
	
			data = [results[m][iterbest] for m in metrics]
			positions = numpy.arange(len(data)) + 0.5
			ppl.boxplot(axs[4,0], data, positions=positions)
			axs[4,0].set_ylim(bottom=0)
			axs[4,0].set_title('Iteration for Best')
					
			data = list(zip(*[results[m][depth] for m in metrics]))
			width = 0.5/len(data)
			positions = [numpy.arange(len(data[0])) + i*width + 0.5 for i in range(len(data))]
			for d, pos in zip(data, positions):
				ppl.boxplot(axs[4,1], d, sym='', positions=pos, widths=width)
			axs[4,1].set_xlim(left=0, right=len(data[0]))
			axs[4,1].set_title(depth)
	
			plt.savefig(filename + '.png', bbox_inches='tight', orientation='portrait')
		
		maxwidth = max([len(m) for m in metrics])
		
		print('\nFINAL ERROR')
		for metric in metrics:
			print('{:{width}} : {:.3f} +/- {:.3f}'.format(metric, numpy.mean(results[metric][finerr]), numpy.std(results[metric][finerr]), width=maxwidth)) 
	
		print('\nFINAL ERROR PER BIT')
		data = list(zip(*[results[m][perbitfin] for m in metrics]))
		for metric in metrics:
			print('{:{width}} :'.format(metric, width=maxwidth), end=' ') 
			for bitdata in results[metric][perbitfin]:
				 print('{:.2f} +/-{:.2f}  '.format(numpy.mean(bitdata), numpy.std(bitdata)), end=' ')
			print('')
	
		print('\n#COMPLETELY GENERALISED')
		for metric in metrics:
			print('{:{width}} : {} / {}'.format(metric, results[metric][finerr].count(0.0), len(results[metric][finerr]), width=maxwidth)) 
		print('\n#COMPLETELY MEMORISED')
		for metric in metrics:
			print('{:{width}} : {} / {}'.format(metric, results[metric][trgerr].count(0.0), len(results[metric][trgerr]), width=maxwidth)) 
		print('\n#GENERALISED / #MEMORISED')
		for metric in metrics:
			if results[metric][trgerr].count(0.0) != 0:
				print('{:{width}} : {:.3f}'.format(metric, results[metric][finerr].count(0.0) / float(results[metric][trgerr].count(0.0)), width=maxwidth)) 
			else:
				print('{:{width}} : {}'.format(metric, 0, width=maxwidth))
		print('\n#GENERALISED / #MEMORISED / FIN_ERROR')
		for metric in metrics:
			if results[metric][trgerr].count(0.0) != 0:
				print('{:{width}} : {:.3f}'.format(metric, results[metric][finerr].count(0.0) / float(results[metric][trgerr].count(0.0)) / numpy.mean(results[metric][finerr]), width=maxwidth)) 
			else:
				print('{:{width}} : {}'.format(metric, 0, width=maxwidth))
		print('\nNUM ITERATIONS TO TERMINATE')
		for metric in metrics:
			print('{:{width}} : {:.3f} +/- {:.3f}'.format(metric, numpy.mean(results[metric][iterfin]), numpy.std(results[metric][iterfin]), width=maxwidth)) 
		print('\nTime (s)')
		for metric in metrics:
			print('{:{width}} : {:.3f} +/- {:.3f}'.format(metric, numpy.mean(results[metric][time]), numpy.std(results[metric][time]), width=maxwidth))
		
		print('\nNUM ITERATIONS TO FIND BEST')
		for metric in metrics:
			print('{:{width}} : {:.3f} +/- {:.3f}'.format(metric, numpy.mean(results[metric][iterbest]), numpy.std(results[metric][iterbest]), width=maxwidth)) 

if __name__ == '__main__':
	if len(argv) == 2:
		filename = argv[1]
	else:
		filename = 'Results/results'
	main(filename)
