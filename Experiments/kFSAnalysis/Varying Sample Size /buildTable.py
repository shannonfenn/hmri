import pandas as pd
from itertools import product

n_list = [4, 8, 12, 16, 32, 48, 64, 128, 192, 256]
featurenames = [''.join(x) for x in product(['a', 'b'], ['0', '1', '2', '3'])]
M = 10

occurrences = [pd.DataFrame(data=0, index=n_list, columns=featurenames) for i in range(4)]

# for each number of samples
for n in n_list:
    # for each random set of samples of size n
    for m in range(1, M+1):
        # for each target feature
        for c in range(4):
            try:
                f = open('results/{}_{}_{}.abk.out'.format(n, m, c))
                # skip first two lines
                next(f)
                next(f)
                # for each feature in file
                for feature in f:
                    occurrences[c][feature.strip()][n] += 1
            except:
                print('No FS for bit {} with n={} for trial {}'.format(c, n, m))

for c in range(len(occurrences)):
    print('\nc{}'.format(c))
    print(occurrences[c] / 10)