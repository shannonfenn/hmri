from itertools import product
import sys

def writeABKFile(filehandle, samplesnames, featurenames, nontargets, target):
    # build file preamble
    header = '\n'.join(['FEATURESINROWS', 'TARGETPRESENT', 'LAST', 
                        str(len(featurenames)), str(len(samplesnames))])
    # prepend 'dummy' to sample names
    samplesnames = ' '.join(['dummy'] + samplesnames)
    # prepend feature names to each nontarget vector
    nontargets = [ [a] + b for a, b in zip(featurenames, nontargets) ]
    nontargets = [' '.join(t) for t in nontargets]
    # write out to file
    filehandle.write('\n'.join([header, samplesnames] + nontargets + [' '.join(target)]))
    filehandle.write('\n')


def convertSampleFileToABK(samplefilename, abkfileprefix):
    with open(samplefilename) as infile:
        lines = infile.readlines()

        # generate matrices of inputs and outputs
        # samples in rows, features in columns
        inps = []
        outps = []
        for line in lines:
            inp, outp = line.split(',')
            inps.append(inp.split())
            outps.append(outp.split())

        # transpose input matrix
        inps = [ list(z) for z in zip(*inps) ]

        # Setting up for file
        samplesnames = inps[0]
        # non target features
        nontargets = inps[1:]
        # generate names for features: a0, .., an, b0, .., bn
        featurenames = [''.join(x) for x in product(['a', 'b'], [str(i) for i in range(len(nontargets)/2)])]

        # transpose output matrix
        targets = [ list(z) for z in zip(*outps) ]

        # write an ABK file for each output feature
        for i in range(len(targets)):
            with open(abkfileprefix + str(i) + '.abk', 'w') as f:
                writeABKFile(f, samplesnames, featurenames, nontargets, targets[i])


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('usage: ' + sys.argv[0] + ' inputfile outputprefix')
    else:
        convertSampleFileToABK(sys.argv[1], sys.argv[2])
