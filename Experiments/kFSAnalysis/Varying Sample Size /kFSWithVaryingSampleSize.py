from genData import generateAndWriteDataAndSamples
from samplesToABK import convertSampleFileToABK

n_list = [4, 8, 12, 16, 32, 48, 64, 128, 192, 256]
m = 10

for n in n_list:
    generateAndWriteDataAndSamples('Data/', 4, m, n)
    for i in range(1, m+1):
        infile = 'Data/samples_' + str(i) + '_full'
        outprefix = 'abkfiles/' + str(n) + '_' + str(i) + '_'
        convertSampleFileToABK(infile, outprefix)

