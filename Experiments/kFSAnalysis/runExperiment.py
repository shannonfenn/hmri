import subprocess
import datetime
import os
import os.path
import genData
import collections
import glob
import shutil

ParameterSet = collections.namedtuple('ParameterSet',
                          ['numBits',
                           'numExamples',
                           'numGatesInitial',
                           'numGates',
                           'numTemps',
                           'numStepsPerTemp',
                           'initialTemp',
                           'tempScaler',
                           'numExperiments'])

def setupAndGetResultDir(buildDir, now):
    # build binary
    subprocess.check_call('make', cwd=buildDir)
    # move it to this directory
    subprocess.check_call(['cp', buildDir  + 'BooleanNet', '.'])
    # make directory for the results
    resultDir = 'Results_' + now.strftime('%y-%m-%d_%H-%M')

    # find the first number i such that 'Results_datetime_i' is not
    # already a directory and make that new directory
    i = 0
    while os.path.isdir(resultDir + '_' + str(i)):
        i += 1
    resultDir += '_' + str(i)
    os.mkdir(resultDir)

    #copy this script into results directory
    shutil.copy(__file__, os.path.join(resultDir, 'scriptusedforexperiment') )

    # make or clear data folder
    if os.path.isdir('Data'):
        for f in glob.glob('Data/*'):
            os.remove(f)
    else:
        os.mkdir('Data')

    # return the directory name for the results to go in
    return resultDir
    
def createCommandsFile(f, optimiser, methods, parameters):
    for i in range(parameters.numExperiments):
        for method in methods:
            f.write('./BooleanNet givensamples Data/data Data/samples_' + str(i+1) + ' ' + optimiser + ' ')
            if optimiser == 'ganneal':
                f.write(str(parameters.numGatesInitial) + ' ')
            f.write(' '.join(str(p) for p in parameters[3:8]))
            f.write(' ' + method)
            if i < parameters.numExperiments - 1:
                f.write('; echo, ')
            f.write('\n')


def run(buildDir, optimisers, methods, params):
    # Get timestamp
    startTime = datetime.datetime.now()
    # Build timestamped result directory and data dir
    resultDir = setupAndGetResultDir(buildDir, startTime)
    # Generate data files
    genData.generateAndWriteDataAndSamples('Data/', params.numBits, params.numExperiments, params.numExamples)
    # Copy data directory into result directory for later reference
    shutil.copytree('Data', os.path.join(resultDir, 'Data'))

    for opt in optimisers:
        # Create result and error files and prep them
        with open(os.path.join(resultDir, 'results'), 'w') as f:
            f.write('[')    # prep for JSON
        with open(os.path.join(resultDir, 'errors'), 'w') as f:
            f.write('')



        # create commands file
        #with open('commands', 'w') as infile:
        #    createCommandsFile(infile, opt, methods, parameters)
        #    # run these commands 8 at a time using GNUparallel
        #
        #    with open(os.path.join(resultDir, 'results'), 'w') as outfile:
        #        with open(os.path.join(resultDir, 'errors'), 'w') as errfile:
        #            # Fix parallel later
        #            #subprocess.check_call(['parallel', '-j8'], stdin=infile, stdout=outfile, stderr=errfile)
        #            infile.seek(0)
        #            line = infile.readline()
        #            subprocess.check_call(line.split())

 #MODIFIED VERSION
        import io
        cmdstring = io.StringIO()
        createCommandsFile(cmdstring, opt, methods, parameters)
            # run these commands 8 at a time using GNUparallel

        with open(os.path.join(resultDir, 'results'), 'w') as outfile:
            with open(os.path.join(resultDir, 'errors'), 'w') as errfile:
                # Fix parallel later
                #subprocess.check_call(['parallel', '-j8'], stdin=infile, stdout=outfile, stderr=errfile)
                subprocess.check_call(cmdstring.getvalue().split(), stdout=outfile, stderr=errfile)




        # finish JSON list in results file
        with open(os.path.join(resultDir, 'results'), 'a') as f:
            f.write(']')
        # rename results and errors files
        os.rename(os.path.join(resultDir, 'results'), os.path.join(resultDir, 'results_' + opt))
        os.rename(os.path.join(resultDir, 'errors'), os.path.join(resultDir, 'errors_' + opt))
        # remove temp commands file
        #os.remove('commands')


	#python analyse.py Results/results_anneal > Results/results_anneal.txt
	#python analyse.py Results/results_ganneal > Results/results_ganneal.txt

    finalTime = datetime.datetime.now()
    print('time taken: ' + str(finalTime - startTime))
    #subprocess.call(['mailx', '-x', 'Experiment done ' + str(finalTime - startTime), '<', '/dev/null', 'shannon.fenn@gmail.com'])



if __name__ == '__main__':
    buildDir = '../../Code/BooleanNet/C++/build-BooleanNet-Desktop-Release/'

    #optimisers = ['anneal', 'ganneal']
    #methods = ['simple',
    #       'weighted_lin_lsb',
    #       'hierarchical_lin_lsb',
    #       'e4_lsb',
    #       'e5_lsb',
    #       'e6_lsb',
    #       'e7_lsb']
    #parameters = ParameterSet(8,    # numBits
    #                      128,  # numExamples
    #                      8,    # numGatesInitial
    #                      300,  # numGates
    #                      10000,# numTemps
    #                      150,  # numStepsPerTemp
    #                      0.05, # initialTemp
    #                      0.997,# tempScaler
    #                      100)  # numExperiments

    optimisers = ['ganneal']
    methods = ['e6_lsb']
    parameters = ParameterSet(4,    # numBits
                          32,  # numExamples
                          8,    # numGatesInitial
                          60,  # numGates
                          1000,# numTemps
                          150,  # numStepsPerTemp
                          0.05, # initialTemp
                          0.997,# tempScaler
                          1)  # numExperiments
    run(buildDir, optimisers, methods, parameters)
