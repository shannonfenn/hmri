import sys
import random
from itertools import chain

def generateAdditionData(Ni):
	# Upper limit
	upper = 2**Ni
	# generate tuples of (input1, input2, output)
	#dataset = [(i1, i2, (i1+i2)%upper ) for i1 in xrange(upper) for i2 in xrange(upper)]
	dataset = []

	for i1 in range(upper):
		for i2 in range(upper):
			i1bits = [ bool(int(c)) for c in '{:0{width}b}'.format(i1, width=Ni)]
			i2bits = [ bool(int(c)) for c in '{:0{width}b}'.format(i2, width=Ni)]
			i1bits = i1bits[::-1]
			i2bits = i2bits[::-1]
			sumbits = [False]
			carrybits = [False]
			for i1b, i2b in zip(i1bits, i2bits):
				s = i1b ^ i2b ^ carrybits[-1]
				c = (i1b and i2b) or ( carrybits[-1] and (i1b ^ i2b) )
				sumbits.append(s)
				carrybits.append(c)
				
			#output = []
			#for s, c in zip(sumbits[1:], carrybits[1:]):
			#	output.append(s)
			#	output.append(c)
			dataset.append( (i1bits[::-1], i2bits[::-1], sumbits[:0:-1], carrybits[:0:-1]) )
			#dataset.append( (i1bits[::-1], i2bits[::-1], output[-2::-1]) )

	return dataset


def main(argv):
	# process arguments
	if not ( 5 <= len(argv) <= 6):
		sys.exit("Usage " + argv[0] + " prefix #inputbits #sets #samplesPerSet")
	
	# parameters
	prefix = argv[1]
	Ni = int(argv[2])
	numSets = int(argv[3])
	samplesPerSet = int(argv[4])
	numExamples = 2**(2*Ni)
	
	if samplesPerSet > numExamples:
		sys.exit('Error: samplesPerSet ({}) larger than number of examples({})'.format(samplesPerSet, numExamples))
	
	# generate full dataset and write to file
	dataset = generateAdditionData(Ni)
	with open(prefix + 'data', 'w') as f:
		f.write( '{} {}\n'.format(2*Ni, Ni) )
		for i1, i2, s, c in dataset:
			f.write(' '.join( [str(int(b)) for b in chain(i1, i2, s)] ) + '\n')
	with open(prefix + 'datawithcarry', 'w') as f:
		f.write( '{} {}\n'.format(2*Ni, 2*Ni-1) )
		for i1, i2, s, c in dataset:
			out = [b for b in chain.from_iterable(list(zip(c, s)))]
			f.write(' '.join( [str(int(b)) for b in chain(i1, i2, out[1:])] ) + '\n')
	
	# generate index samples	
	samples = [random.sample(range(numExamples), samplesPerSet) for i in range(numSets)]

	# and write them to files
	for i in range(len(samples)):
		# shuffle randomly
		random.shuffle(samples[i])
			
		with open(prefix + 'samples_' + str(i+1), 'w') as f:
			f.write(' '.join(str(s) for s in samples[i]))

if __name__ == '__main__':
	main(sys.argv)