#!/bin/bash
STARTTIME=$(date +%s)

EXP_DIR=$(pwd)
BUILD_DIR="/home/shannon/HMRI/BooleanNet/C++/build-BooleanNet-Desktop-Release/"

cd $BUILD_DIR
make
make_ret=$?
cd $EXP_DIR


if [ $make_ret -ne 0 ]; then
	echo 'make failed, aborting.'
else
	cp "$BUILD_DIR"BooleanNet $EXP_DIR

	echo "          metric             trgerror(guiding)    trgerror(simple)    error(guiding)     error(simple)      #iterBest     #iterFinish      time(s)" > results # Clear the results file

	# Parameters
	Ni=4
	Ne=64
	Ng=45
	Ns=1000
	T=2
	# generate N copies of commands	
	N=10
	N2=10

	# generate data file
	python generateData.py $Ni
	# generate random samples
	python generateSamples.py data $N $Ne

	echo -n "" > commands
	for i in $(seq 1 $N);
	do 
		for k in $(seq 1 $N2);
		do
			echo "./BooleanNet givensamples data data_samples_$i $Ng $Ns $T 0.999 simple" >> commands
        	        echo "./BooleanNet givensamples data data_samples_$i $Ng $Ns $T 0.999 weighted_linear_msb" >> commands
                	echo "./BooleanNet givensamples data data_samples_$i $Ng $Ns $T 0.999 weighted_linear_lsb" >> commands
                	echo "./BooleanNet givensamples data data_samples_$i $Ng $Ns $T 0.999 hierarchical_linear_msb" >> commands
                	echo "./BooleanNet givensamples data data_samples_$i $Ng $Ns $T 0.999 hierarchical_linear_lsb" >> commands
                	echo "./BooleanNet givensamples data data_samples_$i $Ng $Ns $T 0.999 weighted_exp_msb" >> commands
                	echo "./BooleanNet givensamples data data_samples_$i $Ng $Ns $T 0.999 weighted_exp_lsb" >> commands
                	echo "./BooleanNet givensamples data data_samples_$i $Ng $Ns $T 0.999 hierarchical_exp_msb" >> commands
                	echo "./BooleanNet givensamples data data_samples_$i $Ng $Ns $T 0.999 hierarchical_exp_lsb" >> commands
 		done
	done

	# run these commands 8 at a time
	parallel -j8 < commands >> results
	# remove temp commands file
#	rm commands

	CMDENDTIME=$(date +%s)

	echo "Finished : $[$CMDENDTIME - $STARTTIME]s"
fi
