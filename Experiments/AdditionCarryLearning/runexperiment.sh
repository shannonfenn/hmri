#!/bin/bash
STARTTIME=$(date +%s)

EXP_DIR=$(pwd)
BUILD_DIR="/home/shannon/HMRI/BooleanNet/C++/build-BooleanNet-Desktop-Release/"

cd $BUILD_DIR
make
make_ret=$?
cd $EXP_DIR


if [ $make_ret -ne 0 ]; then
	echo 'make failed, aborting.'
else
	cp "$BUILD_DIR"BooleanNet $EXP_DIR

	# Parameters
	Ni=4
	Ne=32
#	Ng=64
	NgStart=8
	Ng=64
	Nt=1000
#	Ns=15000
	Ns=1500
	T=0.05
#	T=0
	Ts=0.997
	# generate N copies of commands	
	N=1000

	mkdir -p Results
	mkdir -p Data

	# generate data file and random samples
	rm Data/*
	python genData.py Data/ $Ni $N $Ne

	methods="simple weighted_lin_lsb hierarchical_lin_lsb e4_lsb e5_lsb e6_lsb e7_lsb"
	
	for problem in data datawithcarry
	do
		echo "[" > Results/results # Clear the results file and start JSON list
		echo "" > Results/errors

		# Clear commands file
		echo -n "" > commands
		for i in $(seq 1 $N);
		do 
			for method in $methods
			do
#				echo "./BooleanNet givensamples Data/$problem Data/samples_$i ganneal $NgStart $Ng $Nt $Ns $T $Ts $method; echo ," >> commands
				echo "./BooleanNet givensamples Data/$problem Data/samples_$i anneal $Ng $Nt $Ns $T $Ts $method; echo ," >> commands
			done
		done

		# run these commands 8 at a time
		parallel -j8 < commands 1>> Results/results 2>> Results/errors

		#remove trailing comma
		sed -i '$ d' Results/results
		echo "]" >> Results/results		# finish JSON list

		mv Results/results Results/results_$problem
		mv Results/errors Results/errors_$problem
		# remove temp commands file
		rm commands
	done

	cp $0 Results/$0

	for problem in data datawithcarry
	do
	    python analyse.py Results/results_$problem > Results/results_$problem.txt
    done

	mailx -s "Experiment done" < /dev/null "shannon.fenn@gmail.com"

	CMDENDTIME=$(date +%s)
	echo "Commands finished : $[$CMDENDTIME - $STARTTIME]s"
fi
